library ieee;
use ieee.std_logic_1164.all;

entity not1 is
	port(
		input: in std_logic;
		output: out std_logic
	);
end not1;

architecture behavioral of not1 is
begin
	output <= not input;
end behavioral;