library ieee;
use ieee.std_logic_1164.all;

entity nand2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end nand2;

architecture behavioral of nand2 is
begin
	output <= inputs(0) nand inputs(1);
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity nand3 is
	port(
		inputs: in std_logic_vector(2 downto 0);
		output: out std_logic
	);
end nand3;

architecture behavioral of nand3 is
begin
	output <= not (inputs(0) and inputs(1) and inputs(2));
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity nandN is
	generic(
		N: natural := 2
	);
	port(
		inputs: in std_logic_vector(N - 1 downto 0);
		output: out std_logic
	);
end nandN;

architecture behavioral of nandN is
	signal ones: std_logic_vector(N - 1 downto 0) := (others => '1');
begin
	output <= '0' when inputs = ones else '1';
end behavioral;