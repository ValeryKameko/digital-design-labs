library ieee;
use ieee.std_logic_1164.all;

entity and2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end and2;

architecture behavioral of and2 is
begin
	output <= inputs(0) and inputs(1);
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity and3 is
	port(
		inputs: in std_logic_vector(2 downto 0);
		output: out std_logic
	);
end and3;

architecture behavioral of and3 is
begin
	output <= inputs(0) and inputs(1) and inputs(2);
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity andN is
	generic(
		N: natural := 2
	);
	port(
		inputs: in std_logic_vector(N - 1 downto 0);
		output: out std_logic
	);
end andN;

architecture behavioral of andN is
	signal ones: std_logic_vector(N - 1 downto 0) := (others => '1');
begin
	output <= '1' when inputs = ones else '0';
end behavioral;