library ieee;
use ieee.std_logic_1164.all;

entity jk_latch is
	port(
		j, k: in std_logic;
		en: in std_logic;
		set, reset: in std_logic;
		q, nq: out std_logic
	);
end entity jk_latch;

architecture behavioral of jk_latch is
	signal storing_value: std_logic;
begin
	store_value: process (set, reset, j, k, en)
	begin
		if reset = '1' then
			storing_value <= '0';
		elsif set = '1' then
			storing_value <= '1';
		elsif en = '1' then
			case to_stdlogicvector(j & k) is
				when "11" => storing_value <= not storing_value;
				when "10" => storing_value <= '1';
				when "01" => storing_value <= '0';
				when others => storing_value <= storing_value;
			end case;
		end if;
	end process store_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;