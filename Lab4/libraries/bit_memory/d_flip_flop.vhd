library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop is
	port(
		d, clk, en: in std_logic;
		set, reset: in std_logic;
		q, nq: out std_logic
	);
end entity d_flip_flop;

architecture behavioral of d_flip_flop is
	signal storing_value: std_logic;
begin
	store_value: process (d, en, clk, set, reset)
	begin
		if reset = '1' then
			storing_value <= '0';
		elsif set = '1' then
			storing_value <= '1';
		elsif rising_edge(clk) and en = '1' then
			storing_value <= d;
		else
			storing_value <= storing_value;
		end if;
	end process store_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;