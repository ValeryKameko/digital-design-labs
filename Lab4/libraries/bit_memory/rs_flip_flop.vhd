library ieee;
use ieee.std_logic_1164.all;

entity rs_flip_flop is
	port(
		set, reset: in std_logic;
		en, clk: in std_logic;
		q, nq: out std_logic
	);
end entity rs_flip_flop;

architecture behavioral of rs_flip_flop is
	signal storing_value: std_logic;
begin
	store_value: process (clk, en, set, reset)
	begin
		if rising_edge(clk) and en = '1' then
			if reset = '1' then
				storing_value <= '0';
			elsif set = '1' then
				storing_value <= '1';
			else
				storing_value <= storing_value;
			end if;
		end if;
	end process store_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;