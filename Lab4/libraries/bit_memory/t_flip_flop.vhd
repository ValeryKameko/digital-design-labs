library ieee;
use ieee.std_logic_1164.all;

entity t_flip_flop is
	port(
		t, clk: in std_logic;
		set, reset: in std_logic;
		q, nq: out std_logic
	);
end entity t_flip_flop;

architecture behavioral of t_flip_flop is
	signal storing_value: std_logic;
begin
	store_value: process (t, clk, set, reset)
	begin
		if reset = '1' then
			storing_value <= '0';
		elsif set = '1' then
			storing_value <= '1';
		elsif rising_edge(clk) and t = '1' then
			storing_value <= not storing_value;
		else
			storing_value <= storing_value;
		end if;
	end process store_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;