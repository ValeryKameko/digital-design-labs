`timescale 1ns / 1ps


module signature_analyzer__structural(
		input sin,
		input [N - 1:0] din,
		
		input clk,
		input en,
		input set,
		input reset,
		
		output [N - 1:0] dout
    );

parameter N = 8;
parameter [N:0] POLYNOMIAL = 'b0;

wire not_set, enable;
wire [N:0] shifting_bits;

not invert_set(not_set, set);

or calc_enable(enable, set, en);

assign shifting_bits[0] = sin;

wire in_bit = (POLYNOMIAL[N] == 1) ? shifting_bits[N] : 'b0;

genvar i;

for (i = 1; i <= N; i = i + 1)
begin
	wire shifting_bit, setting_bit;
	wire shift_in_bit, xor_value;
	
	if (POLYNOMIAL[i - 1] == 1)
		xor calc_xor_value(xor_value, shifting_bits[i - 1], in_bit);
	else
		assign xor_value = shifting_bits[i - 1];
	
	and calc_setting_bit(setting_bit, set, din[i - 1]);
	
	and calc_shifting_bit(shifting_bit, not_set, xor_value);
	
	or calc_shift_in_bit(shift_in_bit, shifting_bit, setting_bit);
	
	d_flip_flop_ shift_bit(
		.d(shift_in_bit),
		.clk(clk),
		.en(enable),
		.set(0),
		.reset(reset),
		.q(shifting_bits[i])
	);
end;

assign dout = shifting_bits[N:1];

endmodule


module signature_analyzer__behavioral(
		input sin,
		input [N - 1:0] din,
		
		input clk,
		input en,
		input set,
		input reset,
		
		output [N - 1:0] dout
    );

parameter N = 8;
parameter [N:0] POLYNOMIAL = 'b1_0000_0011;

reg [N:0] shifting_bits;
reg [N - 1:0] xor_values;
reg in_bit;

always @(sin)
	shifting_bits[0] = sin;

integer i;

always @(posedge clk or posedge reset)
begin : shift_bits
	if (reset == 1)
		shifting_bits[N:1] <= 'b0;
	else if (set == 1)
		shifting_bits[N:1] <= din;
	else if (en == 1)
	begin		
		in_bit = (POLYNOMIAL[N] == 1) ? shifting_bits[N] : 'b0;

		for (i = 0; i < N; i = i + 1)
			xor_values[i] = shifting_bits[i] ^ (POLYNOMIAL[i] & in_bit);
		
		shifting_bits[N:1] <= xor_values;
	end;
end : shift_bits

assign dout = shifting_bits[N:1];

endmodule
