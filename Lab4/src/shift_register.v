`timescale 1ns / 1ps


module shift_register__structural(
		input sin,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output [N - 1:0] pout
    );

parameter N = 8;

wire [N:0] shifting_bits;

assign shifting_bits[0] = sin;

genvar i;

for (i = 1; i <= N; i = i + 1)
begin : shift_bits		
	d_flip_flop_ shift_bit(
		.d(shifting_bits[i - 1]),
		.clk(clk),
		.en(en),
		.set(set),
		.reset(reset),
		.q(shifting_bits[i])
	);
end : shift_bits

assign pout = shifting_bits[N:1];

endmodule


module shift_register__behavioral(
		input sin,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output [N - 1:0] pout
    );

parameter N = 8;

reg [N - 1:0] shifting_bits;

always @(posedge clk or posedge set or posedge reset)
begin : shift_bits
	if (reset == 1)
		shifting_bits <= {N{1'b0}};
	else if (set == 1)
		shifting_bits <= {N{1'b1}};
	else if (en == 1)
	begin
		shifting_bits <= (N == 1) ? sin : {shifting_bits[N - 2:0], sin};
	end
end : shift_bits

assign pout = shifting_bits;

endmodule
