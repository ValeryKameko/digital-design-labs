`timescale 1ns / 1ps


module lfsr_example_(
		input clk,
		input en_switch,
		input set_button,
		input reset_button,
		input [SWITCHES_COUNT - 1:0] switches,
		output [LEDS_COUNT - 1:0] leds,
		output [HEX_DIGITS_COUNT - 1:0] hex_digits_en,
		output [6:0] hex_digits_controls
    );

parameter unsigned SWITCHES_COUNT = 15;
parameter unsigned LEDS_COUNT = 16;
parameter unsigned HEX_DIGITS_COUNT = 8;
parameter unsigned HEX_DIGITS_COUNT_BITS = 3;
localparam unsigned HEX_DIGITS_BITS_PADDING = (4 - LFSR_BITS_COUNT % 4) % 4;

parameter unsigned PRIMARY_CLOCK_COUNT = 100_000_000;
parameter unsigned HEX_DIGITS_CLOCK_FREQUENCY = 100;
parameter unsigned LFSR_CLOCK_FREQUENCY = 1;
localparam unsigned CLOCK_BITS_COUNT = 32;
localparam unsigned TEMP_HEX_DIGITS_BITS_COUNT = HEX_DIGITS_CLOCK_COUNT * 4;
localparam unsigned HEX_DIGITS_CLOCK_COUNT = PRIMARY_CLOCK_COUNT / HEX_DIGITS_CLOCK_FREQUENCY;
localparam unsigned LFSR_CLOCK_COUNT = PRIMARY_CLOCK_COUNT / LFSR_CLOCK_FREQUENCY;

parameter unsigned LFSR_BITS_COUNT = 14;
parameter [LFSR_BITS_COUNT:0] LFSR_POLYNOMIAL = 'b100_0000_0001_0100_0011;

// Actual LFSR
wire lfsr_clk;

clock_divider_ #(.BITS(CLOCK_BITS_COUNT), .COUNT(LFSR_CLOCK_COUNT)) divide_lfsr_clk(
	.clk(clk),
	.reset(reset_button),
	.divided_clk(lfsr_clk)
);

wire [LFSR_BITS_COUNT - 1:0] lfsr_din;
wire lfsr_en = en_switch;

wire lfsr_set = set_button;
wire lfsr_reset = reset_button;
wire [LFSR_BITS_COUNT - 1:0] lfsr_dout;

lfsr_with_inner_xors__structural #(
	.N(LFSR_BITS_COUNT), .POLYNOMIAL(LFSR_POLYNOMIAL)
) lfsr(
	.din(lfsr_din),
	.clk(lfsr_clk),
	.en(lfsr_en),
	.set(lfsr_set),
	.reset(lfsr_reset),
	.dout(lfsr_dout)
);

if (SWITCHES_COUNT >= LFSR_BITS_COUNT)
	assign lfsr_din = switches[LFSR_BITS_COUNT - 1:0];
else
	assign lfsr_din = {'b0, switches};


// LED output
if (LEDS_COUNT <= LFSR_BITS_COUNT)
	assign leds = lfsr_dout[LEDS_COUNT - 1:0];
else
	assign leds = {'b0, lfsr_dout};


// HEX output
wire hex_clk;

clock_divider_ #(.BITS(CLOCK_BITS_COUNT), .COUNT(HEX_DIGITS_CLOCK_COUNT)) divide_hex_clk(
	.clk(clk),
	.reset(reset_button),
	.divided_clk(hex_clk)
);

integer current_hex_digit_index;

always @(posedge hex_clk or posedge reset_button)
begin : calc_current_hex_digit_index
	if (reset_button == 1)
		current_hex_digit_index <= 0;
	else
		current_hex_digit_index <= (current_hex_digit_index + 1) % (HEX_DIGITS_COUNT * 4);
end : calc_current_hex_digit_index

wire [TEMP_HEX_DIGITS_BITS_COUNT - 1:0] temp_hex_digit_bits;

if (TEMP_HEX_DIGITS_BITS_COUNT >= LFSR_BITS_COUNT)
	assign temp_hex_digit_bits = {'b0, lfsr_dout};
else
	assign temp_hex_digit_bits = lfsr_dout[TEMP_HEX_DIGITS_BITS_COUNT - 1:0];

wire [3:0] current_hex_digit;
wire [HEX_DIGITS_COUNT - 1:0] hex_digits_enable;

genvar i;
for (i = 0; i < HEX_DIGITS_COUNT; i = i + 1)
	assign hex_digits_enable[i] = i == current_hex_digit_index;

assign current_hex_digit = temp_hex_digit_bits[current_hex_digit_index * 4 +: 4];

assign hex_digits_en = ~hex_digits_enable;

seven_segment_encoder_ encode_hex_digit(
	.digit(current_hex_digit),
	.enables(hex_digits_controls)
);

endmodule
