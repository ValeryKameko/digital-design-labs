library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity combinational_circuit is
	generic(
		INPUTS: natural := 8;
		OUTPUTS: natural := 1;
		VALUES: std_logic_vector := (
			x"AA" & x"E5" & x"F9" & x"05" & x"69" & x"0C" & x"55" & x"9A" & x"81" & x"65" & x"9A" & x"4B" & x"A3" & x"39" & x"A4" & x"EA" &
			x"2C" & x"A0" & x"35" & x"51" & x"67" & x"D9" & x"60" & x"31" & x"43" & x"AA" & x"C4" & x"70" & x"71" & x"2F" & x"22" & x"3F"
		)
	);
	port(
		din: in std_logic_vector(INPUTS - 1 downto 0);
		dout: out std_logic_vector(OUTPUTS - 1 downto 0)
	);
end entity combinational_circuit;

architecture behavioral of combinational_circuit is
	signal mapping: std_logic_vector(2 ** INPUTS * OUTPUTS - 1 downto 0) := VALUES;
	signal index: natural;
begin
	index <= to_integer(unsigned(din) * OUTPUTS);
	dout <= mapping(index + OUTPUTS - 1 downto index);
end architecture behavioral;
