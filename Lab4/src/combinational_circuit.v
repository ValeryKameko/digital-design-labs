`timescale 1ns / 1ps


module combinational_circuit_(
		input [INPUTS - 1:0] din,
		output [OUTPUTS - 1:0] dout
    );

parameter INPUTS = 8;
parameter OUTPUTS = 1;
localparam INPUTS_2_OUTPUTS = 2 ** INPUTS * OUTPUTS;
parameter [INPUTS_2_OUTPUTS - 1:0] VALUES = {
	8'hAA, 8'hE5, 8'hF9, 8'h05, 8'h69, 8'h0C, 8'h55, 8'h9A, 8'h81, 8'h65, 8'h9A, 8'h4B, 8'hA3, 8'h39, 8'hA4, 8'hEA,
	8'h2C, 8'hA0, 8'h35, 8'h51, 8'h67, 8'hD9, 8'h60, 8'h31, 8'h43, 8'hAA, 8'hC4, 8'h70, 8'h71, 8'h2F, 8'h22, 8'h3F
};

assign dout = VALUES[din * OUTPUTS +: OUTPUTS];

endmodule
