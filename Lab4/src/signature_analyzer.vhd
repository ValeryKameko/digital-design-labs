library ieee;
library bit_memory;
library gates;
use ieee.std_logic_1164.all;
use work.ring_counter_pkg.all;

entity signature_analyzer_structural is
	generic(
		N: natural := 8;
		POLYNOMIAL: std_logic_vector := b"1_0000_0011"
	);
	port(
		sin: in std_logic;
		din: in std_logic_vector(N - 1 downto 0); 
		clk, en: in std_logic;
		set, reset: in std_logic;
		dout: out std_logic_vector(N - 1 downto 0)
	);
end entity signature_analyzer_structural;

architecture structural of signature_analyzer_structural is
	component d_flip_flop is
		port(
			d, clk, en: in std_logic;
			set, reset: in std_logic;
			q: out std_logic
		);
	end component d_flip_flop;

	component xor2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component xor2;
	
	component and2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component and2;
	
	component or2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component or2;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;

	signal not_set, enable, in_bit: std_logic;
	signal shifting_bits: std_logic_vector(N downto 0);
begin
	invert_set: not1
		port map (input => set, output => not_set); 

	calc_enable: or2
		port map (inputs => en & set, output => enable);
		
	shifting_bits(0) <= sin;
	in_bit <= shifting_bits(N) when POLYNOMIAL(N) = '1' else '0';

	shift_bits: for i in 0 to N - 1 generate
		signal setting_bit, shifting_bit: std_logic;
		signal shift_in_bit, xor_value: std_logic;
	begin
		skip_xor: if POLYNOMIAL(i) = '0' generate
			xor_value <= shifting_bits(i);
		end generate skip_xor;

		perform_xor: if POLYNOMIAL(i) = '1' generate
			xor_bit: xor2
				port map (inputs => shifting_bits(i) & in_bit, output => xor_value);
		end generate perform_xor;

		calc_setting_bit: and2
			port map (inputs => set & din(i), output => setting_bit);

		calc_shifting_bit: and2
			port map (inputs => not_set & xor_value, output => shifting_bit);

		calc_shift_in_bit: or2
			port map (inputs => setting_bit & shifting_bit, output => shift_in_bit);

		shift_bit: d_flip_flop
			port map (d => shift_in_bit, clk => clk, en => enable, set => '0', reset => reset, q => shifting_bits(i + 1));
	end generate shift_bits;
	
	dout <= shifting_bits(N downto 1);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;
use work.ring_counter_pkg.all;

entity signature_analyzer_behavioral is
	generic(
		N: natural := 8;
		POLYNOMIAL: std_logic_vector := b"1_0000_0011"
	);
	port(
		sin: in std_logic;
		din: in std_logic_vector(N - 1 downto 0); 
		clk, en: in std_logic;
		set, reset: in std_logic;
		dout: out std_logic_vector(N - 1 downto 0)
	);
end entity signature_analyzer_behavioral;

architecture behavioral of signature_analyzer_behavioral is
	signal shifting_bits: std_logic_vector(N downto 0);
begin
	shifting_bits(0) <= sin;

	shift_bits: process (en, clk, reset, set, din)
		variable xor_values: std_logic_vector(N - 1 downto 0);
		variable in_bit: std_logic;
	begin
		if reset = '1' then
			shifting_bits(N downto 1) <= (others => '0');
		elsif set = '1' then
			shifting_bits(N downto 1) <= din;
		elsif rising_edge(clk) and en = '1' then 
			in_bit := POLYNOMIAL(N) and shifting_bits(N);
			
			for i in 0 to N - 1 loop
				xor_values(i) := shifting_bits(i) xor (POLYNOMIAL(i) and in_bit);
			end loop;
			
			shifting_bits(N downto 1) <= xor_values;
		end if;
	end process shift_bits;
	
	dout <= shifting_bits(N downto 1);
end architecture behavioral;
