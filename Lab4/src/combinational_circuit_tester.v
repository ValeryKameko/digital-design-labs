`timescale 1ns / 1ps


module combinational_circuit_tester_(
		input [INPUTS - 1:0] seed_din,
		output [INPUTS - 1:0] seed_dout,
		
		input [SIGNATURE_BITS - 1:0] signature_din,
		output [SIGNATURE_BITS - 1:0] signature_dout,

		output [INPUTS - 1:0] circuit_inputs,
		input circuit_output,

		input en,
		input clk,

		input set_seed,
		input set_signature,
		input reset_seed,
		input reset_signature
    );

parameter INPUTS = 8;
parameter SIGNATURE_BITS = 10;
parameter [INPUTS:0] SEED_POLYNOMIAL = 'h9B;
parameter [SIGNATURE_BITS:0] SIGNATURE_POLYNOMIAL = 'h3D9;

wire [INPUTS - 1:0] seed_bits;

lfsr_with_outer_xors__structural #(.N(INPUTS), .POLYNOMIAL(SEED_POLYNOMIAL)) seed_inputs(
	.din(seed_din),
	.clk(clk),
	.en(en),
	.set(set_seed),
	.reset(reset_seed),
	.dout(seed_bits)
);

assign circuit_inputs = seed_bits;
assign seed_dout = seed_bits;

signature_analyzer__structural #(.N(SIGNATURE_BITS), .POLYNOMIAL(SIGNATURE_POLYNOMIAL)) generate_signature(
	.sin(circuit_output),
	.din(signature_din),
	.clk(clk),
	.en(en),
	.set(set_signature),
	.reset(reset_signature),
	.dout(signature_dout)
);

endmodule
