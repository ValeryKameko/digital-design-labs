library ieee;
use ieee.std_logic_1164.all;

package ring_counter_pkg is
	type RingCounterType is (Straight, Johnson);
end package ring_counter_pkg;


library ieee;
library bit_memory;
library gates;
use ieee.std_logic_1164.all;
use work.ring_counter_pkg.all;

entity ring_counter_structural is
	generic(
		N: natural := 8;
		RING_TYPE: RingCounterType := Johnson
	);
	port(
		clk, en: in std_logic;
		reset: in std_logic;
		pout: out std_logic_vector(N - 1 downto 0)
	);
end entity ring_counter_structural;

architecture structural of ring_counter_structural is
	component d_flip_flop is
		port(
			d, clk, en: in std_logic;
			set, reset: in std_logic;
			q: out std_logic
		);
	end component d_flip_flop;

	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	

	signal shifting_bits: std_logic_vector(N downto 0);
	signal need_set_first_bit, need_reset_first_bit: std_logic;
begin
	generate_straight_counter: if RING_TYPE = Straight generate
		need_set_first_bit <= reset;
		need_reset_first_bit <= '0';
		shifting_bits(0) <= shifting_bits(N);
	end generate generate_straight_counter;
	
	generate_johnson_counter: if RING_TYPE = Johnson generate
		calc_shifting_bit: not1
			port map (input => shifting_bits(N), output => shifting_bits(0));
		need_reset_first_bit <= reset;
	end generate generate_johnson_counter;
		
	shift_bits: for i in 1 to N generate
		signal need_set, need_reset: std_logic;
	begin
		generate_first_bit: if i = 1 generate
			need_set <= need_set_first_bit;
			need_reset <= need_reset_first_bit;
		end generate generate_first_bit;
		
		generate_non_first_bit: if i /= 1 generate
			need_set <= '0';
			need_reset <= reset;
		end generate generate_non_first_bit;
		
		shift_bit: d_flip_flop
			port map (d => shifting_bits(i - 1), clk => clk, en => en, set => need_set, reset => need_reset, q => shifting_bits(i));
	end generate shift_bits;
	
	pout <= shifting_bits(N downto 1);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;
use work.ring_counter_pkg.all;

entity ring_counter_behavioral is
	generic(
		N: natural := 8;
		RING_TYPE: RingCounterType := Johnson
	);
	port(
		clk, en: in std_logic;
		reset: in std_logic;
		pout: out std_logic_vector(N - 1 downto 0)
	);
end entity ring_counter_behavioral;

architecture behavioral of ring_counter_behavioral is
	signal shifting_bits: std_logic_vector(N - 1 downto 0);
begin
	shift_bits: process (en, clk, reset)
		variable in_bit: std_logic;
	begin
		if reset = '1' then
			case RING_TYPE is
				when Straight => in_bit := '1';
				when Johnson => in_bit := '0';
			end case;
			
			shifting_bits <= (0 => in_bit, others => '0');
		elsif rising_edge(clk) and en = '1' then
			case RING_TYPE is
				when Straight => in_bit := shifting_bits(N - 1);
				when Johnson => in_bit := not shifting_bits(N - 1);
			end case;
			
			shifting_bits <= shifting_bits(N - 2 downto 0) & in_bit;
		else
			shifting_bits <= shifting_bits;
		end if;
	end process shift_bits;
	
	pout <= shifting_bits;
end architecture behavioral;