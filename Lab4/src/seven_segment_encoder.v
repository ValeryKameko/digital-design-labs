`timescale 1ns / 1ps


module seven_segment_encoder_(
		input [3:0] digit,
		output [6:0] enables
    );

combinational_circuit_ #(
	.INPUTS(4),
	.OUTPUTS(7),
	.VALUES({
		7'b0111000,
		7'b0110000,
		7'b1000010,
		7'b0110001,
		7'b1100000,
		7'b0000010,
		7'b0000100,
		7'b0000000,
		7'b0001111,
		7'b0100000,
		7'b0100100,
		7'b1001100,
		7'b0000110,
		7'b0010010,
		7'b1001111,
		7'b0000001
	})
) encode(
	.din(digit),
	.dout(enables)
);

endmodule
