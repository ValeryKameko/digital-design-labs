library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lfsr_example is
	generic(
		SWITCHES_COUNT: natural := 15;
		LEDS_COUNT: natural := 16;
		HEX_DIGITS_COUNT: natural := 8;
		HEX_DIGITS_COUNT_BITS: natural := 3;
		CLOCK_BITS_COUNT: natural := 32;
		PRIMARY_CLOCK_COUNT: natural := 100_000_000;
		HEX_DIGITS_CLOCK_FREQUENCY: natural := 100;
		LFSR_CLOCK_FREQUENCY: natural := 1;
		LFSR_BITS_COUNT: natural := 14;
		LFSR_POLYNOMIAL: std_logic_vector := b"100_0000_0001_0100_0011"
	);
	port(
		clk, en_switch: in std_logic;
		set_button, reset_button: in std_logic;
		switches: in std_logic_vector(SWITCHES_COUNT - 1 downto 0);
		leds: out std_logic_vector(LEDS_COUNT - 1 downto 0);
		hex_digits_en: out std_logic_vector(HEX_DIGITS_COUNT - 1 downto 0);
		hex_digits_controls: out std_logic_vector(6 downto 0)
	);
end entity lfsr_example;

architecture behavioral of lfsr_example is
	component clock_divider is
		generic(
			BITS: natural;
			COUNT: natural
		);
		port(
			clk, reset: in std_logic;
			divided_clk: out std_logic
		);
	end component clock_divider;
	
	component lfsr is
		generic(
			N: natural;
			POLYNOMIAL: std_logic_vector
		);
		port(
			din: in std_logic_vector(N - 1 downto 0); 
			clk, en: in std_logic;
			set, reset: in std_logic;
			dout: out std_logic_vector(N - 1 downto 0)
		);
	end component lfsr;
	
	component seven_segment_encoder is
		port(
			digit: in std_logic_vector(3 downto 0);
			enables: out std_logic_vector(6 downto 0)
		);
	end component seven_segment_encoder;
	
	for all : lfsr use entity work.lfsr_with_inner_xors_structural(structural);


	constant TEMP_HEX_DIGITS_BITS_COUNT: natural := HEX_DIGITS_COUNT * 4;
	constant HEX_DIGITS_CLOCK_COUNT: natural := PRIMARY_CLOCK_COUNT / HEX_DIGITS_CLOCK_FREQUENCY;
	constant LFSR_CLOCK_COUNT: natural := PRIMARY_CLOCK_COUNT / LFSR_CLOCK_FREQUENCY;

	signal lfsr_din, lfsr_dout: std_logic_vector(LFSR_BITS_COUNT - 1 downto 0);
	signal lfsr_clk, lfsr_en, lfsr_set, lfsr_reset: std_logic;
	
	signal hex_clk: std_logic;
	signal current_hex_digit_index: integer := 0;
	signal current_hex_digit: std_logic_vector(3 downto 0);
	signal hex_digits_enable: std_logic_vector(HEX_DIGITS_COUNT - 1 downto 0);
	signal temp_hex_digit_bits: std_logic_vector(TEMP_HEX_DIGITS_BITS_COUNT - 1 downto 0);
begin
	-- Actual LFSR
	divide_lfsr_clk: clock_divider
		generic map (BITS => CLOCK_BITS_COUNT, COUNT => LFSR_CLOCK_COUNT)
		port map (clk => clk, reset => reset_button, divided_clk => lfsr_clk);

	lfsr_en <= en_switch;
	lfsr_set <= set_button;
	lfsr_reset <= reset_button;

	lfsr_generator: lfsr
		generic map (N => LFSR_BITS_COUNT, POLYNOMIAL => LFSR_POLYNOMIAL)
		port map (din => lfsr_din, clk => lfsr_clk, en => lfsr_en, set => lfsr_set, reset => lfsr_reset, dout => lfsr_dout);

	assign_lfsr_din: for i in 0 to LFSR_BITS_COUNT - 1 generate
		lfsr_din(i) <= switches(i) when i < SWITCHES_COUNT else '0';
	end generate assign_lfsr_din;

	-- LED output
	assign_leds: for i in leds'range generate
		assign_led: if i < LFSR_BITS_COUNT generate
			leds(i) <= lfsr_dout(i);
		end generate assign_led;
		
		skip_led: if i >= LFSR_BITS_COUNT generate
			leds(i) <= '0';
		end generate skip_led;
	end generate assign_leds;

	-- HEX output
	divide_hex_clk: clock_divider
		generic map (BITS => CLOCK_BITS_COUNT, COUNT => HEX_DIGITS_CLOCK_COUNT)
		port map (clk => clk, reset => reset_button, divided_clk => hex_clk);

	calc_current_hex_digit_index: process (hex_clk, reset_button)
	begin
		if reset_button = '1' then
			current_hex_digit_index <= 0;
		elsif rising_edge(hex_clk) then
			if current_hex_digit_index < HEX_DIGITS_COUNT - 1 then
				current_hex_digit_index <= current_hex_digit_index + 1;
			else
				current_hex_digit_index <= 0;
			end if;
		end if;
	end process calc_current_hex_digit_index;

	assign_temp_hex_digit_bits: for i in temp_hex_digit_bits'range generate
		assign_temp_hex_digit_bits: if i < LFSR_BITS_COUNT generate
			temp_hex_digit_bits(i) <= lfsr_dout(i);
		end generate assign_temp_hex_digit_bits;
		
		skip_temp_hex_digit_bits: if i >= LFSR_BITS_COUNT generate
			temp_hex_digit_bits(i) <= '0';
		end generate skip_temp_hex_digit_bits;
	end generate assign_temp_hex_digit_bits;
	
	assign_current_hex_digit_index: process (current_hex_digit_index)
	begin
		hex_digits_enable <= (others => '0');
		hex_digits_enable(current_hex_digit_index) <= '1';
--		hex_digits_enable <= (current_hex_digit_index => '1', others => '0');
	end process assign_current_hex_digit_index;
	
	hex_digits_en <= not hex_digits_enable;
	
	calc_current_hex_digit: process (temp_hex_digit_bits, current_hex_digit_index)
		variable low_index: integer;
	begin
		low_index := current_hex_digit_index * 4;
		current_hex_digit <= temp_hex_digit_bits(low_index + 3 downto low_index);
	end process calc_current_hex_digit;
	
	encode_hex_digit: seven_segment_encoder
		port map (digit => current_hex_digit, enables => hex_digits_controls);
end architecture behavioral;