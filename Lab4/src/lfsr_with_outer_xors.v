`timescale 1ns / 1ps


module lfsr_with_outer_xors__structural(
		input [N - 1:0] din,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output [N - 1:0] dout
    );

parameter unsigned N = 8;
parameter [N:0] POLYNOMIAL = 'b1;

wire not_set, enable;
wire [N:0] shifting_bits;
wire [N:0] xor_values;

not invert_set(not_set, set);

or calc_enable_bit(enable, en, set);

assign shifting_bits[0] = xor_values[0];

genvar i;

for (i = 1; i <= N; i = i + 1)
begin : shift_bits
	wire shifting_bit, setting_bit;
	wire shift_in_bit;
	
	and calc_setting_bit(setting_bit, set, din[i - 1]);
	
	and calc_shifting_bit(shifting_bit, not_set, shifting_bits[i - 1]);

	or calc_shift_in_bit(shift_in_bit, setting_bit, shifting_bit);
	
	d_flip_flop_ shift_bit(
		.d(shift_in_bit),
		.clk(clk),
		.en(enable),
		.set(0),
		.reset(reset),
		.q(shifting_bits[i])
	);
	
end : shift_bits

for (i = N; i >= 0; i = i - 1)
begin : xor_bits
	case (i)
		0: assign xor_values[i] = (POLYNOMIAL[i] == 1) ? xor_values[i + 1] : 1'b0;
		N: assign xor_values[i] = (POLYNOMIAL[i] == 1) ? shifting_bits[i] : 1'b0;
		default: 
			case (POLYNOMIAL[i])
				0: assign xor_values[i] = xor_values[i + 1];
				1: xor perform_xor(xor_values[i], xor_values[i + 1], shifting_bits[i]);
			endcase
	endcase
end : xor_bits
	
assign dout = shifting_bits[N:1];

endmodule


module lfsr_with_outer_xors__behavioral(
		input [N - 1:0] din,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output [N - 1:0] dout
    );

parameter unsigned N = 8;
parameter [N:0] POLYNOMIAL = 'b1;

reg [N - 1:0] shifting_bits;

reg in_value;

always @(posedge clk or posedge reset)
begin : shift_bits
	if (reset == 1)
		shifting_bits <= 'b0;
	else if (set == 1)
		shifting_bits <= din;
	else if (en == 1)
	begin
		if (POLYNOMIAL[0] == 1)
			in_value = ^(POLYNOMIAL[N:1] & shifting_bits);
		else
			in_value = 1'b0;
		
		shifting_bits <= (N == 2) ? in_value : {shifting_bits[N - 2:0], in_value};
	end
end : shift_bits

assign dout = shifting_bits;

endmodule
