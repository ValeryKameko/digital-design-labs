library ieee;
use ieee.std_logic_1164.ALL;

entity combinational_circuit_tester is
	generic(
		INPUTS: natural := 8;
		SIGNATURE_BITS: natural := 10;
		SEED_POLYNOMIAL: std_logic_vector := x"09B";
		SIGNATURE_POLYNOMIAL: std_logic_vector := x"3D9"
	);
	port(
		seed_din: in std_logic_vector(INPUTS - 1 downto 0);
		seed_dout: out std_logic_vector(INPUTS - 1 downto 0);
		
		signature_din: in std_logic_vector(SIGNATURE_BITS - 1 downto 0);
		signature_dout: out std_logic_vector(SIGNATURE_BITS - 1 downto 0);
		
		circuit_inputs: out std_logic_vector(INPUTS - 1 downto 0);
		circuit_output: in std_logic;

		en, clk: in std_logic;
		
		set_seed, set_signature: in std_logic;
		reset_seed, reset_signature: in std_logic
	);
end entity combinational_circuit_tester;

architecture behavioral of combinational_circuit_tester is
	component lfsr is
		generic(
			N: natural;
			POLYNOMIAL: std_logic_vector
		);
		port(
			din: in std_logic_vector(N - 1 downto 0); 
			clk, en: in std_logic;
			set, reset: in std_logic;
			dout: out std_logic_vector(N - 1 downto 0)
		);
	end component lfsr;
	
	component signature_analyzer is
		generic(
			N: natural;
			POLYNOMIAL: std_logic_vector
		);
		port(
			sin: in std_logic;
			din: in std_logic_vector(N - 1 downto 0); 
			clk, en: in std_logic;
			set, reset: in std_logic;
			dout: out std_logic_vector(N - 1 downto 0)
		);
	end component signature_analyzer;
	
	for all : lfsr 
		use entity work.lfsr_with_outer_xors_structural(structural);
	for all : signature_analyzer 
		use entity work.signature_analyzer_structural(structural);

	signal seed_bits: std_logic_vector(INPUTS - 1 downto 0);
begin	
	seed_inputs: lfsr
		generic map (N => INPUTS, POLYNOMIAL => SEED_POLYNOMIAL)
		port map (din => seed_din, clk => clk, en => en, set => set_seed, reset => reset_seed, dout => seed_bits);
	
	circuit_inputs <= seed_bits;
	seed_dout <= seed_bits;
	
	generate_signature: signature_analyzer
		generic map (N => SIGNATURE_BITS, POLYNOMIAL => SIGNATURE_POLYNOMIAL)
		port map (sin => circuit_output, din => signature_din, clk => clk, en => en, set => set_signature, reset => reset_signature, dout => signature_dout);

end architecture behavioral;
