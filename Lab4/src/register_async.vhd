library ieee;
library bit_memory;
use ieee.std_logic_1164.all;

entity register_async_structural is
	generic(
		N: natural := 8
	);
	port(
		din: in std_logic_vector(N - 1 downto 0);
		en: in std_logic;
		set, reset: in std_logic;
		dout: out std_logic_vector(N - 1 downto 0)
	);
end entity register_async_structural;

architecture structural of register_async_structural is
	component d_latch is
		port(
			d, en: in std_logic;
			set, reset: in std_logic;
			q: out std_logic
		);
	end component d_latch;
begin
	store_bits: for i in din'range generate
		store_bit: d_latch
			port map (d => din(i), en => en, set => set, reset => reset, q => dout(i));
	end generate store_bits;
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity register_async_behavioral is
	generic(
		N: natural := 8
	);
	port(
		din: in std_logic_vector(N - 1 downto 0);
		en: in std_logic;
		set, reset: in std_logic;
		dout: out std_logic_vector(N - 1 downto 0)
	);
end entity register_async_behavioral;

architecture behavioral of register_async_behavioral is
	signal storing_value: std_logic_vector(N - 1 downto 0);
begin
	store_value: process (din, en, set, reset)
	begin
		if reset = '1' then
			storing_value <= (others => '0');
		elsif set = '1' then
			storing_value <= (others => '1');
		elsif en = '1' then
			storing_value <= din;
		end if;
	end process store_value;
	
	dout <= storing_value;
end architecture behavioral;