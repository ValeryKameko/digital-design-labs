library ieee;
use ieee.std_logic_1164.ALL;

entity seven_segment_encoder is
	port(
		digit: in std_logic_vector(3 downto 0);
		enables: out std_logic_vector(6 downto 0)
	);
end entity seven_segment_encoder;

architecture behavioral of seven_segment_encoder is
	component combinational_circuit is
		generic(
			INPUTS: natural;
			OUTPUTS: natural;
			VALUES: std_logic_vector
		);
		port(
			din: in std_logic_vector(INPUTS - 1 downto 0);
			dout: out std_logic_vector(OUTPUTS - 1 downto 0)
		);
	end component combinational_circuit;
	
	-- abcdefg
	constant ENABLES_VALUES: std_logic_vector := (
		b"0111000" &
		b"0110000" &
		b"1000010" &
		b"0110001" &
		b"1100000" &
		b"0000010" &
		b"0000100" &
		b"0000000" &
		b"0001111" &
		b"0100000" &
		b"0100100" &
		b"1001100" &
		b"0000110" &
		b"0010010" &
		b"1001111" &
		b"0000001"
	);
begin
	encode: combinational_circuit
		generic map (INPUTS => 4, OUTPUTS => 7, VALUES => ENABLES_VALUES)
		port map (din => digit, dout => enables);
end architecture behavioral;
