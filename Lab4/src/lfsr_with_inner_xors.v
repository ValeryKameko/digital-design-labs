`timescale 1ns / 1ps


module lfsr_with_inner_xors__structural(
		input [N - 1:0] din,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output [N - 1:0] dout
    );

parameter unsigned N = 7;
parameter [N:0] POLYNOMIAL = 'b1000_0011;

wire not_set, enable;
wire [N:0] shifting_bits;

not invert_set(not_set, set);

or calc_enable(enable, en, set);

assign shifting_bits[0] = shifting_bits[N];

genvar i;

for (i = 0; i <= N; i = i + 1)
begin : shift_bits
	wire setting_bit, shifting_bit;
	wire shift_in_bit, xor_value;

	if (i == 0)
		assign xor_value = (POLYNOMIAL[i] == 1) ? shifting_bits[i] : 0;
	
	if (i != 0 && i != N)
	begin
		if (POLYNOMIAL[i] == 0)
			assign xor_value = shifting_bits[i];
		else
			xor xor_bit(xor_value, shifting_bits[i], shifting_bits[N]);
	end
	
	if (i != N)
	begin
		and calc_setting_bit(setting_bit, set, din[i]);
		
		and calc_shifting_bit(shifting_bit, not_set, xor_value);
		
		or calc_shift_in_bit(shift_in_bit, setting_bit, shifting_bit);
		
		d_flip_flop_ shift_bit(
			.d(shift_in_bit),
			.clk(clk),
			.en(enable),
			.set(0),
			.reset(reset),
			.q(shifting_bits[i + 1])
		);
	end
end : shift_bits
	
assign dout = shifting_bits[N:1];

endmodule


module lfsr_with_inner_xors__behavioral(
		input [N - 1:0] din,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output [N - 1:0] dout
    );

parameter unsigned N = 7;
parameter [N:0] POLYNOMIAL = 'b1000_0011;

reg [N:0] shifting_bits;
reg [N:0] xor_values;
reg in_bit;

integer i;

always @(posedge clk or posedge reset)
begin : shift_bits
	if (reset == 1)
		shifting_bits <= 0;
	else if (set == 1)
		shifting_bits <= {din, 1'b0};
	else if (en == 1)
	begin
		xor_values[0] = 0;
		
		in_bit = POLYNOMIAL[N] & shifting_bits[N];
				
		for (i = 0; i < N; i = i + 1)
			xor_values[i + 1] = shifting_bits[i] ^ (POLYNOMIAL[i] & in_bit);
		
		shifting_bits <= xor_values;
	end
end : shift_bits

assign dout = shifting_bits[N:1];

endmodule
