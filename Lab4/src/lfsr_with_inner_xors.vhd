library ieee;
library bit_memory;
library gates;
use ieee.std_logic_1164.all;
use work.ring_counter_pkg.all;

entity lfsr_with_inner_xors_structural is
	generic(
		N: natural := 8;
		POLYNOMIAL: std_logic_vector := b"1_0000_0011"
	);
	port(
		din: in std_logic_vector(N - 1 downto 0); 
		clk, en: in std_logic;
		set, reset: in std_logic;
		dout: out std_logic_vector(N - 1 downto 0)
	);
end entity lfsr_with_inner_xors_structural;

architecture structural of lfsr_with_inner_xors_structural is
	component d_flip_flop is
		port(
			d, clk, en: in std_logic;
			set, reset: in std_logic;
			q: out std_logic
		);
	end component d_flip_flop;

	component xor2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component xor2;
	
	component and2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component and2;
	
	component or2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component or2;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;

	signal not_set, enable: std_logic;
	signal shifting_bits: std_logic_vector(N downto 0);
begin
	invert_set: not1
		port map (input => set, output => not_set); 

	calc_enable: or2
		port map (inputs => en & set, output => enable);
		
	shifting_bits(0) <= shifting_bits(N);

	shift_bits: for i in 0 to N generate
		signal not_din_i: std_logic;
		signal setting_bit, resetting_bit, need_resetting_bit: std_logic;
		signal xor_value: std_logic;
	begin
		generate_first_xor_value: if i = 0 generate
			xor_value <= shifting_bits(i) when POLYNOMIAL(i) = '1' else '0';
		end generate generate_first_xor_value;
		
		generate_inner_xor_value: if i /= 0 and i /= N generate
			skip_xor: if POLYNOMIAL(i) = '0' generate
				xor_value <= shifting_bits(i);
			end generate skip_xor;

			perform_xor: if POLYNOMIAL(i) = '1' generate
				xor_bit: xor2
					port map (inputs => shifting_bits(i) & shifting_bits(N), output => xor_value);
			end generate perform_xor;
		end generate generate_inner_xor_value;

		generate_not_last_bit: if i /= N generate
			invert_bit: not1
				port map (input => din(i), output => not_din_i);
				
			calc_setting_bit: and2
				port map (inputs => set & din(i), output => setting_bit);
			
			calc_need_resetting_bit: and2
				port map (inputs => set & not_din_i, output => need_resetting_bit);
			
			calc_resetting_bit: or2
				port map (inputs => need_resetting_bit & reset, output => resetting_bit);

			shift_bit: d_flip_flop
				port map (d => xor_value, clk => clk, en => enable, set => setting_bit, reset => resetting_bit, q => shifting_bits(i + 1));
		end generate generate_not_last_bit;
	end generate shift_bits;
	
	dout <= shifting_bits(N downto 1);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;
use work.ring_counter_pkg.all;

entity lfsr_with_inner_xors_behavioral is
	generic(
		N: natural := 8;
		POLYNOMIAL: std_logic_vector := b"1_0000_0011"
	);
	port(
		din: in std_logic_vector(N - 1 downto 0); 
		clk, en: in std_logic;
		set, reset: in std_logic;
		dout: out std_logic_vector(N - 1 downto 0)
	);
end entity lfsr_with_inner_xors_behavioral;

architecture behavioral of lfsr_with_inner_xors_behavioral is
	signal shifting_bits: std_logic_vector(N downto 0);
begin
	shifting_bits(0) <= '0';

	shift_bits: process (en, clk, reset, set, din)
		variable xor_values: std_logic_vector(N downto 0);
		variable in_bit: std_logic;
	begin
		if reset = '1' then
			shifting_bits <= (others => '0');
		elsif set = '1' then
			shifting_bits(N downto 1) <= din;
		elsif rising_edge(clk) and en = '1' then 
			xor_values(0) := '0';
			
			in_bit := POLYNOMIAL(N) and shifting_bits(N);
			
			for i in 0 to N - 1 loop
				xor_values(i + 1) := shifting_bits(i) xor (POLYNOMIAL(i) and in_bit);
			end loop;
			
			shifting_bits <= xor_values;
		end if;
	end process shift_bits;
	
	dout <= shifting_bits(N downto 1);
end architecture behavioral;