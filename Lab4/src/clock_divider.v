`timescale 1ns / 1ps


module clock_divider_(
		input clk,
		input reset,
		output divided_clk
    );

parameter COUNT = 10000;
parameter BITS = 14;

reg clock_value;
reg [BITS - 1:0] counter;

always @(posedge clk or posedge reset)
begin
	if (reset == 1)
	begin
		counter <= 0;
		clock_value <= 0;
	end
	else
	begin
		if (counter == COUNT - 1)
			clock_value <= ~clock_value;
		counter <= (counter + 1) % COUNT;
	end;
end

assign divided_clk = clock_value;

endmodule
