library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock_divider is
	generic(
		BITS: natural := 14;
		COUNT: natural := 10000
	);
	port(
		clk, reset: in std_logic;
		divided_clk: out std_logic
	);
end entity clock_divider;

architecture behavioral of clock_divider is
	signal clock_value: std_logic;
	signal counter: integer;
begin
	perform_count: process (clk, reset)
	begin
		if reset = '1' then
			counter <= 0;
			clock_value <= '0';
		elsif rising_edge(clk) then
			if counter < COUNT - 1 then
				counter <= counter + 1;
			else
				counter <= 0;
				clock_value <= not clock_value;
			end if;
		end if;
	end process perform_count;
	
	divided_clk <= clock_value;
end architecture behavioral;
