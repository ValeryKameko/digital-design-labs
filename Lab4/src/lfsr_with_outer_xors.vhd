library ieee;
library bit_memory;
library gates;
use ieee.std_logic_1164.all;
use work.ring_counter_pkg.all;

entity lfsr_with_outer_xors_structural is
	generic(
		N: natural := 8;
		POLYNOMIAL: std_logic_vector := b"1_0000_0011"
	);
	port(
		din: in std_logic_vector(N - 1 downto 0); 
		clk, en: in std_logic;
		set, reset: in std_logic;
		dout: out std_logic_vector(N - 1 downto 0)
	);
end entity lfsr_with_outer_xors_structural;

architecture structural of lfsr_with_outer_xors_structural is
	component d_flip_flop is
		port(
			d, clk, en: in std_logic;
			set, reset: in std_logic;
			q: out std_logic
		);
	end component d_flip_flop;

	component xor2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component xor2;
	
	component and2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component and2;
	
	component or2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component or2;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	
	signal not_set, enable: std_logic;
	signal shifting_bits: std_logic_vector(N downto 0);
	signal xor_values: std_logic_vector(N downto 0);
begin
	invert_set: not1
		port map (input => set, output => not_set);

	calc_enable_bit: or2
		port map (inputs => en & set, output => enable);

	shifting_bits(0) <= xor_values(0);

	shift_bits: for i in 1 to N generate
		signal shifting_bit, setting_bit: std_logic;
		signal shift_in_bit: std_logic;
	begin
		calc_setting_bit: and2
			port map (inputs => set & din(i - 1), output => setting_bit);
			
		calc_shifting_bit: and2
			port map (inputs => not_set & shifting_bits(i - 1), output => shifting_bit);
		
		calc_in_bit: or2
			port map (inputs => setting_bit & shifting_bit, output => shift_in_bit);
		
		shift_bit: d_flip_flop
			port map(d => shift_in_bit, clk => clk, en => enable, set => '0', reset => reset, q => shifting_bits(i));
	end generate shift_bits;
	
	xor_bits: for i in N downto 0 generate
		connect_last_bit: if i = N generate
			xor_values(i) <= shifting_bits(i) when POLYNOMIAL(i) = '1' else '0';
		end generate connect_last_bit;
		
		calc_inner_bit: if i /= 0 and i /= N generate
			skip_xor_bit: if POLYNOMIAL(i) = '0' generate
				xor_values(i) <= xor_values(i + 1);
			end generate skip_xor_bit;
			
			calc_xor_bit: if POLYNOMIAL(i) = '1' generate
				perform_xor: xor2
					port map (inputs => xor_values(i + 1) & shifting_bits(i), output => xor_values(i));
			end generate calc_xor_bit;
		end generate calc_inner_bit;
		
		connect_first_bit: if i = 0 generate
			xor_values(i) <= xor_values(i + 1) when POLYNOMIAL(i) = '1' else '0';
		end generate connect_first_bit;
	end generate xor_bits;
	
	dout <= shifting_bits(N downto 1);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;
use work.ring_counter_pkg.all;

entity lfsr_with_outer_xors_behavioral is
	generic(
		N: natural := 8;
		POLYNOMIAL: std_logic_vector := b"1_0000_0011"
	);
	port(
		din: in std_logic_vector(N - 1 downto 0); 
		clk, en: in std_logic;
		set, reset: in std_logic;
		dout: out std_logic_vector(N - 1 downto 0)
	);
end entity lfsr_with_outer_xors_behavioral;

architecture behavioral of lfsr_with_outer_xors_behavioral is
	signal shifting_bits: std_logic_vector(N - 1 downto 0);
begin
	shift_bits: process (en, clk, reset, set, din)
		variable xor_value, in_value: std_logic;
	begin
		if reset = '1' then
			shifting_bits <= (others => '0');
		elsif set = '1' then
			shifting_bits <= din;
		elsif rising_edge(clk) and en = '1' then 
			xor_value := '0';
			
			for i in N downto 1 loop
				xor_value := xor_value xor (shifting_bits(i - 1) and POLYNOMIAL(i));
			end loop;
			
			in_value := POLYNOMIAL(0) and xor_value;
		
			shifting_bits <= shifting_bits(N - 2 downto 0) & in_value;
		end if;
	end process shift_bits;
	
	dout <= shifting_bits;
end architecture behavioral;