`timescale 1ns / 1ps


module combinational_circuit_tester_example_(
		input [INPUTS - 1:0] seed_din,
		output [INPUTS - 1:0] seed_dout,
		
		input [SIGNATURE_BITS - 1:0] signature_din,
		output [SIGNATURE_BITS - 1:0] signature_dout,

		output [INPUTS - 1:0] circuit_inputs,
		output circuit_output,

		input en,
		input clk,

		input set_seed,
		input set_signature,
		input reset_seed,
		input reset_signature
    );

parameter INPUTS = 8;
parameter SIGNATURE_BITS = 10;
parameter [INPUTS:0] SEED_POLYNOMIAL = 'h9B;
parameter [SIGNATURE_BITS:0] SIGNATURE_POLYNOMIAL = 'h3D9;
parameter [2 ** INPUTS - 1:0] FUNCTION_VALUES = {
	8'hFC, 8'h45, 8'hC1, 8'h15, 8'h62, 8'h98, 8'hFD, 8'h99, 8'h67, 8'h20, 8'hFD, 8'h03, 8'hBF, 8'h0B, 8'h73, 8'h98,
	8'hE4, 8'h42, 8'hFC, 8'h99, 8'h2B, 8'h80, 8'h7E, 8'h58, 8'h5C, 8'h2D, 8'h76, 8'h51, 8'hAB, 8'h33, 8'hF1, 8'h95
};

wire [INPUTS - 1:0] din;
wire dout;

combinational_circuit_ #(
	.INPUTS(INPUTS), 
	.OUTPUTS(1), 
	.VALUES(FUNCTION_VALUES)
) calc_function(
	.din(din),
	.dout(dout)
);

assign circuit_output = dout;
assign circuit_inputs = din;

combinational_circuit_tester_ #(
	.INPUTS(INPUTS), 
	.SIGNATURE_BITS(SIGNATURE_BITS),
	.SEED_POLYNOMIAL(SEED_POLYNOMIAL),
	.SIGNATURE_POLYNOMIAL(SIGNATURE_POLYNOMIAL)
) test_function(
	.seed_din(seed_din),
	.seed_dout(seed_dout),
	
	.signature_din(signature_din),
	.signature_dout(signature_dout),
	
	.circuit_inputs(din),
	.circuit_output(dout),
	
	.en(en),
	.clk(clk),
	
	.set_seed(set_seed),
	.set_signature(set_signature),
	.reset_seed(reset_seed),
	.reset_signature(reset_signature)
);

endmodule
