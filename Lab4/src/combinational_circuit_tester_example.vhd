library ieee;
use ieee.std_logic_1164.ALL;

entity combinational_circuit_tester_example is
	generic(
		INPUTS: natural := 8;
		SIGNATURE_BITS: natural := 10;
		SEED_POLYNOMIAL: std_logic_vector := x"09B";
		SIGNATURE_POLYNOMIAL: std_logic_vector := x"3D9";
		FUNCTION_VALUES: std_logic_vector := (
			x"AA" & x"E5" & x"F9" & x"05" & x"69" & x"0C" & x"55" & x"9A" & x"81" & x"65" & x"9A" & x"4B" & x"A3" & x"39" & x"A4" & x"EA" &
			x"2C" & x"A0" & x"35" & x"51" & x"67" & x"D9" & x"60" & x"31" & x"43" & x"AA" & x"C4" & x"70" & x"71" & x"2F" & x"22" & x"3F"
		)
	);
	port(
		seed_din: in std_logic_vector(INPUTS - 1 downto 0);
		seed_dout: out std_logic_vector(INPUTS - 1 downto 0);
		
		signature_din: in std_logic_vector(SIGNATURE_BITS - 1 downto 0);
		signature_dout: out std_logic_vector(SIGNATURE_BITS - 1 downto 0);
		
		circuit_inputs: out std_logic_vector(INPUTS - 1 downto 0);
		circuit_output: out std_logic;

		en, clk: in std_logic;
		
		set_seed, set_signature: in std_logic;
		reset_seed, reset_signature: in std_logic
	);
end entity combinational_circuit_tester_example;

architecture behavioral of combinational_circuit_tester_example is
	component combinational_circuit is
		generic(
			INPUTS: natural;
			OUTPUTS: natural;
			VALUES: std_logic_vector
		);
		port(
			din: in std_logic_vector(INPUTS - 1 downto 0);
			dout: out std_logic_vector(OUTPUTS - 1 downto 0)
		);
	end component combinational_circuit;
	
	component combinational_circuit_tester is
		generic(
			INPUTS: natural;
			SIGNATURE_BITS: natural;
			SEED_POLYNOMIAL: std_logic_vector;
			SIGNATURE_POLYNOMIAL: std_logic_vector
		);
		port(
			seed_din: in std_logic_vector(INPUTS - 1 downto 0);
			seed_dout: out std_logic_vector(INPUTS - 1 downto 0);
			
			signature_din: in std_logic_vector(SIGNATURE_BITS - 1 downto 0);
			signature_dout: out std_logic_vector(SIGNATURE_BITS - 1 downto 0);
			
			circuit_inputs: out std_logic_vector(INPUTS - 1 downto 0);
			circuit_output: in std_logic;

			en, clk: in std_logic;
			
			set_seed, set_signature: in std_logic;
			reset_seed, reset_signature: in std_logic
		);
	end component combinational_circuit_tester;

	signal din: std_logic_vector(INPUTS - 1 downto 0);
	signal dout: std_logic;
begin
	calc_function: combinational_circuit
		generic map (INPUTS => INPUTS, OUTPUTS => 1, VALUES => FUNCTION_VALUES)
		port map (din => din, dout(0) => dout);

	circuit_inputs <= din;
	circuit_output <= dout;
	
	test_function: combinational_circuit_tester
		generic map (
			INPUTS => INPUTS, 
			SIGNATURE_BITS => SIGNATURE_BITS, 
			SEED_POLYNOMIAL => SEED_POLYNOMIAL, 
			SIGNATURE_POLYNOMIAL => SIGNATURE_POLYNOMIAL
		)
		port map (
			seed_din => seed_din,
			seed_dout => seed_dout,
			
			signature_din => signature_din,
			signature_dout => signature_dout,
			
			circuit_inputs => din,
			circuit_output => dout,
			
			en => en,
			clk => clk,
			
			set_seed => set_seed,
			set_signature => set_signature,
			reset_seed => reset_seed,
			reset_signature => reset_signature
		);
end architecture behavioral;