library ieee;
library bit_memory;
use ieee.std_logic_1164.all;

entity shift_register_structural is
	generic(
		N: natural := 8
	);
	port(
		sin: in std_logic;
		clk, en: in std_logic;
		set, reset: in std_logic;
		pout: out std_logic_vector(N - 1 downto 0)
	);
end entity shift_register_structural;

architecture structural of shift_register_structural is
	component d_flip_flop is
		port(
			d, clk, en: in std_logic;
			set, reset: in std_logic;
			q: out std_logic
		);
	end component d_flip_flop;
	
	signal shifting_bits: std_logic_vector(N downto 0);
begin
	shifting_bits(0) <= sin;
	
	shift_bits: for i in 1 to N generate
		shift_bit: d_flip_flop
			port map (d => shifting_bits(i - 1), clk => clk, en => en, set => set, reset => reset, q => shifting_bits(i));
	end generate shift_bits;
	
	pout <= shifting_bits(N downto 1);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity shift_register_behavioral is
	generic(
		N: natural := 8
	);
	port(
		sin: in std_logic;
		clk, en: in std_logic;
		set, reset: in std_logic;
		pout: out std_logic_vector(N - 1 downto 0)
	);
end entity shift_register_behavioral;

architecture behavioral of shift_register_behavioral is
	signal shifting_bits: std_logic_vector(N - 1 downto 0);
begin
	shift_bits: process (sin, en, clk, set, reset)
	begin
		if reset = '1' then
			shifting_bits <= (others => '0');
		elsif set = '1' then
			shifting_bits <= (others => '1');
		elsif rising_edge(clk) and en = '1' then
			shifting_bits <= shifting_bits(N - 2 downto 0) & sin;
		end if;
	end process shift_bits;
	
	pout <= shifting_bits;
end architecture behavioral;