`timescale 1ns / 1ps

`ifndef RING_COUNTER_H
`define RING_COUNTER_H

localparam STRAIGHT_RING_COUNTER_TYPE = 1;
localparam JOHNSON_RING_COUNTER_TYPE = 2;

`endif // RING_COUNTER_H

module ring_counter__structural(
		input clk,
		input en,
		
		input reset,
		
		output [N - 1:0] pout
    );

parameter N = 8;
parameter RING_TYPE = JOHNSON_RING_COUNTER_TYPE;

wire [N:0] shifting_bits;
wire need_set_first_bit, need_reset_first_bit;

case (RING_TYPE)
	STRAIGHT_RING_COUNTER_TYPE:
		begin : generate_straight_counter
			assign need_set_first_bit = reset;
			assign shifting_bits[0] = shifting_bits[N];
		end
	JOHNSON_RING_COUNTER_TYPE:
		begin : generate_johnson_counter
			assign need_reset_first_bit = reset;
			not calc_shifting_bit(shifting_bits[0], shifting_bits[N]);
		end
endcase

genvar i;

for (i = 1; i <= N; i = i + 1)
begin : shift_bits		
	wire need_set = (i == 1) ? need_set_first_bit : 0;
	wire need_reset = (i == 1) ? need_reset_first_bit : reset;
	
	d_flip_flop_ shift_bit(
		.d(shifting_bits[i - 1]),
		.clk(clk),
		.en(en),
		.set(need_set),
		.reset(need_reset),
		.q(shifting_bits[i])
	);
end : shift_bits

assign pout = shifting_bits[N:1];

endmodule


module ring_counter__behavioral(
		input clk,
		input en,

		input reset,
		
		output [N - 1:0] pout
    );

parameter N = 8;
parameter RING_TYPE = JOHNSON_RING_COUNTER_TYPE;

reg [N - 1:0] shifting_bits;

always @(posedge clk or posedge reset)
begin : shift_bits
	if (reset == 1)
		case (RING_TYPE)
			STRAIGHT_RING_COUNTER_TYPE: shifting_bits <= 'b1;
			JOHNSON_RING_COUNTER_TYPE: shifting_bits <= 'b0;
		endcase
	else if (en == 1)
	begin
		case (RING_TYPE)
			STRAIGHT_RING_COUNTER_TYPE: shifting_bits <= {shifting_bits[N - 2:0], shifting_bits[N - 1]};
			JOHNSON_RING_COUNTER_TYPE: shifting_bits <= {shifting_bits[N - 2:0], ~shifting_bits[N - 1]};
		endcase
	end
end : shift_bits

assign pout = shifting_bits;

endmodule
