`timescale 1ns / 1ps


module register_sync__structural(
		input [N - 1:0] din,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output [N - 1:0] dout
    );

parameter N = 1;

genvar i;

generate 
	for (i = 0; i < N; i = i + 1)
	begin
		d_flip_flop_ store_bit(
			.d(din[i]),
			.clk(clk),
			.en(en),
			.set(set),
			.reset(reset),
			.q(dout[i])
		);
	end
endgenerate

endmodule


module register_sync__behavioral(
		input [N - 1:0] din,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output [N - 1:0] dout
    );

parameter N = 1;

reg [N - 1:0] storing_value;

always @(posedge clk or posedge set or posedge reset)
begin : store_value
	if (reset == 1)
		storing_value <= {N{1'b0}};
	else if (set == 1)
		storing_value <= {N{1'b1}};
	else if (en == 1)
		storing_value <= din;
end : store_value

assign dout = storing_value;

endmodule
