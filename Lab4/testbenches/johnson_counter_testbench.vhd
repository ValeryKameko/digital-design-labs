library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use work.ring_counter_pkg.all;

entity johnson_counter_testbench is
end entity johnson_counter_testbench;

architecture testbench of johnson_counter_testbench is
	component ring_counter is
		generic(
			N: natural;
			RING_TYPE: RingCounterType
		);
		port(
			clk, en: in std_logic;
			reset: in std_logic;
			pout: out std_logic_vector(N - 1 downto 0)
		);
	end component ring_counter;

	for uut_structural: ring_counter
		use entity work.ring_counter_structural(structural);
	for uut_behavioral: ring_counter
		use entity work.ring_counter_behavioral(behavioral);
	

	constant N: natural := 10;

	signal pout_structural: std_logic_vector(N - 1 downto 0);
	signal pout_behavioral: std_logic_vector(N - 1 downto 0);

	signal clock: std_logic := '0';
	signal clk, en: std_logic;
	signal reset: std_logic;
	
	constant DELAY: time := 10 ns;
begin
	uut_structural: ring_counter
		generic map (N => N, RING_TYPE => Johnson)
		port map (clk => clk, en => en, reset => reset, pout => pout_structural);
	
	uut_behavioral: ring_counter
		generic map (N => N, RING_TYPE => Johnson)
		port map (clk => clk, en => en, reset => reset, pout => pout_behavioral);
	
	clk <= clock;
	
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		-- Reset
		en <= '0'; reset <= '1';
		wait until falling_edge(clock);
		

		-- Check count
		en <= '1'; reset <= '0';
		for i in 0 to N + N - 1 loop
			wait until falling_edge(clock);
			assert pout_structural = pout_behavioral report "Test fail: Check count" severity FAILURE;
		end loop;

		-- Check not count
		en <= '0'; reset <= '1';
		wait until falling_edge(clock);
		en <= '0'; reset <= '0';
		wait until falling_edge(clock);
			assert pout_structural = (others => '0') and
					 pout_behavioral = (others => '0') report "Test fail: Check not count" severity FAILURE;
		
		-- Check reset
		en <= '1'; reset <= '0';
		wait until falling_edge(clock);
		en <= '0'; reset <= '1';
		wait until falling_edge(clock);
			assert pout_structural = (others => '0') and
					 pout_behavioral = (others => '0') report "Test fail: Check reset" severity FAILURE;

				
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
