`timescale 1ns / 1ps
`include "assert.v"


module signature_analyzer__testbench;

wire [N - 1:0] dout_structural;
wire [N - 1:0] dout_behavioral;

reg clock = 0;
wire clk = clock;
reg [N - 1:0] din;
reg sin, en;
reg set, reset;

integer seed = 1234;
localparam time DELAY = 10;
localparam unsigned N = 3;
localparam [N:0] POLYNOMIAL = 'b1011;
localparam [N - 1:0] INITIAL_DIN = 'b000;

localparam unsigned INPUT_SIZE = 8;
localparam [INPUT_SIZE - 1:0] INPUT = 'b11000011;


signature_analyzer__structural #(.N(N), .POLYNOMIAL(POLYNOMIAL)) uut_behavioral(
	.sin(sin),
	.din(din),
	.clk(clk),
	.en(en),
	.set(set),
	.reset(reset),
	.dout(dout_structural)
);

signature_analyzer__behavioral #(.N(N), .POLYNOMIAL(POLYNOMIAL)) uut_structural(
	.sin(sin),
	.din(din),
	.clk(clk),
	.en(en),
	.set(set),
	.reset(reset),
	.dout(dout_behavioral)
);

always
	clock = #(DELAY / 2) ~clock;

integer i;

initial
begin
	// Reset
	{en, set, reset} <= 3'b001;
	@(negedge clock);


	// Check set
	din <= INITIAL_DIN;
	{en, set, reset} <= 3'b010;
	@(negedge clock);
		`assert_(dout_structural == INITIAL_DIN && dout_behavioral == INITIAL_DIN,
			"Test fail: Check set");

	// Check generate
	{en, set, reset} <= 3'b100;
	for (i = 0; i < INPUT_SIZE; i = i + 1)
	begin
		sin = INPUT[i];
		@(negedge clock);
			`assert_(dout_structural == dout_behavioral,
				"Test fail: Check generate");
	end;
	en <= 0;
	@(negedge clock);

	// Check not count
	sin <= 'b1;
	{en, set, reset} <= 3'b001;
	@(negedge clock);
	{en, set, reset} <= 3'b000;
	@(negedge clock);
		`assert_(dout_structural == 'b0 && dout_behavioral == 'b0,
			"Test fail: Check not count");

	// Check reset
	din <= 'b1;
	{en, set, reset} <= 3'b100;
	@(negedge clock);
	{en, set, reset} <= 3'b001;
	@(negedge clock);
		`assert_(dout_structural == 'b0 && dout_behavioral == 'b0,
			"Test fail: Check reset");

	$stop;
end;

endmodule
