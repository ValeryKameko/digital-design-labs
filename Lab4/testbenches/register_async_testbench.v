`timescale 1ns / 1ps
`include "assert.v"


module register_async__testbench;

wire [N - 1:0] dout_structural;
wire [N - 1:0] dout_behavioral;

reg clock = 0;
reg [N - 1:0] din;
reg en;
reg set, reset;

localparam time DELAY = 10;
localparam unsigned N = 10;
integer seed = 123;

register_async__behavioral #(.N(N)) uut_behavioral(
	.din(din),
	.en(en),
	.set(set),
	.reset(reset),
	.dout(dout_behavioral)
);

register_async__structural #(.N(N)) uut_structural(
	.din(din),
	.en(en),
	.set(set),
	.reset(reset),
	.dout(dout_structural)
);

task randomize_din;
	integer i;
begin
	for (i = 0; i < N; i = i + 1)
	begin
		din[i] <= $random(seed) % 2;
	end
end
endtask

always
	clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	{en, set, reset} <= 3'b001; din <= {N{1'b0}};
	@(negedge clock);
	

	// Check store
	randomize_din;
	{en, set, reset} <= 3'b100;
	@(negedge clock);
		`assert_(dout_structural == din && dout_behavioral == din,
			"Test fail: Check store");

	// Check not store
	randomize_din;
	{en, set, reset} <= 3'b100; din <= {N{1'b0}};
	@(negedge clock);
	{en, set, reset} <= 3'b000; din <= {N{1'b1}};
	@(negedge clock);
		`assert_(dout_structural == {N{1'b0}} && dout_behavioral == {N{1'b0}},
			"Test fail: Check not store");

	// Check reset
	randomize_din;
	{en, set, reset} <= 3'b100; din <= {N{1'b1}};
	@(negedge clock);
	{en, set, reset} <= 3'b001;
	@(negedge clock);
		`assert_(dout_structural == {N{1'b0}} && dout_behavioral == {N{1'b0}},
			"Test fail: Check reset");

	// Check set
	randomize_din;
	{en, set, reset} <= 3'b100; din <= {N{1'b0}};
	@(negedge clock);
	{en, set, reset} <= 3'b010;
	@(negedge clock);
		`assert_(dout_structural == {N{1'b1}} && dout_behavioral == {N{1'b1}},
			"Test fail: Check set");	

	$stop;
end;

endmodule
