library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;

entity signature_analyzer_testbench is
end entity signature_analyzer_testbench;

architecture testbench of signature_analyzer_testbench is
	component signature_analyzer is
		generic(
			N: natural;
			POLYNOMIAL: std_logic_vector
		);
		port(
			sin: in std_logic;
			din: in std_logic_vector(N - 1 downto 0);
			clk, en: in std_logic;
			set, reset: in std_logic;
			dout: out std_logic_vector(N - 1 downto 0)
		);
	end component signature_analyzer;

	for uut_structural: signature_analyzer
		use entity work.signature_analyzer_structural(structural);
	for uut_behavioral: signature_analyzer
		use entity work.signature_analyzer_behavioral(behavioral);


	constant N: natural := 3;
	constant POLYNOMIAL: std_logic_vector := b"1011";
	constant INITIAL_DIN: std_logic_vector := b"000";
	
	constant INPUT: std_logic_vector := b"11000011";

	signal dout_structural: std_logic_vector(N - 1 downto 0);
	signal dout_behavioral: std_logic_vector(N - 1 downto 0);

	signal clock: std_logic := '0';
	signal sin: std_logic;
	signal din: std_logic_vector(N - 1 downto 0);
	signal clk, en, set: std_logic;
	signal reset: std_logic;

	constant DELAY: time := 10 ns;
begin
	uut_structural: signature_analyzer
		generic map (N => N, POLYNOMIAL => POLYNOMIAL)
		port map (sin => sin, din => din, clk => clk, en => en, set => set, reset => reset, dout => dout_structural);

	uut_behavioral: signature_analyzer
		generic map (N => N, POLYNOMIAL => POLYNOMIAL)
		port map (sin => sin, din => din, clk => clk, en => en, set => set, reset => reset, dout => dout_behavioral);
	
	clk <= clock;
	
	clock <= not clock after (DELAY / 2);

	test: process
	begin
		-- Reset
		en <= '0'; set <= '0'; reset <= '1';
		wait until falling_edge(clock);

		-- Check set
		din <= (others => '1');
		en <= '0'; set <= '1'; reset <= '0';
		wait until falling_edge(clock);
			assert dout_structural = (din'range => '1') and
					 dout_behavioral = (din'range => '1') report "Test fail: Check set" severity FAILURE;

		-- Check generate
		en <= '1'; set <= '0'; reset <= '0';
		for i in INPUT'low to INPUT'high loop
			sin <= INPUT(i);
			wait until falling_edge(clock);
			assert dout_structural = dout_behavioral report "Test fail: Check generate" severity FAILURE;
		end loop;
		en <= '0';
		wait until falling_edge(clock);

		-- Check not count
		en <= '0'; set <= '0'; reset <= '1';
		wait until falling_edge(clock);
		sin <= '1';
		en <= '0'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
			assert dout_structural = (others => '0') and
					 dout_behavioral = (others => '0') report "Test fail: Check not count" severity FAILURE;
		
		-- Check async reset
		din <= (others => '1');
		en <= '1'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
		en <= '0'; set <= '0'; reset <= '1';
		wait until rising_edge(clock);
			assert dout_structural = (others => '0') and
					 dout_behavioral = (others => '0') report "Test fail: Check async reset" severity FAILURE;
		en <= '0'; set <= '0'; reset <= '1';
		wait until falling_edge(clock);
			assert dout_structural = (others => '0') and
					 dout_behavioral = (others => '0') report "Test fail: Check async reset" severity FAILURE;				


		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
