library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;

package random_pkg is
	procedure random_std_logic_vector(variable seed1, seed2: inout positive; signal vector: out std_logic_vector);
end random_pkg;

package body random_pkg is
	procedure random_std_logic_vector(variable seed1, seed2: inout positive; signal vector: out std_logic_vector) is
		variable r: real;
	begin
		for i in vector'range loop
			uniform(seed1, seed2, r);
			if r > 0.5 then
				vector(i) <= '1';
			else 
				vector(i) <= '0';
			end if;
		end loop;
		wait for 0 ns;
	end procedure random_std_logic_vector; 
end random_pkg;
