library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use work.random_pkg.all;

entity combinational_circuit_tester_example_testbench is
end entity combinational_circuit_tester_example_testbench;

architecture testbench of combinational_circuit_tester_example_testbench is
	component combinational_circuit_tester_example is
		generic(
			INPUTS: natural;
			SIGNATURE_BITS: natural;
			SEED_POLYNOMIAL: std_logic_vector;
			SIGNATURE_POLYNOMIAL: std_logic_vector;
			FUNCTION_VALUES: std_logic_vector
		);
		port(
			seed_din: in std_logic_vector(INPUTS - 1 downto 0);
			seed_dout: out std_logic_vector(INPUTS - 1 downto 0);
			
			signature_din: in std_logic_vector(SIGNATURE_BITS - 1 downto 0);
			signature_dout: out std_logic_vector(SIGNATURE_BITS - 1 downto 0);
			
			circuit_inputs: out std_logic_vector(INPUTS - 1 downto 0);
			circuit_output: out std_logic;

			en, clk: in std_logic;
			
			set_seed, set_signature: in std_logic;
			reset_seed, reset_signature: in std_logic
		);
	end component combinational_circuit_tester_example;

	for uut: combinational_circuit_tester_example
		use entity work.combinational_circuit_tester_example(behavioral);


	constant INPUTS: natural := 5;
	constant SEED_POLYNOMIAL_VALUE: std_logic_vector(2 * 4 - 1 downto 0) := x"25";
	constant SEED_POLYNOMIAL: std_logic_vector(INPUTS downto 0) := SEED_POLYNOMIAL_VALUE(INPUTS downto 0);

	constant SIGNATURE_BITS: natural := 32;
	constant SIGNATURE_POLYNOMIAL_VALUE: std_logic_vector(9 * 4 - 1 downto 0) := x"10FC22F87";
	constant SIGNATURE_POLYNOMIAL: std_logic_vector(SIGNATURE_BITS downto 0) := SIGNATURE_POLYNOMIAL_VALUE(SIGNATURE_BITS downto 0);
	
	constant FUNCTION_VALUES: std_logic_vector(2 ** INPUTS - 1 downto 0) := (
		x"6D" & x"C1" & x"40" & x"0F"
	);

	signal clock: std_logic := '0';
	signal seed_din: std_logic_vector(INPUTS - 1 downto 0) := (others => '0');
	signal seed_dout: std_logic_vector(INPUTS - 1 downto 0);

	signal signature_din: std_logic_vector(SIGNATURE_BITS - 1 downto 0) := (others => '0');
	signal signature_dout: std_logic_vector(SIGNATURE_BITS - 1 downto 0);
	
	signal circuit_inputs: std_logic_vector(INPUTS - 1 downto 0);
	signal circuit_output: std_logic;
	
	signal en, clk: std_logic := '0';
	
	signal set_seed, set_signature: std_logic := '0';
	signal reset_seed, reset_signature: std_logic := '0';

	constant DELAY: time := 10 ns;
	
	signal temp_seed: std_logic_vector(INPUTS - 1 downto 0);
	signal temp_signature: std_logic_vector(SIGNATURE_BITS - 1 downto 0);
begin
	uut: combinational_circuit_tester_example
		generic map (
			INPUTS => INPUTS,
			SIGNATURE_BITS => SIGNATURE_BITS,
			SEED_POLYNOMIAL => SEED_POLYNOMIAL,
			SIGNATURE_POLYNOMIAL => SIGNATURE_POLYNOMIAL,
			FUNCTION_VALUES => FUNCTION_VALUES
		)
		port map (
			seed_din => seed_din,
			seed_dout => seed_dout,

			signature_din => signature_din,
			signature_dout => signature_dout,

			circuit_inputs => circuit_inputs,
			circuit_output => circuit_output,

			en => en,
			clk => clk,

			set_seed => set_seed,
			set_signature => set_signature,

			reset_seed => reset_seed,
			reset_signature => reset_signature
		);
	
	clk <= clock;
	
	clock <= not clock after (DELAY / 2);
	
	test: process
		variable seed1, seed2: integer := 228;
	begin
		-- Reset
		reset_seed <= '1'; reset_signature <= '1';
		wait until falling_edge(clock);
		reset_seed <= '0'; reset_signature <= '0';


		-- Check testing
		set_seed <= '1'; seed_din <= (0 => '1', others => '0');
		set_signature <= '1'; signature_din <= (0 => '1', others => '0');
		wait until falling_edge(clock);
		
		en <= '1'; set_seed <= '0'; set_signature <= '0';
		for i in 0 to 2 ** INPUTS - 1 loop
			wait until falling_edge(clock);
		end loop;
		en <= '0';

		-- Check not testing
		random_std_logic_vector(seed1, seed2, temp_seed);
		random_std_logic_vector(seed1, seed2, temp_signature);
		set_seed <= '1'; seed_din <= temp_seed;
		set_signature <= '1'; signature_din <= temp_signature;
		wait until falling_edge(clock);
		
		set_seed <= '0'; set_signature <= '0';
		wait until falling_edge(clock);
			assert seed_dout = temp_seed and
					 signature_dout = temp_signature report "Test fail: Check not testing" severity FAILURE;

		-- Check set
		reset_seed <= '1'; reset_signature <= '1';
		wait until falling_edge(clock);
		reset_seed <= '0'; reset_signature <= '0';
		
		set_seed <= '1'; seed_din <= (0 => '1', others => '0');
		set_signature <= '1'; signature_din <= (0 => '1', others => '0');
		wait until falling_edge(clock);
		set_seed <= '0'; set_signature <= '0';
			assert seed_dout = seed_din and
					 signature_dout = signature_dout report "Test fail: Check set" severity FAILURE;

		-- Check reset
		set_seed <= '1'; seed_din <= (others => '1');
		set_signature <= '1'; signature_din <= (others => '1');
		wait until falling_edge(clock);
		set_seed <= '0'; set_signature <= '0';
		
		reset_seed <= '1'; reset_signature <= '1';
		wait until falling_edge(clock);
		reset_seed <= '0'; reset_signature <= '0';
			assert seed_dout = (seed_dout'range => '0') and
					 signature_dout = (signature_dout'range => '0') report "Test fail: Check reset" severity FAILURE;


		assert false 
			report "End simulation" 
			severity FAILURE;
	end process test;
end architecture testbench;