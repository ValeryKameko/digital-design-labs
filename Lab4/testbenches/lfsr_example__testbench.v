`timescale 1ns / 1ps
`include "assert.v"


module lfsr_example__testbench;

reg clock = 1'b0;
wire clk = clock;
reg en_switch = 1'b0;
reg set_button = 1'b0;
reg reset_button = 1'b0;
reg [SWITCHES_COUNT - 1:0] switches = 'b0;
wire [LEDS_COUNT - 1:0] leds;
wire [HEX_DIGITS_COUNT - 1:0] hex_digits_en;
wire [6:0] hex_digits_controls;

localparam unsigned SWITCHES_COUNT = 16;
localparam unsigned LEDS_COUNT = 16;
localparam unsigned HEX_DIGITS_COUNT = 8;
localparam unsigned HEX_DIGITS_COUNT_BITS = 3;

localparam unsigned CLOCK_BITS_COUNT = 32;
localparam unsigned PRIMARY_CLOCK_COUNT = 100_000_000;
localparam unsigned HEX_DIGITS_CLOCK_FREQUENCY = 5_000_000;
localparam unsigned LFSR_CLOCK_FREQUENCY = 100_000_000;

localparam unsigned LFSR_BITS_COUNT = 14;
localparam [LFSR_BITS_COUNT:0] LFSR_POLYNOMIAL = 'b100_0000_0001_0100_0011;

localparam DELAY = 20;

lfsr_example_ #(
	.SWITCHES_COUNT(SWITCHES_COUNT),
	.LEDS_COUNT(LEDS_COUNT),
	.HEX_DIGITS_COUNT(HEX_DIGITS_COUNT),
	.HEX_DIGITS_COUNT_BITS(HEX_DIGITS_COUNT_BITS),
	.PRIMARY_CLOCK_COUNT(PRIMARY_CLOCK_COUNT),
	.HEX_DIGITS_CLOCK_FREQUENCY(HEX_DIGITS_CLOCK_FREQUENCY),
	.LFSR_CLOCK_FREQUENCY(LFSR_CLOCK_FREQUENCY),
	.LFSR_BITS_COUNT(LFSR_BITS_COUNT),
	.LFSR_POLYNOMIAL(LFSR_POLYNOMIAL)
) uut(
	.clk(clk),
	.en_switch(en_switch),
	.set_button(set_button),
	.reset_button(reset_button),
	.switches(switches),
	.leds(leds),
	.hex_digits_en(hex_digits_en),
	.hex_digits_controls(hex_digits_controls)
);

always
	clock = #(DELAY / 2) ~clock;

integer i;

initial
begin
	// Reset
	reset_button <= 1'b1;
	@(negedge clock);
	reset_button <= 1'b0;


	// Check reset
	set_button <= 1'b1; 
	switches <= {'b0, 1'b1};
	@(negedge clock);
	set_button <= 1'b0;
	
	reset_button <= 1'b1;
	@(negedge clock);
	reset_button <= 1'b0;
		`assert_(leds == 0,
			"Test fail: Check reset");

	// Check set
	reset_button <= 1'b1;
	@(negedge clock);
	reset_button <= 1'b0;
	
	set_button <= 1'b1; 
	switches <= {SWITCHES_COUNT{1'b1}};
	@(negedge clock);

	// Check work
	set_button <= 1'b1; 
	switches <= {'b0, 1'b1};
	@(negedge clock);
	set_button <= 1'b0;
	
	en_switch <= 1'b1;
	for (i = 0; i < 2 ** LFSR_BITS_COUNT; i = i + 1)
		 @(negedge clock);
	en_switch <= 1'b0;


	$stop;
end;

endmodule
