`timescale 1ns / 1ps
`include "assert.v"


module combinational_circuit_tester_example__testbench;

reg [INPUTS - 1:0] seed_din = 'b0;
wire [INPUTS - 1:0] seed_dout;

reg [SIGNATURE_BITS - 1:0] signature_din = 'b0;
wire [SIGNATURE_BITS - 1:0] signature_dout;

wire [INPUTS - 1:0] circuit_inputs;
wire circuit_output;

reg clock = 0;
reg en = 0;
wire clk = clock;

reg set_seed = 0, set_signature = 0;
reg reset_seed = 0, reset_signature = 0;

localparam INPUTS = 5;
localparam [INPUTS:0] SEED_POLYNOMIAL = 'h25;
localparam SIGNATURE_BITS = 32;
localparam [SIGNATURE_BITS:0] SIGNATURE_POLYNOMIAL = 'h10FC22F87;
localparam [2 ** INPUTS - 1:0] FUNCTION_VALUES = {
	8'h6D, 8'hC1, 8'h40, 8'h0F
};

reg [INPUTS - 1:0] temp_seed;
reg [SIGNATURE_BITS - 1:0] temp_signature;
localparam time DELAY = 10;
integer seed = 123;

task randomize_temp;
	integer i;
begin
	for (i = 0; i < INPUTS; i = i + 1)
		temp_seed[i] = $random(seed) % 2;
	for (i = 0; i < SIGNATURE_BITS; i = i + 1)
		temp_signature[i] = $random(seed) % 2;
end
endtask


combinational_circuit_tester_example_ #(
	.INPUTS(INPUTS),
	.SIGNATURE_BITS(SIGNATURE_BITS),
	.SEED_POLYNOMIAL(SEED_POLYNOMIAL),
	.SIGNATURE_POLYNOMIAL(SIGNATURE_POLYNOMIAL),
	.FUNCTION_VALUES(FUNCTION_VALUES)
) uut(
	.seed_din(seed_din),
	.seed_dout(seed_dout),

	.signature_din(signature_din),
	.signature_dout(signature_dout),

	.circuit_inputs(circuit_inputs),
	.circuit_output(circuit_output),

	.en(en),
	.clk(clk),

	.set_seed(set_seed),
	.set_signature(set_signature),

	.reset_seed(reset_seed),
	.reset_signature(reset_signature)
);

integer i;

always
	clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	{reset_seed, reset_signature} <= 'b11;
	@(negedge clock);
	{reset_seed, reset_signature} <= 'b00;


	// Check testing
	set_seed <= 1; seed_din <= 'b1;
	set_signature <= 1; signature_din <= 'b1;
	@(negedge clock);
	{en, set_seed, set_signature} <= 'b100;
	for (i = 0; i < 2 ** INPUTS; i = i + 1)
		 @(negedge clock);
	en <= 'b0;

	// Check not test
	randomize_temp;
	{set_seed, seed_din, set_signature, signature_din} <= {1'b1, temp_seed, 1'b1, temp_signature};
	@(negedge clock);
	{set_seed, set_signature} <= 'b00;
	@(negedge clock);
		`assert_(seed_dout == temp_seed && signature_dout == temp_signature,
			"Test fail: Check not test");

	// Check set
	{reset_seed, reset_signature} <= 'b11;
	@(negedge clock);
	{reset_seed, reset_signature} <= 'b00;
	{set_seed, seed_din, set_signature, signature_din} <= {1'b1, {INPUTS{1'b1}}, 1'b1, {SIGNATURE_BITS{1'b1}}};
	@(negedge clock);
	{set_seed, set_signature} <= 'b00;
		`assert_(seed_dout == {INPUTS{1'b1}} && signature_dout == {SIGNATURE_BITS{1'b1}},
			"Test fail: Check set");
	
	// Check reset
	{set_seed, seed_din, set_signature, signature_din} <= {1'b1, {INPUTS{1'b1}}, 1'b1, {SIGNATURE_BITS{1'b1}}};
	@(negedge clock);
	{set_seed, set_signature} <= 'b00;
	{reset_seed, reset_signature} <= 'b11;
	@(negedge clock);
	{reset_seed, reset_signature} <= 'b00;
		`assert_(seed_dout == 'b0 && signature_dout == 'b0,
			"Test fail: Check reset");


	$stop;
end;

endmodule
