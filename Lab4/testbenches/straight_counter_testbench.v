`timescale 1ns / 1ps
`include "assert.v"


module straight_counter__testbench;

wire [N - 1:0] pout_structural;
wire [N - 1:0] pout_behavioral;

reg clock = 0;
wire clk = clock;
reg en;
reg reset;

localparam time DELAY = 10;
localparam unsigned N = 10;

ring_counter__behavioral #(.N(N), .RING_TYPE(STRAIGHT_RING_COUNTER_TYPE)) uut_behavioral(
	.clk(clk),
	.en(en),
	.reset(reset),
	.pout(pout_behavioral)
);

ring_counter__structural #(.N(N), .RING_TYPE(STRAIGHT_RING_COUNTER_TYPE)) uut_structural(
	.clk(clk),
	.en(en),
	.reset(reset),
	.pout(pout_structural)
);

always
	clock = #(DELAY / 2) ~clock;

integer i;

initial
begin
	// Reset
	{en, reset} <= 2'b01;
	@(negedge clock);
	

	// Check count
	{en, reset} <= 3'b10;
	for (i = 0; i < N; i = i + 1)
	begin
		 @(negedge clock);
		 `assert_(pout_structural == pout_behavioral,
			"Test fail: Check count");
	end;
	
	// Check not count
	{en, reset} <= 2'b01;
	@(negedge clock);
	{en, reset} <= 2'b00;
	@(negedge clock);
		`assert_(pout_structural == 'b1 && pout_behavioral == 'b1,
			"Test fail: Check not count");

	// Check reset
	{en, reset} <= 2'b10;
	@(negedge clock);
	{en, reset} <= 2'b01;
	@(negedge clock);
		`assert_(pout_structural == 'b1 && pout_behavioral == 'b1,
			"Test fail: Check reset");


	$stop;
end;

endmodule

