library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use work.random_pkg.all;

entity shift_register_testbench is
end entity shift_register_testbench;

architecture testbench of shift_register_testbench is
	component shift_register is
		generic(
			N: natural
		);
		port(
			sin: in std_logic;
			clk, en: in std_logic;
			set, reset: in std_logic;
			pout: out std_logic_vector(N - 1 downto 0)
		);
	end component shift_register;

	
	for uut_structural: shift_register
		use entity work.shift_register_structural(structural);
	for uut_behavioral: shift_register
		use entity work.shift_register_behavioral(behavioral);
	

	constant N: natural := 10;

	signal pout_structural: std_logic_vector(N - 1 downto 0);
	signal pout_behavioral: std_logic_vector(N - 1 downto 0);
	
	signal clock: std_logic := '0';
	signal test_bits: std_logic_vector(N - 1 downto 0);
	signal sin: std_logic;
	signal clk, en: std_logic;
	signal set, reset: std_logic;
	
	constant DELAY: time := 10 ns;
begin
	uut_structural: shift_register
		generic map (N => N)
		port map (sin => sin, clk => clk, en => en, set => set, reset => reset, pout => pout_structural);
	
	uut_behavioral: shift_register
		generic map (N => N)
		port map (sin => sin, clk => clk, en => en, set => set, reset => reset, pout => pout_behavioral);
	
	clk <= clock;
	
	clock <= not clock after (DELAY / 2);
	
	test: process
		variable seed1, seed2: integer := 228;
	begin
		-- Reset
		en <= '0'; set <= '0'; reset <= '1';
		wait until falling_edge(clock);
		

		-- Check shift bits
		random_std_logic_vector(seed1, seed2, test_bits);
		en <= '1'; set <= '0'; reset <= '0';
		for i in test_bits'range loop
			sin <= test_bits(i);
			wait until falling_edge(clock);
		end loop;
			assert pout_structural = test_bits and
					 pout_behavioral = test_bits report "Test fail: Check shift bits" severity FAILURE;

		-- Check not shift
		en <= '0'; set <= '0'; reset <= '1'; sin <= '0';
		wait until falling_edge(clock);
		en <= '0'; set <= '0'; reset <= '0'; sin <= '1';
		wait until falling_edge(clock);
			assert pout_structural = (others => '0') and
					 pout_behavioral = (others => '0') report "Test fail: Check not shift" severity FAILURE;

		-- Check reset
		en <= '1'; set <= '0'; reset <= '0'; sin <= '1';
		wait until falling_edge(clock);
		en <= '0'; set <= '0'; reset <= '1'; sin <= '0';
		wait until falling_edge(clock);
			assert pout_structural = (others => '0') and
					 pout_behavioral = (others => '0') report "Test fail: Check reset" severity FAILURE;

		-- Check set
		en <= '1'; set <= '0'; reset <= '0'; sin <= '0';
		wait until falling_edge(clock);
		en <= '0'; set <= '1'; reset <= '0'; sin <= '0';
		wait until falling_edge(clock);
			assert pout_structural = (others => '1') and
					 pout_behavioral = (others => '1') report "Test fail: Check set" severity FAILURE;

				
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
