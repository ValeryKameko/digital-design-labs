`timescale 1ns / 1ps
`include "assert.v"


module shift_register__testbench;

wire [N - 1:0] pout_structural;
wire [N - 1:0] pout_behavioral;

reg [N - 1:0] test_bits;

reg clock = 0;
wire clk = clock;
reg sin;
reg en;
reg set, reset;

localparam time DELAY = 10;
localparam unsigned N = 10;
integer seed = 123;

shift_register__behavioral #(.N(N)) uut_behavioral(
	.sin(sin),
	.clk(clk),
	.en(en),
	.set(set),
	.reset(reset),
	.pout(pout_behavioral)
);

shift_register__structural #(.N(N)) uut_structural(
	.sin(sin),
	.clk(clk),
	.en(en),
	.set(set),
	.reset(reset),
	.pout(pout_structural)
);

task randomize_test_bits;
	integer i;
begin
	for (i = 0; i < N; i = i + 1)
	begin
		test_bits[i] = $random(seed) % 2;
	end
end
endtask

always
	clock = #(DELAY / 2) ~clock;

integer i;

initial
begin
	// Reset
	{en, set, reset} <= 3'b001;
	@(negedge clock);
	

	// Check shift bits
	randomize_test_bits;
	{en, set, reset} <= 3'b100;
	for (i = N - 1; i >= 0; i = i - 1)
	begin
		 sin <= test_bits[i];
		 @(negedge clock);
	end;
		`assert_(pout_structural == test_bits && pout_behavioral == test_bits,
			"Test fail: Check shift bits");
	
	// Check not shift
	{en, set, reset, sin} <= 4'b0010;
	@(negedge clock);
	{en, set, reset, sin} <= 4'b0001;
	@(negedge clock);
		`assert_(pout_structural == {N{1'b0}} && pout_behavioral == {N{1'b0}},
			"Test fail: Check not shift");

	// Check reset
	{en, set, reset, sin} <= 4'b1001;
	@(negedge clock);
	{en, set, reset, sin} <= 4'b0010;
	@(negedge clock);
		`assert_(pout_structural == {N{1'b0}} && pout_behavioral == {N{1'b0}},
			"Test fail: Check reset");

	// Check set
	{en, set, reset, sin} <= 4'b1000;
	@(negedge clock);
	{en, set, reset, sin} <= 4'b0100;
	@(negedge clock);
		`assert_(pout_structural == {N{1'b1}} && pout_behavioral == {N{1'b1}},
			"Test fail: Check set");

	$stop;
end;

endmodule
