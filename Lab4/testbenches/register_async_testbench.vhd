library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use work.random_pkg.all;

entity register_async_testbench is
end entity register_async_testbench;

architecture testbench of register_async_testbench is
	component register_async is
		generic(
			N: natural := 1
		);
		port(
			din: in std_logic_vector(N - 1 downto 0);
			en: in std_logic;
			set, reset: in std_logic;
			dout: out std_logic_vector(N - 1 downto 0)
		);
	end component register_async;

	
	for uut_structural: register_async
		use entity work.register_async_structural(structural);
	for uut_behavioral: register_async
		use entity work.register_async_behavioral(behavioral);
	

	constant N: natural := 10;

	signal dout_structural: std_logic_vector(N - 1 downto 0);
	signal dout_behavioral: std_logic_vector(N - 1 downto 0);
	
	signal clock: std_logic := '0';
	signal din: std_logic_vector(N - 1 downto 0);
	signal en: std_logic;
	signal set, reset: std_logic;
	
	constant DELAY: time := 10 ns;
begin
	uut_structural: register_async 
		generic map (N => N)
		port map (din => din, en => en, set => set, reset => reset, dout => dout_structural);
	
	uut_behavioral: register_async 
		generic map (N => N)
		port map (din => din, en => en, set => set, reset => reset, dout => dout_behavioral);
	
	clock <= not clock after (DELAY / 2);
	
	test: process
		variable seed1, seed2: integer := 228;
	begin
		-- Reset
		en <= '0';set <= '0'; reset <= '1'; din <= (others => '0');
		wait until falling_edge(clock);
		

		-- Check store
		random_std_logic_vector(seed1, seed2, din);
		en <= '1'; set <= '0'; reset <= '0'; 
		wait until falling_edge(clock);
			assert dout_structural = din and
					 dout_behavioral = din report "Test fail: Check store" severity FAILURE;
		
		-- Check not store
		en <= '1'; set <= '0'; reset <= '0'; din <= (others => '0');
		wait until falling_edge(clock);
		en <= '0'; set <= '0'; reset <= '0'; din <= (others => '1');
		wait until falling_edge(clock);
			assert dout_structural /= (din'range => '1') and
					 dout_behavioral /= (din'range => '1') report "Test fail: Check not store" severity FAILURE;
		
		-- Check reset
		en <= '1'; set <= '0'; reset <= '0'; din <= (others => '1');
		wait until falling_edge(clock);
		en <= '0'; set <= '0'; reset <= '1';
		wait until falling_edge(clock);
			assert dout_structural = (din'range => '0') and
					 dout_behavioral = (din'range => '0') report "Test fail: Check reset" severity FAILURE;
		
		-- Check set
		en <= '1'; set <= '0'; reset <= '0'; din <= (others => '0');
		wait until falling_edge(clock);
		en <= '0'; set <= '1'; reset <= '0';
		wait until falling_edge(clock);
			assert dout_structural = (din'range => '1') and
					 dout_behavioral = (din'range => '1') report "Test fail: Check set" severity FAILURE;
	

		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
