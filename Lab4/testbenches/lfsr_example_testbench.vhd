library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;

entity lfsr_example_testbench is
end entity lfsr_example_testbench;

architecture testbench of lfsr_example_testbench is
	component lfsr_example is
		generic(
			SWITCHES_COUNT: natural;
			LEDS_COUNT: natural;
			HEX_DIGITS_COUNT: natural;
			HEX_DIGITS_COUNT_BITS: natural;
			CLOCK_BITS_COUNT: natural;
			PRIMARY_CLOCK_COUNT: natural;
			HEX_DIGITS_CLOCK_FREQUENCY: natural;
			LFSR_CLOCK_FREQUENCY: natural;
			LFSR_BITS_COUNT: natural;
			LFSR_POLYNOMIAL: std_logic_vector
		);
		port(
			clk, en_switch: in std_logic;
			set_button, reset_button: in std_logic;
			switches: in std_logic_vector(SWITCHES_COUNT - 1 downto 0);
			leds: out std_logic_vector(LEDS_COUNT - 1 downto 0);
			hex_digits_en: out std_logic_vector(HEX_DIGITS_COUNT - 1 downto 0);
			hex_digits_controls: out std_logic_vector(6 downto 0)
		);
	end component lfsr_example;
	
	constant SWITCHES_COUNT: natural := 16;
	constant LEDS_COUNT: natural := 16;
	constant HEX_DIGITS_COUNT: natural := 8;
	constant HEX_DIGITS_COUNT_BITS: natural := 3;

	constant CLOCK_BITS_COUNT: natural := 32;
	constant PRIMARY_CLOCK_COUNT: natural := 100_000_000;
	constant HEX_DIGITS_CLOCK_FREQUENCY: natural := 5_000_000;
	constant LFSR_CLOCK_FREQUENCY: natural := 100_000_000;
	
	constant LFSR_BITS_COUNT: natural := 14;
	constant LFSR_POLYNOMIAL: std_logic_vector := b"100_0000_0001_0100_0011";	
	
	signal clock: std_logic := '0';
	signal clk: std_logic;
	signal en_switch: std_logic := '0';
	signal set_button, reset_button: std_logic := '0';
	signal switches: std_logic_vector(SWITCHES_COUNT - 1 downto 0) := (others => '0');
	
	signal hex_digits_en: std_logic_vector(HEX_DIGITS_COUNT - 1 downto 0);
	signal hex_digits_controls: std_logic_vector(7 - 1 downto 0);
	signal leds: std_logic_vector(LEDS_COUNT - 1 downto 0);

	constant DELAY: time := 20 ns;
begin
	uut: lfsr_example
		generic map (
			SWITCHES_COUNT => SWITCHES_COUNT,
			LEDS_COUNT => LEDS_COUNT,
			HEX_DIGITS_COUNT => HEX_DIGITS_COUNT,
			HEX_DIGITS_COUNT_BITS => HEX_DIGITS_COUNT_BITS,
			CLOCK_BITS_COUNT => CLOCK_BITS_COUNT,
			PRIMARY_CLOCK_COUNT => PRIMARY_CLOCK_COUNT,
			HEX_DIGITS_CLOCK_FREQUENCY => HEX_DIGITS_CLOCK_FREQUENCY,
			LFSR_CLOCK_FREQUENCY => LFSR_CLOCK_FREQUENCY,
			LFSR_BITS_COUNT => LFSR_BITS_COUNT,
			LFSR_POLYNOMIAL => LFSR_POLYNOMIAL
		)
		port map (
			clk => clk,
			en_switch => en_switch,
			set_button => set_button,
			reset_button => reset_button,
			switches => switches,
			leds => leds,
			hex_digits_en => hex_digits_en,
			hex_digits_controls => hex_digits_controls
		);
	
	clk <= clock;
	
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin	
		-- Reset
		reset_button <= '1';
		wait until falling_edge(clock);
		reset_button <= '0';

		-- Check work
		set_button <= '1';
		switches <= (0 => '1', others => '0');
		wait until falling_edge(clock);
		set_button <= '0';
		
		en_switch <= '1';
		for i in 0 to 2 ** LFSR_BITS_COUNT loop
			wait until falling_edge(clock);
		end loop;
		en_switch <= '0';
		
		-- Check reset
		set_button <= '1';
		switches <= (others => '1');
		wait until falling_edge(clock);
		set_button <= '0';
		
		reset_button <= '1';
		wait until falling_edge(clock);
		reset_button <= '0';
			assert leds = (others => '0') and
					 hex_digits_en = (0 => '0', others => '1')
					 report "Test fail: Check reset" severity FAILURE;
		
		
		-- Check set
		reset_button <= '1';
		wait until falling_edge(clock);
		reset_button <= '0';
		
		set_button <= '1';
		switches <= (others => '1');
		wait until falling_edge(clock);
		set_button <= '0';
			assert leds = (switches'range => '1', others => '0')
					 report "Test fail: Check set" severity FAILURE;
		
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
