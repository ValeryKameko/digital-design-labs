#!/usr/bin/python3

import re

polynomial_string = input("POLYNOMIAL: ")

elements = re.split(r"\w*\+\w*", polynomial_string)

mask = 0
for element in elements:
    i = re.findall(r"\d+", element)[0]
    if "x" in element:
        mask += 2 ** int(i)
    else:
        mask += 1

hexdigits = f"{mask:X}"
hexdigits_count = len(hexdigits)
hexdigits_count += hexdigits_count % 2

print("'h" + hexdigits.rjust(hexdigits_count, '0'))

print("")

print("x\"" + hexdigits.rjust(hexdigits_count, '0') + "\"")
