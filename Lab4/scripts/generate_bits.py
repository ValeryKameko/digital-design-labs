import random
	
N = int(input("N: "))

number = random.randint(0, 2 ** N - 1)

bs = number.to_bytes((N + 7) // 8, byteorder='big')

lines = [', '.join([f"8'h{b:02X}" for b in bs[line:line + 16]]) for line in range(0, len(bs), 16)]

print(',\n'.join(lines))

print("")

lines = [' & '.join([f"x\"{b:02X}\"" for b in bs[line:line + 16]]) for line in range(0, len(bs), 16)]

print(' &\n'.join(lines))
