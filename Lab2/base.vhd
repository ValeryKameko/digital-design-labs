library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity and2 is
	port(
		in1, in2: in std_logic;
		output: out std_logic
	);
end and2;

architecture Behavioral of and2 is
begin
	output <= in1 and in2;
end Behavioral;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity and3 is
	port(
		in1, in2, in3: in std_logic;
		output: out std_logic
	);
end and3;

architecture Behavioral of and3 is
begin
	output <= in1 and in2 and in3;
end Behavioral;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity or2 is
	port(
		in1, in2: in std_logic;
		output: out std_logic
	);
end or2;


architecture Behavioral of or2 is
begin
	output <= in1 or in2;
end Behavioral;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity xor2 is
	port(
		in1, in2: in std_logic;
		output: out std_logic
	);
end xor2;


architecture Behavioral of xor2 is
begin
	output <= in1 xor in2;
end Behavioral;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity not1 is
	port(
		input: in std_logic;
		output: out std_logic
	);
end not1;

architecture Behavioral of not1 is
begin
	output <= not input;
end Behavioral;

