`define assert_(expression, message) \
	if ((expression) !== 1) \
	begin \
		$display((message)); $stop; \
	end;
