`timescale 1ns / 1ps
`include "assert.v"


module adderN__testbench;

localparam unsigned N = 2;
localparam unsigned BITS_COUNT = N * 2 + 1;
localparam time DELAY = 10;

reg clock = 0;
reg [BITS_COUNT - 1:0] inputs = 0;

wire [N - 1:0] a = inputs[N - 1:0];
wire [N - 1:0] b = inputs[2 * N - 1:N];
wire in_carry = inputs[2 * N];

wire [N:0] sum_structural;
wire [N:0] sum_behavioral;

adderN__structural #(.N(N)) uut_structural(
	.a(a),
	.b(b),
	.in_carry(in_carry),
	.sum(sum_structural)
);

adderN__behavioral #(.N(N)) uut_behavioral(
	.a(a),
	.b(b),
	.in_carry(in_carry),
	.sum(sum_behavioral)
);

always
	#(DELAY / 2) clock = ~clock;

integer i;
initial
begin
	inputs = 'b0;
	@(negedge clock);

	for (i = 0; i < 2 ** BITS_COUNT; i = i + 1)
	begin
		@(negedge clock);
		
		`assert_(sum_structural == sum_behavioral,
			"Test fail: Check add");
		inputs = inputs + 1;
	end;
	$stop;
end;

endmodule
