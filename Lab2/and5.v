`timescale 1ns / 1ps


module and5__behavioral(
		input [4:0] ins,
		output out
    );

assign out = (ins == 5'b11111) ? 1 : 0;

endmodule


module and5__structural(
		input [4:0] ins,
		output out
    );

wire [5:0] partials;

assign partials[0] = 1'b1;

genvar i;
for (i = 0; i <= 4; i = i + 1) 
	and perform_and(partials[i + 1], partials[i], ins[i]);

assign out = partials[5];

endmodule
