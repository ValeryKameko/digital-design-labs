library ieee;
use ieee.std_logic_1164.all;

entity expression_6_behavioral is
	port(
		w, x, y, z: in std_logic;
		f: out std_logic
	);
end entity expression_6_behavioral;

architecture behavioral of expression_6_behavioral is
	signal partials: std_logic_vector(2 downto 0);
	
	constant partial_ones: std_logic_vector(partials'range) := (others => '1');
begin
	partials(0) <= ((not w) or x) and y;
	partials(1) <= (not w) or x or (not y);
	partials(2) <= w or z;
	
	f <= '1' when partials = partial_ones else '0';
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity expression_6_structural is
	port(
		w, x, y, z: in std_logic;
		f: out std_logic
	);
end entity expression_6_structural;

architecture structural of expression_6_structural is
	component nandN is
		generic(
			N: positive
		);
		port(
			ins: in std_logic_vector(N - 1 downto 0);
			output: out std_logic
		);
	end component nandN;
	
	component norN is
		generic(
			N: positive
		);
		port(
			ins: in std_logic_vector(N - 1 downto 0);
			output: out std_logic
		);
	end component norN;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	
	signal not_x, not_w, not_y: std_logic;
	signal intermediate: std_logic;
	signal partials: std_logic_vector(2 downto 0);
begin
	invert_x: not1
		port map(input => x, output => not_x);
		
	invert_w: not1
		port map(input => w, output => not_w);
	
	invert_y: not1
		port map(input => y, output => not_y);
	
	calc_intermediate: nandN
		generic map(N => 2)
		port map(ins => w & not_x, output => intermediate);
	
	calc_partials_0: nandN
		generic map(N => 2)
		port map(ins => intermediate & y, output => partials(0));
	
	calc_partials_1: norN
		generic map(N => 3)
		port map(ins => not_w & x & not_y, output => partials(1));
	
	calc_partials_2: norN
		generic map(N => 2)
		port map(ins => w & z, output => partials(2));
		
	calc_f: norN
		generic map(N => 3)
		port map(ins => partials, output => f);
	
end architecture structural;