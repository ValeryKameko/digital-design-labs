library ieee;
use ieee.std_logic_1164.all;

entity and5_behavioral is
	port(
		ins: in std_logic_vector(4 downto 0);
		output: out std_logic
	);
end entity and5_behavioral;

architecture behavioral of and5_behavioral is
begin
	output <= '1' when ins = "11111" else '0'; 
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity and5_structural is
	port(
		ins: in std_logic_vector(4 downto 0);
		output: out std_logic
	);
end entity and5_structural;

architecture structural of and5_structural is
	component and2 is
		port(
			in1, in2: in std_logic;
			output: out std_logic
		);
	end component and2;
	
	signal partials: std_logic_vector((ins'high + 1) downto 0);
begin
	partials(0) <= '1';
	
	perform_ands: for i in ins'range generate
		perform_and: and2
			port map(in1 => ins(i), in2 => partials(i), output => partials(i + 1));
	end generate perform_ands;
	
	output <= partials(5);
end architecture structural;