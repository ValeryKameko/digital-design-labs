`timescale 1ns / 1ps


module universal_truth_table_(
		input [N - 1:0] ins,
		output out
    );

parameter N = 3;
parameter [2 ** N - 1:0] UTT_TABLE = 'h80;

assign out = UTT_TABLE[ins];

endmodule
