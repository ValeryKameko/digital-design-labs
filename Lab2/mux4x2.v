`timescale 1ns / 1ps


module mux4x2__behavioral(
		input [1:0] a,
		input [1:0] b,
		input s,
		
		output [1:0] z
    );

assign z = s ? b : a;

endmodule


module mux4x2__structural(
		input [1:0] a,
		input [1:0] b,
		input s,
		
		output [1:0] z
    );

mux2x1__structural mux2x1_0(
	.a(a[0]),
	.b(b[0]),
	.s(s),
	.z(z[0])
);

mux2x1__structural mux2x1_1(
	.a(a[1]),
	.b(b[1]),
	.s(s),
	.z(z[1])
);

endmodule


