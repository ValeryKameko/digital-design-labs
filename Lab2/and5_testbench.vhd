library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity and5_testbench is
end entity and5_testbench;

architecture testbench of and5_testbench is
	component and5 is
		port(
			ins: in std_logic_vector(4 downto 0);
			output: out std_logic
		);
	end component and5;
	
	for uut_structural: and5 use entity work.and5_structural(structural);
	for uut_behavioral: and5 use entity work.and5_behavioral(behavioral);
	
	constant delay: time := 10 ns;
	
	signal clock: std_logic := '0';
	signal ins: std_logic_vector(4 downto 0) := (others => '0');

	signal output_behavioral: std_logic;
	signal output_structural: std_logic;
begin
	uut_structural: and5
		port map(ins => ins, output => output_structural);
		
	uut_behavioral: and5
		port map(ins => ins, output => output_behavioral);
	
	clock_process: process
	begin
		wait for (delay / 2);
		clock <= not clock;
	end process clock_process;
	
	test: process
	begin
		ins <= (others => '0');
		wait until falling_edge(clock);
		
		for i in 0 to 2 ** ins'length - 1 loop
			wait until falling_edge(clock);
			
			assert output_behavioral = output_structural report "Test fail: Check and5" severity FAILURE;
			ins <= std_logic_vector(unsigned(ins) + 1);
		end loop;

		assert false
			report "End simulation"
			severity FAILURE;
	end process test;
end architecture Testbench;
