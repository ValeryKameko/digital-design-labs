library ieee;
use ieee.std_logic_1164.all;

entity mux4x2_behavioral is
	port(
		a, b: in std_logic_vector(1 downto 0);
		s: in std_logic;
		z: out std_logic_vector(1 downto 0)
	);
end entity mux4x2_behavioral;

architecture behavioral of mux4x2_behavioral is
begin
	z <= a when s = '0' else b;
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity mux4x2_structural is
	port(
		a, b: in std_logic_vector(1 downto 0);
		s: in std_logic;
		z: out std_logic_vector(1 downto 0)
	);
end entity mux4x2_structural;

architecture structural of mux4x2_structural is
	component mux2x1 is
		port(
			a, b, s: in std_logic;
			z: out std_logic
		);
	end component mux2x1;
	
	for all: mux2x1 use entity work.mux2x1_structural(structural);
begin
	mux2x1_0: mux2x1
		port map (a => a(0), b => b(0), s => s, z => z(0));
	
	mux2x1_1: mux2x1
		port map (a => a(1), b => b(1), s => s, z => z(1));
end architecture structural;

