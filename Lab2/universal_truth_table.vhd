library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity universal_truth_table is
	generic(
		N: positive := 3;
		utt_table: std_logic_vector := x"80"
	);
	port(
		ins: in std_logic_vector(N - 1 downto 0);
		
		output: out std_logic
	);
end entity universal_truth_table;

architecture behavioral of universal_truth_table is
	constant utt_table_values: std_logic_vector(2 ** N - 1 downto 0) := utt_table;
begin
	output <= utt_table_values(to_integer(unsigned(ins)));
end architecture Behavioral;