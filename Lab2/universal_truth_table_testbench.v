`timescale 1ns / 1ps
`include "assert.v"


module universal_truth_table__testbench;

localparam N = 3;
localparam [2 ** N - 1:0] UTT_TABLE = 8'b1001_1101;

localparam BITS_COUNT = N;
localparam time DELAY = 10;

reg clock = 0;
reg [BITS_COUNT - 1:0] inputs = 0;

wire out;

universal_truth_table_ #(.N(N), .UTT_TABLE(UTT_TABLE)) uut(
	.ins(inputs),
	.out(out)
);

always
	#(DELAY / 2) clock = ~clock;

integer i;
initial
begin
	inputs = 'b0;
	@(negedge clock);

	for (i = 0; i < 2 ** BITS_COUNT; i = i + 1)
	begin
		@(negedge clock);
		inputs = inputs + 1;
	end;
	$stop;
end;

endmodule
