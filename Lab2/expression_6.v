`timescale 1ns / 1ps


module expression_6__behavioral(
		input w,
		input x,
		input y,
		input z,
		
		output f
    );

wire [2:0] partials;

assign partials[0] = ((~w) | x) & y;
assign partials[1] = (~w) | x | (~y);
assign partials[2] = w | z;

assign f = & partials;

endmodule


module expression_6__structural(
		input w,
		input x,
		input y,
		input z,
		
		output f
    );

wire not_x, not_y, not_w;
wire intermediate;
wire [3:0] partials;

not invert_x(not_x, x);
not invert_w(not_w, w);
not invert_y(not_y, y);

nand calc_intermediate(intermediate, w, not_x);

nand calc_partials_0(partials[0], intermediate, y);
nor calc_partials_1(partials[1], not_w, x, not_y);
nor calc_partials_2(partials[2], w, z);

nor calc_f(f, partials[0], partials[1], partials[2]);

endmodule
