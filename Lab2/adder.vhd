library ieee;
use ieee.std_logic_1164.all;

entity half_adder is
	port(
		ins: in std_logic_vector(1 downto 0);
		sum: out std_logic;
		carry: out std_logic
	);
end entity half_adder;

architecture structural of half_adder is
	component xor2 is
		port(
			in1, in2: in std_logic;
			output: out std_logic
		);
	end component xor2;
	
	component and2 is
		port(
			in1, in2: in std_logic;
			output: out std_logic
		);
	end component and2;
begin
	calc_sum: xor2
		port map (in1 => ins(0), in2 => ins(1), output => sum);

	calc_carry: and2
		port map (in1 => ins(0), in2 => ins(1), output => carry);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity adder is
	port(
		ins: in std_logic_vector(1 downto 0);
		in_carry: in std_logic;
		sum: out std_logic;
		carry: out std_logic
	);
end entity adder;

architecture structural of adder is
	component half_adder is
		port(
			ins: in std_logic_vector(1 downto 0);
			sum: out std_logic;
			carry: out std_logic
		);
	end component half_adder;
	
	component or2 is 
		port(
			in1, in2: in std_logic;
			output: out std_logic
		);
	end component or2;
	
	signal temp_carries: std_logic_vector(1 downto 0);
	signal temp_sum: std_logic;
begin
	sum_ins: half_adder
		port map(ins => ins, sum => temp_sum, carry => temp_carries(0));
		
	sum_temp_sum_in_carry: half_adder
		port map(ins => temp_sum & in_carry, sum => sum, carry => temp_carries(1));
	
	calc_carry: or2
		port map(in1 => temp_carries(0), in2 => temp_carries(1), output => carry);
end architecture Structural;


library ieee;
use ieee.std_logic_1164.all;

entity adderN_behavioral is
	generic(
		N: positive := 2
	);
	port(
		a, b: in std_logic_vector(N - 1 downto 0);
		in_carry: in std_logic;
		
		sum: out std_logic_vector(N downto 0)
	);
end entity adderN_behavioral;

architecture behavioral of adderN_behavioral is
	signal carries: std_logic_vector(N downto 0);
begin
	calc_sums: process(a, b, in_carry, carries)
	begin
		carries(0) <= in_carry;
		for i in 0 to N - 1 loop
			sum(i) <= a(i) xor b(i) xor carries(i);
			carries(i + 1) <= (a(i) and b(i)) or ((a(i) xor b(i)) and carries(i));
		end loop;
		sum(N) <= carries(N);
	end process calc_sums;
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity adderN_structural is
	generic(
		N: positive := 2
	);
	port(
		a, b: in std_logic_vector(N - 1 downto 0);
		in_carry: in std_logic;
		sum: out std_logic_vector(N downto 0)
	);
end entity adderN_structural;

architecture structural of adderN_structural is
	component adder is
		port(
			ins: in std_logic_vector(1 downto 0);
			in_carry: in std_logic;
			
			sum: out std_logic;
			carry: out std_logic
		);
	end component adder;
	
	signal carries: std_logic_vector(N downto 0);
begin
	carries(0) <= in_carry;
	
	perform_sums: for i in 0 to N - 1 generate
	begin
		perform_sum: adder
			port map(
				ins => a(i) & b(i), 
				in_carry => carries(i), 
				sum => sum(i),
				carry => carries(i + 1)
			);
	end generate perform_sums;
	
	sum(N) <= carries(N);
end architecture Structural;

