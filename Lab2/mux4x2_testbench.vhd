library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.math_real.all;
 
 
entity mux4x2_testbench is
end entity mux4x2_testbench;
 
architecture testbench of mux4x2_testbench is
	component mux4x2 is
		port(
			a, b: in std_logic_vector(1 downto 0);
			s: in std_logic;
			
			z: out std_logic_vector(1 downto 0)
		);
	end component mux4x2;
	
	for uut_structural: mux4x2 use entity work.mux4x2_structural(structural);
	for uut_behavioral: mux4x2 use entity work.mux4x2_behavioral(behavioral);

   constant base_period: time := 10 ns;
	
	signal clock: std_logic := '0';
	signal inputs: std_logic_vector(4 downto 0) := (others => '0');
	
   signal a: std_logic_vector(1 downto 0);
   signal b: std_logic_vector(1 downto 0);
   signal s: std_logic := '0';

   signal z_structural: std_logic_vector(1 downto 0);
	signal z_behavioral: std_logic_vector(1 downto 0);
begin
	a <= inputs(1 downto 0);
	b <= inputs(3 downto 2);
	s <= inputs(4);

	uut_structural: mux4x2
		port map (a => a, b => b, s => s, z => z_structural); 
	
	uut_behavioral: mux4x2
		port map (a => a, b => b, s => s, z => z_behavioral); 
	
	clock_process: process
	begin
		wait for base_period / 2;
		clock <= not clock;
	end process clock_process;
	
   test: process
	begin
		inputs <= (others => '0');
		wait until falling_edge(clock);
		
		for i in 0 to 2 ** inputs'length - 1 loop
			wait until falling_edge(clock);
			
			assert z_behavioral = z_structural report "Test fail: Check mux2x1" severity FAILURE;
			inputs <= std_logic_vector(unsigned(inputs) + 1);
		end loop;

		assert false
			report "End simulation"
			severity FAILURE;
	end process test;
end architecture testbench;