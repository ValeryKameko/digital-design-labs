library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity nandN is
	generic(
		N: positive := 2
	);
	port(
		ins: in std_logic_vector(N - 1 downto 0);
		output: out std_logic
	);
end entity nandN;

architecture Behavioral of nandN is
	constant ins_ones: std_logic_vector(ins'range) := (others => '1');
begin
	output <= '0' when ins = ins_ones else '1';
end architecture Behavioral;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity norN is
	generic(
		N: positive := 2
	);
	port(
		ins: in std_logic_vector(N - 1 downto 0);
		output: out std_logic
	);
end entity norN;

architecture Behavioral of norN is
	constant ins_zeros: std_logic_vector(ins'range) := (others => '0');
begin
	output <= '1' when ins = ins_zeros else '0';
end architecture Behavioral;