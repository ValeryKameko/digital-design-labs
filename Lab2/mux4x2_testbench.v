`timescale 1ns / 1ps
`include "assert.v"


module mux4x2__testbench;

localparam unsigned BITS_COUNT = 5;
localparam time DELAY = 10;

reg clock = 0;
reg [BITS_COUNT - 1:0] inputs = 0;

wire [1:0] a = inputs[1:0];
wire [1:0] b = inputs[3:2];
wire s = inputs[4];

wire [1:0] z_behavioral;
wire [1:0] z_structural;

mux4x2__behavioral uut_behavioral (
	.a(a),
	.b(b),
	.s(s),
	.z(z_behavioral)
);

mux4x2__structural uut_structural (
	.a(a),
	.b(b),
	.s(s),
	.z(z_structural)
);

always
	#(DELAY / 2) clock <= ~clock;

integer i;
initial
begin
	inputs = 'b0;
	@(negedge clock);

	for (i = 0; i < 2 ** BITS_COUNT; i = i + 1)
	begin
		@(negedge clock);
		
		`assert_(z_structural == z_behavioral,
			"Test fail: Check mux4x2");
		inputs = inputs + 1;
	end;
	$stop;
end;

endmodule
