library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.math_real.all;


entity demux1x4_testbench is
end entity demux1x4_testbench;

architecture testbench of demux1x4_testbench is
	component demux1x4 is
		port(
			sel: in std_logic_vector(1 downto 0);
			data: in std_logic;
			outs: out std_logic_vector(3 downto 0)
		);
	end component demux1x4;
	
	for uut_structural: demux1x4 use entity work.demux1x4_structural(structural);
	for uut_behavioral: demux1x4 use entity work.demux1x4_behavioral(behavioral);
	
	constant base_period: time := 10 ns;
	
	signal clock: std_logic := '0';
	signal inputs: std_logic_vector(2 downto 0) := (others => '0');
	
	signal sel: std_logic_vector(1 downto 0);
	signal data: std_logic;
	
	signal outs_structural: std_logic_vector(3 downto 0);
	signal outs_behavioral: std_logic_vector(3 downto 0);
begin
	sel <= inputs(1 downto 0);
	data <= inputs(2);
	
	uut_structural: demux1x4
		port map(sel => sel, data => data, outs => outs_structural);
	
	uut_behavioral: demux1x4
		port map(sel => sel, data => data, outs => outs_behavioral);
	
	clock_process: process
	begin
		wait for (base_period / 2);
		clock <= not clock;
	end process clock_process;
	
	test: process
	begin
		inputs <= (others => '0');
		wait until falling_edge(clock);
		
		for i in 0 to 2 ** inputs'length - 1 loop
			wait until falling_edge(clock);
			
			assert outs_behavioral = outs_structural report "Test fail: Check demux1x4" severity FAILURE;
			inputs <= std_logic_vector(unsigned(inputs) + 1);
		end loop;

		assert false
			report "End simulation"
			severity FAILURE;
	end process test;
end architecture Testbench;