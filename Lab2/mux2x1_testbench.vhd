library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.math_real.all;
use std.env.stop;
 
 
entity mux2x1_testbench is
end entity mux2x1_testbench;
 
architecture Testbench of mux2x1_testbench is
	component mux2x1 is
		port(
			a, b, s: in std_logic;
			z: out std_logic
		);
	end component mux2x1;
	
	for uut_structural: mux2x1 use entity work.mux2x1_structural(structural);
	for uut_behavioral: mux2x1 use entity work.mux2x1_behavioral(behavioral);

   constant base_period: time := 10 ns;
	
	signal clock: std_logic := '0';
	
	signal inputs: std_logic_vector(2 downto 0) := (others => '0');
	
   signal a: std_logic := '0';
   signal b: std_logic := '0';
   signal s: std_logic := '0';

   signal z_structural: std_logic;
	signal z_behavioral: std_logic;
begin
	a <= inputs(0);
	b <= inputs(1);
	s <= inputs(2);

	uut_structural: mux2x1
		port map (a => a, b => b, s => s, z => z_structural); 
	
	uut_behavioral: mux2x1
		port map (a => a, b => b, s => s, z => z_behavioral); 
	
	clock_process: process
	begin
		wait for base_period / 2;
		clock <= not clock;
	end process clock_process;

	test: process
	begin
		inputs <= (others => '0');
		wait until falling_edge(clock);
		
		for i in 0 to 2 ** inputs'length - 1 loop
			wait until falling_edge(clock);
			
			assert z_behavioral = z_structural report "Test fail: Check mux2x1" severity FAILURE;
			inputs <= std_logic_vector(unsigned(inputs) + 1);
		end loop;

		assert false
			report "End simulation"
			severity FAILURE;
	end process test;
end architecture testbench;