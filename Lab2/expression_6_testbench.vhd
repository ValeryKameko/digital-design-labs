library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity expression_6_testbench is
end entity expression_6_testbench;

architecture testbench of expression_6_testbench is
	component expression_6 is
		port(
			w, x, y, z: in std_logic;
			f: out std_logic
		);
	end component expression_6;
	
	for uut_structural: expression_6 use entity work.expression_6_structural(structural);
	for uut_behavioral: expression_6 use entity work.expression_6_behavioral(behavioral);
	
	constant delay: time := 10 ns;
	
	signal clock: std_logic := '0';
	signal inputs: std_logic_vector(3 downto 0) := (others => '0');
	
	signal w, x, y, z: std_logic;
	signal f_structural, f_behavioral: std_logic;
begin
	w <= inputs(0);
	x <= inputs(1);
	y <= inputs(2);
	z <= inputs(3);
	
	uut_structural: expression_6
		port map(w => w, x => x, y => y, z => z, f => f_structural);
	
	uut_behavioral: expression_6
		port map(w => w, x => x, y => y, z => z, f => f_behavioral);
	
	clock_process: process
	begin
		wait for (delay / 2);
		clock <= not clock;
	end process clock_process;
	
	test: process
	begin
		inputs <= (others => '0');
		wait until falling_edge(clock);
		
		for i in 0 to 2 ** inputs'length - 1 loop
			wait until falling_edge(clock);
			
			assert f_behavioral = f_structural report "Test fail: Check expression6" severity FAILURE;
			inputs <= std_logic_vector(unsigned(inputs) + 1);
		end loop;

		assert false
			report "End simulation"
			severity FAILURE;
	end process test;
end architecture testbench;