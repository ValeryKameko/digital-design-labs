`timescale 1ns / 1ps


module mux2x1__behavioral(
		input a,
		input b,
		input s,
		
		output z
    );

assign z = s ? b : a;

endmodule


module mux2x1__structural(
		input a, 
		input b, 
		input s,
		
		output z
    );

wire not_s;
wire use_a;
wire use_b;

not invert_s(not_s, s);

and choose_a(use_a, a, not_s);
and choose_b(use_b, b, s);

or result(z, use_a, use_b);

endmodule
