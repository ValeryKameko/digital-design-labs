`timescale 1ns / 1ps
`include "assert.v"


module expression_6__testbench;

localparam unsigned BITS_COUNT = 4;
localparam time DELAY = 10;

reg clock = 0;
reg [BITS_COUNT - 1:0] inputs = 0;

wire w = inputs[0];
wire x = inputs[1];
wire y = inputs[2];
wire z = inputs[3];

wire f_structural;
wire f_behavioral;

expression_6__structural uut_structural(	
	.w(w),
	.x(x),
	.y(y),
	.z(z),
	.f(f_structural)
);

expression_6__behavioral uut_behavioral(	
	.w(w),
	.x(x),
	.y(y),
	.z(z),
	.f(f_behavioral)
);

always
	#(DELAY / 2) clock = ~clock;

integer i;
initial
begin
	inputs = 'b0;
	@(negedge clock);

	for (i = 0; i < 2 ** BITS_COUNT; i = i + 1)
	begin
		@(negedge clock);
		
		`assert_(f_structural == f_behavioral,
			"Test fail: Check expression6");
		inputs = inputs + 1;
	end;
	$stop;
end;

endmodule
