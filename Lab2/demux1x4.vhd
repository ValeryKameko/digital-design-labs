library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity demux1x4_behavioral is
	port(
		sel: in std_logic_vector(1 downto 0);
		data: in std_logic;
		outs: out std_logic_vector(3 downto 0)
	);
end entity demux1x4_behavioral;

architecture behavioral of demux1x4_behavioral is
begin
	assign_outs: process (data, sel)
		variable index: natural;
	begin
		outs <= (others => '0');
		if data = '1' then
			index := to_integer(unsigned(sel));
			outs(index) <= data;
		end if;
	end process assign_outs;
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity demux1x4_structural is
	port(
		sel: in std_logic_vector(1 downto 0);
		data: in std_logic;
		outs: out std_logic_vector(3 downto 0)
	);
end entity demux1x4_structural;

architecture structural of demux1x4_structural is
	component and3 is
		port(
			in1, in2, in3: in std_logic;
			output: out std_logic
		);
	end component and3;
	
	component or2 is
		port(
			in1, in2: in std_logic;	
			output: out std_logic
		);
	end component or2;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	
	signal not_sel: std_logic_vector(1 downto 0);
begin
	invert_sel_0: not1
		port map (input => sel(0), output => not_sel(0));
	
	invert_sel_1: not1
		port map (input => sel(1), output => not_sel(1));
	
	select_0: and3
		port map (in1 => not_sel(0), in2 => not_sel(1), in3 => data, output => outs(0));
	
	select_1: and3
		port map (in1 =>     sel(0), in2 => not_sel(1), in3 => data, output => outs(1));
		
	select_2: and3
		port map (in1 => not_sel(0), in2 =>     sel(1), in3 => data, output => outs(2));
		
	select_3: and3
		port map (in1 =>     sel(0), in2 =>     sel(1), in3 => data, output => outs(3));
end architecture structural;

