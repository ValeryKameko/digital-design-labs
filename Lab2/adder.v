`timescale 1ns / 1ps


module half__adder(
		input [1:0] ins,
		
		output sum,
		output carry
	 );

xor calc_sum(sum, ins[0], ins[1]);
and calc_carry(carry, ins[0], ins[1]);

endmodule


module adder_(
		input [1:0] ins,
		input in_carry,
		
		output sum,
		output carry
    );

wire [1:0] temp_carries;
wire temp_sum;

half__adder sum_ins(
	.ins(ins),
	.sum(temp_sum),
	.carry(temp_carries[0])
);

half__adder sum_temp_sum_in_carry(
	.ins({temp_sum, in_carry}),
	.sum(sum),
	.carry(temp_carries[1])
);

or calc_carry(carry, temp_carries[0], temp_carries[1]);

endmodule


module adderN__behavioral(
			input [N - 1:0] a,
			input [N - 1:0] b,
			input in_carry,
			
			output reg [N:0] sum
		);
parameter N = 2;

reg carries[N:0];

integer i;

always @(a or b or in_carry)
begin
	carries[0] = in_carry;
	
	for (i = 0; i < N; i = i + 1)
	begin
		sum[i] = a[i] ^ b[i] ^ carries[i];
		carries[i + 1] = (a[i] & b[i]) | ((a[i] ^ b[i]) & carries[i]);
	end;
	
	sum[N] = carries[N];
end
		
endmodule


module adderN__structural(
			input [N - 1:0] a,
			input [N - 1:0] b,
			input in_carry,
			
			output [N:0] sum
		);
parameter N = 2;

wire carries[N:0];

assign carries[0] = in_carry;

genvar i;

generate
	for (i = 0; i < N; i = i + 1)
	begin
		adder_ perform_sum_i(
			.ins({a[i], b[i]}),
			.in_carry(carries[i]),
			.sum(sum[i]),
			.carry(carries[i + 1])
		);
	end
endgenerate

assign sum[N] = carries[N];
		
endmodule

