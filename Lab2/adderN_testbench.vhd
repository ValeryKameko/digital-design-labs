library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity adderN_testbench is
end entity adderN_testbench;

architecture testbench of adderN_testbench is
	component adderN is
		generic(
			N: positive
		);
		port(
			a, b: in std_logic_vector(N - 1 downto 0);
			in_carry: in std_logic;
			sum: out std_logic_vector(N downto 0)
		);
	end component adderN;
	
	for uut_behavioral: adderN use entity work.adderN_behavioral(behavioral);
	for uut_structural: adderN use entity work.adderN_structural(structural);
	
	constant N: positive := 2;
	constant delay: time := 10 ns;
	
	signal clock: std_logic := '0';
	signal inputs: std_logic_vector(N + N downto 0) := (others => '0');
	
	signal a, b: std_logic_vector(N - 1 downto 0);
	signal in_carry: std_logic;
	signal sum_structural, sum_behavioral: std_logic_vector(N downto 0);
begin
	a <= inputs(N - 1 downto 0);
	b <= inputs(N + N - 1 downto N);
	in_carry <= inputs(2 * N);

	uut_behavioral: adderN
		generic map (N => N)
		port map (a => a, b => b, in_carry => in_carry, sum => sum_behavioral);
	
	uut_structural: adderN
		generic map (N => N)
		port map (a => a, b => b, in_carry => in_carry, sum => sum_structural);

	clock_process: process
	begin
		wait for (delay / 2);
		clock <= not clock;
	end process clock_process;
	
	test: process
	begin
		inputs <= (others => '0');
		wait until falling_edge(clock);
		
		for i in 0 to 2 ** inputs'length - 1 loop
			wait until falling_edge(clock);
			
			assert sum_behavioral = sum_structural report "Test fail: Check add" severity FAILURE;
			inputs <= std_logic_vector(unsigned(inputs) + 1);
		end loop;

		assert false
			report "End simulation"
			severity FAILURE;
	end process;
end architecture testbench;