`timescale 1ns / 1ps


module demux1x4__behavioral(
		input [1:0] sel,
		input data,
		
		output [3:0] outs
    );

assign outs = data << sel;

endmodule


module demux1x4__structural(
		input [1:0] sel,
		input data,
		
		output [3:0] outs
    );

wire [1:0] not_sel;

not invert_ins_0(not_sel[0], sel[0]);
not invert_ins_1(not_sel[1], sel[1]);

and select_0(outs[0], not_sel[0], not_sel[1], data);
and select_1(outs[1],     sel[0], not_sel[1], data);
and select_2(outs[2], not_sel[0],     sel[1], data);
and select_3(outs[3],     sel[0],     sel[1], data);

endmodule
