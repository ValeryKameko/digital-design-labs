`timescale 1ns / 1ps
`include "assert.v"


module demux1x4__testbench;

localparam unsigned BITS_COUNT = 3;
localparam time DELAY = 10;

reg clock = 0;
reg [BITS_COUNT - 1:0] inputs = 0;

wire [1:0] sel = inputs[1:0];
wire data = inputs[2];

wire [3:0] outs_behavioral;
wire [3:0] outs_structural;

demux1x4__behavioral uut_behavioral(
	.sel(sel),
	.data(data),
	.outs(outs_behavioral)
);

demux1x4__structural uut_structural(
	.sel(sel),
	.data(data),
	.outs(outs_structural)
);

always
	#(DELAY / 2) clock = ~clock;

integer i;
initial
begin
	inputs = 'b0;
	@(negedge clock);

	for (i = 0; i < 2 ** BITS_COUNT; i = i + 1)
	begin
		@(negedge clock);
		
		`assert_(outs_structural == outs_behavioral,
			"Test fail: Check demux1x4");
		inputs = inputs + 1;
	end;
	$stop;
end;

endmodule
