`timescale 1ns / 1ps
`include "assert.v"


module and5__testbench;
	
localparam unsigned BITS_COUNT = 5;
localparam time DELAY = 10;

reg clock = 0;
reg [BITS_COUNT - 1:0] inputs = 0;

wire out_structural;
wire out_behavioral;

and5__behavioral uut_behavioral(
	.ins(inputs),
	.out(out_behavioral)
);

and5__structural uut_structural(
	.ins(inputs),
	.out(out_structural)
);

always
	#(DELAY / 2) clock <= ~clock;

integer i;
initial
begin
	inputs = 'b0;
	@(negedge clock);

	for (i = 0; i < 2 ** BITS_COUNT; i = i + 1)
	begin
		@(negedge clock);
		
		`assert_(out_structural == out_behavioral,
			"Test fail: Check and5");
		inputs = inputs + 1;
	end;
	$stop;
end;

endmodule
