library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity universal_truth_table_testbench is
end entity universal_truth_table_testbench;

architecture testbench of universal_truth_table_testbench is
	component universal_truth_table is
		generic(
			N: positive;
			utt_table: std_logic_vector
		);
		port(
			ins: in std_logic_vector(N - 1 downto 0);
			output: out std_logic
		);
	end component universal_truth_table;
	
	constant N: positive := 5;
	constant utt_table: std_logic_vector(2 ** N - 1 downto 0) := (5 => '1', 10 => '1', others => '0');
	
	constant delay: time := 10 ns;
	constant bits_count: positive := N;
	constant duration: time := delay * (2 ** bits_count);
	
	signal clock: std_logic := '0';
	signal inputs: std_logic_vector(N - 1 downto 0) := (others => '0');

	signal output: std_logic;
begin
	uut: universal_truth_table
		generic map(N => N, utt_table => utt_table)
		port map(ins => inputs, output => output);
		
	clock_process: process
	begin
		wait for (delay / 2);
		clock <= not clock;
	end process clock_process;
	
	test: process
	begin
		inputs <= (others => '0');
		wait until falling_edge(clock);
		
		for i in 0 to 2 ** inputs'length - 1 loop
			wait until falling_edge(clock);
			inputs <= std_logic_vector(unsigned(inputs) + 1);
		end loop;

		assert false
			report "End simulation"
			severity FAILURE;
	end process test;
end architecture testbench;