`timescale 1ns / 1ps
`include "assert.v"


module mux2x1__testbench;

localparam unsigned BITS_COUNT = 3;
localparam time DELAY = 10;

reg clock = 0;
reg [BITS_COUNT - 1:0] inputs = 0;

wire a = inputs[0];
wire b = inputs[1];
wire s = inputs[2];

wire z_behavioral;
wire z_structural;

mux2x1__behavioral uut_behavioral (
	.a(a),
	.b(b),
	.s(s),
	.z(z_behavioral)
);

mux2x1__structural uut_structural (
	.a(a),
	.b(b),
	.s(s),
	.z(z_structural)
);

always 
	#(DELAY / 2) clock = ~clock;

integer i;
initial
begin
	inputs = 'b0;
	@(negedge clock);

	for (i = 0; i < 2 ** BITS_COUNT; i = i + 1)
	begin
		@(negedge clock);
		
		`assert_(z_structural == z_behavioral,
			"Test fail: Check mux2x1");
		inputs = inputs + 1;
	end;
	$stop;
end;

endmodule
