library ieee;
use ieee.std_logic_1164.all;

entity mux2x1_behavioral is
	port(
		a, b, s: in std_logic;
		z: out std_logic
	);
end entity mux2x1_behavioral;

architecture behavioral of mux2x1_behavioral is
begin 
	z <= a when s = '0' else b;
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity mux2x1_structural is
	port(
		a, b, s: in std_logic;
		z: out std_logic
	);
end entity mux2x1_structural;

architecture structural of mux2x1_structural is	
	component and2 is
		port(
			in1, in2: in std_logic;
			output: out std_logic
		);
	end component and2;
	
	component or2 is
		port(
			in1, in2: in std_logic;	
			output: out std_logic
		);
	end component or2;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;

	signal not_s: std_logic;
	signal use_a, use_b: std_logic;
begin	
	invert_b: not1
		port map (input => s, output => not_s);
	
	choose_a: and2
		port map (in1 => a, in2 => not_s, output => use_a);
	
	choose_b: and2
		port map (in1 => b, in2 => s, output => use_b);
	
	result: or2
		port map (in1 => use_a, in2 => use_b, output => z);
end architecture structural;

