library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux2x1_expression is
	port(
		in1, in2, in3: in std_logic;
		q, nq: out std_logic
	);
end mux2x1_expression;

architecture Behavioral of mux2x1_expression is
signal tmp: std_logic;
begin
	tmp <= (in1 and in2) or (in1 and not in2);
	q <= tmp;
	nq <= not tmp;
end Behavioral;

