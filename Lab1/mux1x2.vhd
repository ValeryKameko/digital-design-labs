library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux1x2 is
	port(
		a, b, s: in std_logic;
		z: out std_logic
	);
end mux1x2;

architecture Behavioral of mux1x2 is
begin
	z <= a when s = '0' else b;
end Behavioral;

