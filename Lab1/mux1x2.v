`timescale 1ns / 1ps

module mux1x2(
		input a, 
		input b,
		input s,

		output z
    );

assign z = s ? a : b;

endmodule
