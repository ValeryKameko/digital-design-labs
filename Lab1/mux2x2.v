`timescale 1ns / 1ps

module mux2x2(
		input [1:0] a,
		input [1:0] b,
		input c,
		
		output [1:0] z
    );

assign z = c ? a : b;

endmodule
