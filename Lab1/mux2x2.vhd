library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux2x2 is
	port(
		a, b: in std_logic_vector(0 to 1);
		c: in std_logic;
		z: out std_logic_vector(0 to 1)
	);
end mux2x2;

architecture Behavioral of mux2x2 is
begin
	z <= a when c = '0' else b;
end Behavioral;