library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity or4 is
	port(
		a, b: in std_logic_vector(0 to 3);
		c: out std_logic_vector(0 to 3)		
	);
end or4;

architecture Behavioral of or4 is
begin
	c <= a or b;
end Behavioral;

