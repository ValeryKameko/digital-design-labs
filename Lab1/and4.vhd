library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity and4 is
	port(
		a, b: in std_logic_vector(0 to 3);
		z: out std_logic_vector(0 to 3)
	);
end and4;

architecture Behavioral of and4 is
begin
	z <= a and b;
end Behavioral;