`timescale 1ns / 1ps

module mux2x1_expression(
		input in1,
		input in2,
		input in3,
		
		output q,
		output nq
    );

assign q = (in1 & in2) | (in3 & ~in2);
assign nq = ~q;

endmodule
