library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity xor4 is
	port(
		a, b: in std_logic_vector(0 to 3);
		c: out std_logic_vector(0 to 3)
	);
end xor4;

architecture Behavioral of xor4 is
begin
	c <= a xor b;
end Behavioral;
