--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: parity_encoder__synthesis.vhd
-- /___/   /\     Timestamp: Fri Oct 16 15:08:36 2020
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm parity_encoder_ -w -dir netgen/synthesis -ofmt vhdl -sim parity_encoder_.ngc parity_encoder__synthesis.vhd 
-- Device	: xc7a100t-1-csg324
-- Input file	: parity_encoder_.ngc
-- Output file	: D:\projects\hardware\digital-design-labs\Lab5\netgen\synthesis\parity_encoder__synthesis.vhd
-- # of Entities	: 1
-- Design Name	: parity_encoder_
-- Xilinx	: D:\programs\XilinxISE\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity \parity_encoder_\ is
  port (
    data : in STD_LOGIC_VECTOR ( 5 downto 0 ); 
    encoded_data : out STD_LOGIC_VECTOR ( 9 downto 0 ) 
  );
end \parity_encoder_\;

architecture Structure of \parity_encoder_\ is
  signal encoded_data_7_OBUF_0 : STD_LOGIC; 
  signal encoded_data_5_OBUF_1 : STD_LOGIC; 
  signal encoded_data_4_OBUF_2 : STD_LOGIC; 
  signal encoded_data_2_OBUF_3 : STD_LOGIC; 
  signal encoded_data_1_OBUF_4 : STD_LOGIC; 
  signal encoded_data_0_OBUF_5 : STD_LOGIC; 
  signal encoded_data_8_OBUF_6 : STD_LOGIC; 
  signal encoded_data_9_OBUF_7 : STD_LOGIC; 
  signal encoded_data_6_OBUF_8 : STD_LOGIC; 
  signal encoded_data_3_OBUF_9 : STD_LOGIC; 
begin
  encoded_data_6_1 : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => encoded_data_2_OBUF_3,
      I1 => encoded_data_4_OBUF_2,
      I2 => encoded_data_5_OBUF_1,
      O => encoded_data_6_OBUF_8
    );
  encoded_data_8_1 : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => encoded_data_1_OBUF_4,
      I1 => encoded_data_5_OBUF_1,
      I2 => encoded_data_7_OBUF_0,
      O => encoded_data_8_OBUF_6
    );
  encoded_data_9_1 : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => encoded_data_0_OBUF_5,
      I1 => encoded_data_4_OBUF_2,
      I2 => encoded_data_7_OBUF_0,
      O => encoded_data_9_OBUF_7
    );
  encoded_data_3_1 : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => encoded_data_0_OBUF_5,
      I1 => encoded_data_1_OBUF_4,
      I2 => encoded_data_2_OBUF_3,
      O => encoded_data_3_OBUF_9
    );
  data_5_IBUF : IBUF
    port map (
      I => data(5),
      O => encoded_data_7_OBUF_0
    );
  data_4_IBUF : IBUF
    port map (
      I => data(4),
      O => encoded_data_5_OBUF_1
    );
  data_3_IBUF : IBUF
    port map (
      I => data(3),
      O => encoded_data_4_OBUF_2
    );
  data_2_IBUF : IBUF
    port map (
      I => data(2),
      O => encoded_data_2_OBUF_3
    );
  data_1_IBUF : IBUF
    port map (
      I => data(1),
      O => encoded_data_1_OBUF_4
    );
  data_0_IBUF : IBUF
    port map (
      I => data(0),
      O => encoded_data_0_OBUF_5
    );
  encoded_data_9_OBUF : OBUF
    port map (
      I => encoded_data_9_OBUF_7,
      O => encoded_data(9)
    );
  encoded_data_8_OBUF : OBUF
    port map (
      I => encoded_data_8_OBUF_6,
      O => encoded_data(8)
    );
  encoded_data_7_OBUF : OBUF
    port map (
      I => encoded_data_7_OBUF_0,
      O => encoded_data(7)
    );
  encoded_data_6_OBUF : OBUF
    port map (
      I => encoded_data_6_OBUF_8,
      O => encoded_data(6)
    );
  encoded_data_5_OBUF : OBUF
    port map (
      I => encoded_data_5_OBUF_1,
      O => encoded_data(5)
    );
  encoded_data_4_OBUF : OBUF
    port map (
      I => encoded_data_4_OBUF_2,
      O => encoded_data(4)
    );
  encoded_data_3_OBUF : OBUF
    port map (
      I => encoded_data_3_OBUF_9,
      O => encoded_data(3)
    );
  encoded_data_2_OBUF : OBUF
    port map (
      I => encoded_data_2_OBUF_3,
      O => encoded_data(2)
    );
  encoded_data_1_OBUF : OBUF
    port map (
      I => encoded_data_1_OBUF_4,
      O => encoded_data(1)
    );
  encoded_data_0_OBUF : OBUF
    port map (
      I => encoded_data_0_OBUF_5,
      O => encoded_data(0)
    );

end Structure;

