`timescale 1ns / 1ps


module fifo_ram_(
		input reset,
		input clk,
		
		input write_enable,
		input [DATA_BITS - 1:0] din,
		
		input read_enable,
		output [DATA_BITS - 1:0] dout,
		
		output full,
		output empty
    );
`include "../libraries/math/math.v"

parameter DEPTH = 1024;
parameter DATA_BITS = 8;
parameter RAM_TYPE = "auto";
localparam ADDRESS_BITS = ceil_log2_(DEPTH);

reg is_empty;
reg is_full;
integer write_address, read_address;
reg [DATA_BITS - 1:0] temp_dout;
(* ram_style = RAM_TYPE *) reg [DATA_BITS - 1:0] data [2 ** ADDRESS_BITS - 1:0];

// Write values
integer next_write_address;
always @*
	next_write_address = (write_address + 1) % DEPTH;

always @(posedge clk or posedge reset)
begin
	if (reset == 1)
		write_address <= 0;
	else if (write_enable == 1 && is_full == 0)
	begin
		data[write_address] <= din;
		write_address <= next_write_address;
	end;
end;

// Read values
integer next_read_address;
always @*
	next_read_address = (read_address + 1) % DEPTH;

always @(posedge clk or posedge reset)
begin
	if (reset == 1)
		read_address <= 0;
	else if (read_enable == 1 && is_empty == 0)
	begin
		read_address <= next_read_address;
		temp_dout <= data[read_address];
	end;
end;

assign dout = (read_enable == 1) ? temp_dout : {DATA_BITS{1'bZ}};

// Update state
always @(posedge clk or posedge reset)
begin
	if (reset == 1)
		{is_full, is_empty} <= 'b01;
	else if (read_enable == 0 && write_enable == 1)
	begin
		is_empty <= 0;
		if (next_write_address == read_address)
			is_full <= 1;
	end
	else if (read_enable == 1 && write_enable == 0)
	begin
		is_full <= 0;
		if (write_address == next_read_address)
			is_empty <= 1;
	end;
end;

assign full = is_full;
assign empty = is_empty;

endmodule
