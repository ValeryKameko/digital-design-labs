`timescale 1ns / 1ps


module hamming_ecc_ram_(
		input clk,
		input write_enable,
		input [ADDRESS_BITS - 1:0] address,
		input [DATA_BITS - 1:0] din,
		output [DATA_BITS - 1:0] dout
    );
`include "../libraries/math/math.v"

parameter unsigned DATA_BITS = 8;
parameter unsigned ADDRESS_BITS = 10;
parameter RAM_TYPE = "auto";

localparam STORED_DATA_BITS = DATA_BITS + inverse_hamming_value_(DATA_BITS);
wire [STORED_DATA_BITS - 1:0] encoded_din, encoded_dout;

hamming_encoder_ #(
	.BITS(DATA_BITS)
) encode_din(
	.data(din),
	.encoded_data(encoded_din)
);

ram_ #(
	.DATA_BITS(STORED_DATA_BITS),
	.ADDRESS_BITS(ADDRESS_BITS),
	.RAM_TYPE(RAM_TYPE)
) store_data(
	.clk(clk),
	.write_enable(write_enable),
	.address(address),
	.din(encoded_din),
	.dout(encoded_dout)
);

hamming_decoder_ #(
	.BITS(DATA_BITS)
) decode_dout(
	.encoded_data(encoded_dout),
	.decoded_data(dout)
);

endmodule
