library ieee;
library math;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use math.math_pkg.all;


entity hamming_ecc_ram is
	generic(
		RAM_TYPE: string := "auto";
		ADDRESS_BITS: natural := 7;
		DATA_BITS: natural := 8
	);
	port(
		clk, write_enable: in std_logic;
		address: in std_logic_vector(ADDRESS_BITS - 1 downto 0);
		din: in std_logic_vector(DATA_BITS - 1 downto 0);
		dout: out std_logic_vector(DATA_BITS - 1 downto 0)
	);
end entity hamming_ecc_ram;

architecture behavioral of hamming_ecc_ram is
	component ram is
		generic(
			RAM_TYPE: string;
			ADDRESS_BITS: natural;
			DATA_BITS: natural
		);
		port(
			clk, write_enable: in std_logic;
			address: in std_logic_vector(ADDRESS_BITS - 1 downto 0);
			din: in std_logic_vector(DATA_BITS - 1 downto 0);
			dout: out std_logic_vector(DATA_BITS - 1 downto 0)
		);
	end component ram;
	
	component hamming_decoder is
		generic(
			BITS: natural
		);
		port(
			encoded_data: in std_logic_vector(inverse_hamming_value(BITS) + BITS - 1 downto 0);
			decoded_data: out std_logic_vector(BITS - 1 downto 0);
			has_errors: out std_logic_vector(BITS - 1 downto 0);
			has_error: out std_logic
		);
	end component hamming_decoder;
	
	component hamming_encoder is
		generic(
			BITS: natural
		);
		port(
			data: in std_logic_vector(BITS - 1 downto 0);
			encoded_data: out std_logic_vector(inverse_hamming_value(BITS) + BITS - 1 downto 0)
		);
	end component hamming_encoder;
	
	constant STORED_DATA_BITS: natural := inverse_hamming_value(DATA_BITS) + DATA_BITS;
	
	signal encoded_din: std_logic_vector(STORED_DATA_BITS - 1 downto 0);
	signal encoded_dout: std_logic_vector(STORED_DATA_BITS - 1 downto 0);
begin
	encode_din: hamming_encoder
		generic map (BITS => DATA_BITS)
		port map (data => din, encoded_data => encoded_din);
	
	store_data: ram
		generic map (RAM_TYPE => RAM_TYPE, ADDRESS_BITS => ADDRESS_BITS, DATA_BITS => STORED_DATA_BITS)
		port map (clk => clk, write_enable => write_enable, address => address, din => encoded_din, dout => encoded_dout);
	
	decode_din: hamming_decoder
		generic map (BITS => DATA_BITS)
		port map (encoded_data => encoded_dout, decoded_data => dout);
end architecture behavioral;
