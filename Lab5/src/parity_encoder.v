`timescale 1ns / 1ps


module parity_encoder_(
		input [BITS - 1:0] data,
		output reg [ENCODED_BITS - 1:0] encoded_data
    );
`include "libraries/math/math.v"

parameter BITS = 6;
localparam integer PARITY_BITS = (ceil_sqrt_(8 * BITS + 1) - 1) / 2 + 1;
localparam integer ENCODED_BITS = BITS + PARITY_BITS;

reg aligned_bits[PARITY_BITS - 1:0][PARITY_BITS - 1:0];

integer i, j, k, h;

always @(data)
begin : align_bits
	i = 0;
	j = 0;
	for (k = 0; k < BITS; k = k + 1)
	begin
		aligned_bits[i][j] = data[k];
		j = j + 1;
		if (j >= PARITY_BITS - i - 1)
		begin
			i = i + 1;
			j = 0;
		end;
	end;
end : align_bits;

reg xor_value;
always @*
begin : calc_parity_bits
	for (k = 0; k < PARITY_BITS; k = k + 1)
	begin
		i = k;
		j = PARITY_BITS - k - 1;
		
		xor_value = 0;
		for (h = 0; h < i; h = h + 1)
			xor_value = xor_value ^ aligned_bits[h][j];
		
		for (h = 0; h < j; h = h + 1)
			xor_value = xor_value ^ aligned_bits[i][h];
		
		aligned_bits[i][j] = xor_value;
	end;
end : calc_parity_bits;

always @(aligned_bits)
begin : collect_encoded_bits
	i = 0;
	j = 0;
	for (k = 0; k < ENCODED_BITS; k = k + 1)
	begin
		encoded_data[k] = aligned_bits[i][j];
		j = j + 1;
		if (j >= PARITY_BITS - i)
		begin
			i = i + 1;
			j = 0;
		end;
	end;
end : collect_encoded_bits;

endmodule
