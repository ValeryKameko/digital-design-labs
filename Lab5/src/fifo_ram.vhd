library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;


entity fifo_ram is
	generic(
		RAM_TYPE: string := "block";
		DEPTH: natural := 16;
		DATA_BITS: natural := 8
	);
	port(
		reset, clk: in std_logic;
		write_enable: in std_logic;
		din: in std_logic_vector(DATA_BITS - 1 downto 0);
		read_enable: in std_logic;
		dout: out std_logic_vector(DATA_BITS - 1 downto 0);
		full, empty: out std_logic
	);
end entity fifo_ram;

architecture behavioral of fifo_ram is
	constant ADDRESS_BITS: integer := integer(ceil(log2(real(DEPTH)))); 
	subtype word_t is std_logic_vector(DATA_BITS - 1 downto 0);
	type ram_t is array(0 to 2 ** ADDRESS_BITS - 1) of word_t;
	
	signal data: ram_t;
	signal is_empty, is_full: std_logic;
	signal read_address, write_address: integer range 0 to DEPTH - 1;
	signal next_read_address, next_write_address: integer range 0 to DEPTH - 1;
	signal temp_dout: word_t;
	
	attribute ram_style: string;
	attribute ram_style of data: signal is RAM_TYPE;
begin
	-- Write value
	next_write_address <= (write_address + 1) mod DEPTH;

	write_value: process (clk, reset, write_enable, is_full)
	begin
		if reset = '1' then
			write_address <= 0;
		elsif rising_edge(clk) and write_enable = '1' and is_full = '0' then
			write_address <= next_write_address;
			data(write_address) <= din;
		end if;
	end process write_value;
	
	-- Read value
	next_read_address <= (read_address + 1) mod DEPTH;

	read_value: process (clk, read_enable, is_empty)
	begin
		if rising_edge(clk) and read_enable = '1' and is_empty = '0' then
			read_address <= next_read_address;
			temp_dout <= data(read_address);
		end if;
	end process read_value;
	
	dout <= temp_dout when read_enable = '1' else (dout'range => 'Z');
		
	-- Update state
	update_state: process (clk, reset)
	begin
		if reset = '1' then
			is_full <= '0';
			is_empty <= '1';
		elsif rising_edge(clk) then
			if write_enable = '1' and read_enable = '0' then
				is_empty <= '0';
				if next_write_address = read_address then
					is_full <= '1';
				end if;
			elsif write_enable = '0' and read_enable = '1' then
				is_full <= '0';
				if write_address = next_read_address then
					is_empty <= '1';
				end if;
			end if;
		end if;
	end process update_state;
	
	full <= is_full;
	empty <= is_empty;
end architecture behavioral;
