library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity ram is
	generic(
		RAM_TYPE: string := "auto";
		ADDRESS_BITS: natural := 10;
		DATA_BITS: natural := 8
	);
	port(
		clk, write_enable: in std_logic;
		address: in std_logic_vector(ADDRESS_BITS - 1 downto 0);
		din: in std_logic_vector(DATA_BITS - 1 downto 0);
		dout: out std_logic_vector(DATA_BITS - 1 downto 0)
	);
end entity ram;

architecture behavioral of ram is
	type ram_t is array(2 ** ADDRESS_BITS - 1 downto 0) of std_logic_vector(DATA_BITS - 1 downto 0);
   signal ram_data: ram_t;
	
	attribute ram_style : string;
	attribute ram_style of ram_data : signal is RAM_TYPE;
begin
	store_data: process (clk, write_enable)
		variable index: integer range 2 ** ADDRESS_BITS - 1 downto 0;
	begin
		if rising_edge(clk) then
			index := conv_integer(unsigned(address));
			if write_enable = '1' then
				ram_data(index) <= din;
			end if;
			dout <= ram_data(index);
		end if;
	end process store_data;
end architecture behavioral;
