`timescale 1ns / 1ps


module parity_decoder_(
		input [ENCODED_BITS - 1:0] encoded_data,
		output reg [BITS - 1:0] decoded_data,
		output reg [BITS - 1:0] has_errors,
		output has_error
    );
`include "libraries/math/math.v"

parameter BITS = 6;
localparam integer PARITY_BITS = (ceil_sqrt_(8 * BITS + 1) - 1) / 2 + 1;
localparam integer ENCODED_BITS = BITS + PARITY_BITS;

reg aligned_bits[PARITY_BITS - 1:0][PARITY_BITS - 1:0];
reg [PARITY_BITS - 1:0] error_bits;

integer i, j, k, h;

always @(encoded_data)
begin : align_bits
	i = 0;
	j = 0;
	for (k = 0; k < ENCODED_BITS; k = k + 1)
	begin
		aligned_bits[i][j] = encoded_data[k];
		j = j + 1;
		if (j >= PARITY_BITS - i)
		begin
			i = i + 1;
			j = 0;
		end;
	end;
end : align_bits;

reg xor_value;
always @(aligned_bits)
begin : calc_errors_bits
	for (k = 0; k < PARITY_BITS; k = k + 1)
	begin
		i = k;
		j = PARITY_BITS - k - 1;
		
		xor_value = 0;
		for (h = 0; h < i; h = h + 1)
			xor_value = xor_value ^ aligned_bits[h][j];
		
		for (h = 0; h < j; h = h + 1)
			xor_value = xor_value ^ aligned_bits[i][h];
		
		error_bits[k] = aligned_bits[i][j] ^ xor_value;
	end;
end : calc_errors_bits;

reg temp_error;
always @(aligned_bits or error_bits)
begin : decode_data
	i = 0;
	j = 0;
	for (k = 0; k < BITS; k = k + 1)
	begin
		temp_error = error_bits[i] & error_bits[PARITY_BITS - j - 1];
		has_errors[k] = temp_error;
		decoded_data[k] = aligned_bits[i][j] ^ temp_error;
		
		j = j + 1;
		if (j >= PARITY_BITS - i - 1)
		begin
			i = i + 1;
			j = 0;
		end;
	end;
end : decode_data;

assign has_error = |has_errors;

endmodule
