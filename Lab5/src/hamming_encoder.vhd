library ieee;
library math;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use math.math_pkg.all;


entity hamming_encoder is
	generic(
		BITS: natural := 6
	);
	port(
		data: in std_logic_vector(BITS - 1 downto 0);
		encoded_data: out std_logic_vector(inverse_hamming_value(BITS) + BITS - 1 downto 0)
	);
end entity hamming_encoder;
	
architecture behavioral of hamming_encoder is
	constant UNSIGNED_BITS: natural := 64;
	constant PARITY_BITS: natural := inverse_hamming_value(BITS);
	constant ENCODED_BITS: natural := BITS + PARITY_BITS;
begin
	encode_bits: process (data)
		variable j: integer;
		variable encoded_bit_values: std_logic_vector(ENCODED_BITS downto 1);
	begin
		j := 0;
		encoded_bit_values := (others => '0');
		for i in 1 to ENCODED_BITS loop
			if (to_unsigned(i, UNSIGNED_BITS) and to_unsigned(i - 1, UNSIGNED_BITS)) /= 0 then
				encoded_bit_values(i) := data(j);
				j := j + 1;
				
				for k in 0 to PARITY_BITS - 1 loop
					if (to_unsigned(2 ** k, UNSIGNED_BITS) and to_unsigned(i, UNSIGNED_BITS)) /= 0 then
						encoded_bit_values(2 ** k):= encoded_bit_values(2 ** k) xor encoded_bit_values(i);
					end if;
				end loop;
			end if;
		end loop;
		
		encoded_data <= encoded_bit_values;	
	end process encode_bits;
end architecture behavioral;

