`timescale 1ns / 1ps


module register_(
		input set,
		input reset,
		input clk,
		
		input write_enable,
		input [BITS - 1:0] din,
		
		input read_enable,
		output [BITS - 1:0] dout
    );

parameter BITS = 8;

reg [BITS - 1:0] storing_value;

always @(posedge clk or posedge reset or posedge set)
begin : store_value
	if (reset == 1)
		storing_value <= {BITS{1'b0}};
	else if (set == 1)
		storing_value <= {BITS{1'b1}};
	else if (write_enable == 1)
		storing_value <= din;
end : store_value;

assign dout = (read_enable == 1) ? storing_value : {BITS{1'bz}};

endmodule
