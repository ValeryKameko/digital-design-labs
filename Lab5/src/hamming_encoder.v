`timescale 1ns / 1ps


module hamming_encoder_(
		input [BITS - 1:0] data,
		output [ENCODED_BITS - 1:0] encoded_data
    );
`include "libraries/math/math.v"

parameter BITS = 6;
localparam integer PARITY_BITS = inverse_hamming_value_(BITS);
localparam integer ENCODED_BITS = BITS + PARITY_BITS;

reg [ENCODED_BITS:1] encoded_bit_values;

integer i, j, k;

always @(data)
begin : encode_bits
	j = 0;
	for (i = 1; i <= ENCODED_BITS; i = i + 1)
	begin
		if ((i & (i - 1)) != 0)
		begin
			encoded_bit_values[i] = data[j];
			j = j + 1;
			
			for (k = 0; k < PARITY_BITS; k = k + 1)
				if ((1 << k) & i)
					encoded_bit_values[1 << k] = encoded_bit_values[1 << k] ^ encoded_bit_values[i];
		end
		else
			encoded_bit_values[i] = 0;
	end;
end : encode_bits;

assign encoded_data = encoded_bit_values;

endmodule
