`timescale 1ns / 1ps


module rom_(
		input enable,
		input [ADDRESS_BITS - 1:0] address,
		output [DATA_BITS - 1:0] dout
    );

parameter [DATA_BITS * 2 ** ADDRESS_BITS - 1:0] ROM_DATA_VALUES = {
	16'hE002,
	16'h10A0,
	16'h0506,
	16'h0010
};
parameter DATA_BITS = 16;
parameter ADDRESS_BITS = 2;

integer index;
reg [DATA_BITS - 1:0] data;

always @(address)
begin
	index = address;
	data <= ROM_DATA_VALUES[index * DATA_BITS +: DATA_BITS];
end;

assign dout = (enable == 1) ? data : {DATA_BITS{1'bZ}};

endmodule
