`timescale 1ns / 1ps


module ram_(
		input clk,
		input write_enable,
		input [ADDRESS_BITS - 1:0] address,
		input [DATA_BITS - 1:0] din,
		output reg [DATA_BITS - 1:0] dout
    );

parameter unsigned DATA_BITS = 8;
parameter unsigned ADDRESS_BITS = 10;
parameter RAM_TYPE = "auto";

(* ram_style = RAM_TYPE *) reg [DATA_BITS - 1:0] ram_data [(1 << ADDRESS_BITS) - 1:0];

always @(posedge clk)
begin
	if (write_enable == 1)
		ram_data[address] = din;
	dout <= ram_data[address];
end;

endmodule
