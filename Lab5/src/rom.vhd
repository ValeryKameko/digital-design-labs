library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity rom is
	generic(
		ADDRESS_BITS: natural := 2;
		DATA_BITS: natural := 16;
		ROM_DATA_VALUES: std_logic_vector := (
			X"E002" &
			X"10A0" &
			X"0506" &
			X"0010"
		)
	);
	port(
		enable: in std_logic;
		address: in std_logic_vector(ADDRESS_BITS - 1 downto 0);
		dout: out std_logic_vector(DATA_BITS - 1 downto 0)
	);
end entity rom;

architecture behavioral of rom is
	signal data: std_logic_vector(DATA_BITS - 1 downto 0);
	signal index: integer range 2 ** ADDRESS_BITS - 1 downto 0;
begin
	index <= (2 ** ADDRESS_BITS - conv_integer(unsigned(address)) - 1);
	data <= ROM_DATA_VALUES(index * DATA_BITS to (index + 1) * DATA_BITS - 1);
	dout <= data when enable = '1' else (dout'range => 'Z');
end architecture behavioral;
