library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity replication_decoder is
	generic(
		N: natural := 4;
		BITS: natural := 3
	);
	port(
		encoded_data: in std_logic_vector(N * BITS - 1 downto 0);
		decoded_data: out std_logic_vector(BITS - 1 downto 0);
		has_errors: out std_logic_vector(BITS - 1 downto 0);
		has_error: out std_logic
	);
end entity replication_decoder;

architecture behavioral of replication_decoder is
	type int_array is array (natural range <>) of integer;
	
	function majority_bit(bits: std_logic_vector) return std_logic is
		variable counts: int_array(0 to 1);
		variable value: integer;
	begin
		for i in bits'range loop
			if bits(i) = '1' then
				value := 1;
			else
				value := 0;
			end if;
			counts(value) := counts(value) + 1;
		end loop;

		if counts(1) > counts(0) then
			return '1';
		else
			return '0';
		end if;
	end function majority_bit;

	signal has_error_bits: std_logic_vector(BITS - 1 downto 0);
	signal zeros_bits: std_logic_vector(BITS - 1 downto 0) := (others => '0');
begin
	decode_bits: for i in 0 to BITS - 1 generate
		signal bits_group: std_logic_vector(N - 1 downto 0);
		constant zeros_group: std_logic_vector(N - 1 downto 0) := (others => '0');
		constant ones_group: std_logic_vector(N - 1 downto 0) := (others => '1');
	begin
		bits_group <= encoded_data((i + 1) * N - 1 downto i * N);
		decoded_data(i) <= majority_bit(bits_group);
		has_error_bits(i) <= '1' when bits_group /= (bits_group'range => '0') and 
												bits_group /= (bits_group'range => '1') else '0';
	end generate decode_bits;

	has_errors <= has_error_bits;
	has_error <= '0' when has_error_bits = zeros_bits else '1';
end architecture behavioral;