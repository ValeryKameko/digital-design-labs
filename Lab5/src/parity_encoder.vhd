library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


entity parity_encoder is
	generic(
		BITS: natural := 6
	);
	port(
		data: in std_logic_vector(BITS - 1 downto 0);
		encoded_data: out std_logic_vector(integer(ceil((sqrt(real(8 * BITS + 1)) - 1.0) / 2.0)) + BITS downto 0)		
	);
end entity parity_encoder;
	
architecture behavioral of parity_encoder is
	constant PARITY_BITS: natural := integer(ceil((sqrt(real(8 * BITS + 1)) - 1.0) / 2.0)) + 1;
	constant ENCODED_BITS: natural := BITS + PARITY_BITS;
	
	type t_std_logic_2d is array (0 to PARITY_BITS - 1) of std_logic_vector(PARITY_BITS - 1 downto 0);
	signal aligned_bits: t_std_logic_2d;
	signal parity_bit_values: std_logic_vector(PARITY_BITS - 1 downto 0);
begin
	align_bits: process (data)
		variable i, j: integer;
		variable xor_value: std_logic;
	begin
		i := 0;
		j := 0;
		
		for k in 0 to BITS - 1 loop
			aligned_bits(i)(j) <= data(k);
			j := j + 1;
			if j >= PARITY_BITS - i - 1 then
				i := i + 1;
				j := 0;				
			end if;
		end loop;
	end process align_bits;
	
	calc_parity_bits: process (data, aligned_bits)
		variable i, j: integer;
		variable xor_value: std_logic;
	begin
		for k in 0 to PARITY_BITS - 1 loop
			i := k;
			j := PARITY_BITS - k - 1;
			
			xor_value := '0';			
			for h in 0 to i - 1 loop
				xor_value := xor_value xor aligned_bits(h)(j);
			end loop;
			
			for h in 0 to j - 1 loop
				xor_value := xor_value xor aligned_bits(i)(h);
			end loop;
			parity_bit_values(k) <= xor_value;
		end loop;
	end process calc_parity_bits;
	
	collect_encoded_bits: process (aligned_bits, parity_bit_values)
		variable i, j: integer;
		variable xor_value: std_logic;
	begin
		i := 0;
		j := 0;
		
		for k in 0 to ENCODED_BITS - 1 loop
			if j >= PARITY_BITS - i - 1 then
				encoded_data(k) <= parity_bit_values(i);
			else
				encoded_data(k) <= aligned_bits(i)(j);
			end if;
			
			j := j + 1;
			if j >= PARITY_BITS - i then
				i := i + 1;
				j := 0;
			end if;
		end loop;
	end process collect_encoded_bits;
end architecture behavioral;
