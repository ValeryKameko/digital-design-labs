`timescale 1ns / 1ps


module hamming_decoder_(
		input [ENCODED_BITS - 1:0] encoded_data,
		output [BITS - 1:0] decoded_data,
		output reg [BITS - 1:0] has_errors,
		output has_error
    );
`include "libraries/math/math.v"

parameter BITS = 6;
localparam integer PARITY_BITS = inverse_hamming_value_(BITS);
localparam integer ENCODED_BITS = BITS + PARITY_BITS;

wire [ENCODED_BITS:1] encoded_bit_values = encoded_data;
reg [PARITY_BITS - 1:0] error_bits;
reg [BITS - 1:0] decoded_bits;

integer i, j, k, h;

always @(encoded_data)
begin : decode_data
	j = 0;
	h = 0;
	error_bits = 0;
	
	for (i = 1; i <= ENCODED_BITS; i = i + 1)
	begin
		if (i & (i - 1))
		begin
			decoded_bits[j] = encoded_bit_values[i];
			j = j + 1;
			
			for (k = 0; k < PARITY_BITS; k = k + 1)
				if ((1 << k) & i)
					error_bits[k] = error_bits[k] ^ encoded_bit_values[i];
		end
		else
		begin
			error_bits[h] = error_bits[h] ^ encoded_bit_values[i];
			h = h + 1;
		end;
	end;
	
	has_errors <= 0;
	if (error_bits != 0)
	begin
		j = 0;
		for (i = 1; i <= ENCODED_BITS; i = i + 1)
			if (i & (i - 1))
			begin
				if (i == error_bits)
				begin
					decoded_bits[j] = ~decoded_bits[j];
					has_errors[j] <= 1;
				end;
				j = j + 1;
			end;
	end;
end : decode_data;

assign decoded_data = decoded_bits;
assign has_error = |has_errors;

endmodule
