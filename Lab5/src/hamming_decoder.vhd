library ieee;
library math;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use math.math_pkg.all;


entity hamming_decoder is
	generic(
		BITS: natural := 3
	);
	port(
		encoded_data: in std_logic_vector(inverse_hamming_value(BITS) + BITS - 1 downto 0);
		decoded_data: out std_logic_vector(BITS - 1 downto 0);
		has_errors: out std_logic_vector(BITS - 1 downto 0);
		has_error: out std_logic
	);
end entity hamming_decoder;
	
architecture behavioral of hamming_decoder is
	constant UNSIGNED_BITS: natural := 64;
	constant PARITY_BITS: natural := inverse_hamming_value(BITS);
	constant ENCODED_BITS: natural := BITS + PARITY_BITS;
begin	
	decode_data: process (encoded_data)
		variable j, h: integer;
		variable encoded_bit_values: std_logic_vector(ENCODED_BITS downto 1);	
		variable error_bits: std_logic_vector(PARITY_BITS - 1 downto 0);
		variable decoded_bits: std_logic_vector(BITS - 1 downto 0);
		variable has_error_bits: std_logic_vector(BITS - 1 downto 0);
	begin
		encoded_bit_values := encoded_data;
		
		j := 0;
		h := 0;
		error_bits := (others => '0');
		
		for i in 1 to ENCODED_BITS loop
			if (to_unsigned(i, UNSIGNED_BITS) and to_unsigned(i - 1, UNSIGNED_BITS)) /= 0 then
				decoded_bits(j) := encoded_bit_values(i);
				j := j + 1;
				
				for k in 0 to PARITY_BITS - 1 loop
					if (to_unsigned(2 ** k, UNSIGNED_BITS) and to_unsigned(i, UNSIGNED_BITS)) /= 0 then
						error_bits(k) := error_bits(k) xor encoded_bit_values(i);
					end if;
				end loop;
			else
				error_bits(h) := error_bits(h) xor encoded_bit_values(i);
				h := h + 1;
			end if;
		end loop;
		
		has_error_bits := (others => '0');
		if error_bits /= (error_bits'range => '0') then
			j := 0;
			for i in 1 to ENCODED_BITS loop
				if (to_unsigned(i, UNSIGNED_BITS) and to_unsigned(i - 1, UNSIGNED_BITS)) /= 0 then
					if i = to_integer(unsigned(error_bits)) then
						decoded_bits(j) := not decoded_bits(j);
						has_error_bits(j) := '1';
					end if;
					j := j + 1;
				end if;
			end loop;
		end if;
		decoded_data <= decoded_bits;
		has_errors <= has_error_bits;
		if has_error_bits /= (has_error_bits'range => '0') then
			has_error <= '1';
		else
			has_error <= '0';
		end if;
	end process decode_data;
end architecture behavioral;
