library ieee;
use ieee.std_logic_1164.all;

entity replication_encoder is
	generic(
		N: natural := 4;
		BITS: natural := 3
	);
	port(
		data: in std_logic_vector(BITS - 1 downto 0);
		encoded_data: out std_logic_vector(BITS * N - 1 downto 0)
	);
end entity replication_encoder;

architecture behavioral of replication_encoder is
begin
	replicate_bits: for i in data'range generate
		encoded_data((i + 1) * N - 1 downto i * N) <= (others => data(i));
	end generate replicate_bits;
end architecture behavioral;