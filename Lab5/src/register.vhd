library ieee;
use ieee.std_logic_1164.all;

entity registerN is
	generic(
		BITS: natural := 8
	);
	port(
		set, reset, clk: in std_logic;
		write_enable: in std_logic;
		din: in std_logic_vector(BITS - 1 downto 0);
		read_enable: in std_logic;
		dout: out std_logic_vector(BITS - 1 downto 0)
	);
end entity registerN;

architecture behavioral of registerN is
	signal storing_value: std_logic_vector(BITS - 1 downto 0);
begin
	store_value: process (clk, reset, set, din, write_enable)
	begin
		if reset = '1' then
			storing_value <= (others => '0');
		elsif set = '1' then
			storing_value <= (others => '1');
		elsif rising_edge(clk) and write_enable = '1' then
			storing_value <= din;
		end if;
	end process store_value;
	
	dout <= storing_value when read_enable = '1' else (dout'range => 'Z');
end architecture behavioral;
