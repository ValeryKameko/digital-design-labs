`timescale 1ns / 1ps


module replication_encoder_(
		input [BITS - 1:0] data,
		output [N * BITS - 1:0] encoded_data
    );

parameter N = 4;
parameter BITS = 3;

genvar i;

for (i = 0; i < BITS; i = i + 1)
	assign encoded_data[i * N +: N] = {N{data[i]}};

endmodule
