library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;


entity lifo_ram is
	generic(
		RAM_TYPE: string := "auto";
		DEPTH: natural := 1024;
		DATA_BITS: natural := 8
	);
	port(
		reset, clk: in std_logic;
		write_enable: in std_logic;
		din: in std_logic_vector(DATA_BITS - 1 downto 0);
		read_enable: in std_logic;
		dout: out std_logic_vector(DATA_BITS - 1 downto 0);
		full, empty: out std_logic
	);
end entity lifo_ram;

architecture behavioral of lifo_ram is
	constant ADDRESS_BITS: integer := integer(ceil(log2(real(DEPTH)))); 
	subtype word_t is std_logic_vector(DATA_BITS - 1 downto 0);
	type ram_t is array(0 to 2 ** ADDRESS_BITS - 1) of word_t;
	
	signal data: ram_t;
	signal is_empty, is_full: std_logic;
	signal address: integer range 0 to DEPTH - 1;
	signal temp_dout: word_t;
	
	attribute ram_style: string;
	attribute ram_style of data: signal is RAM_TYPE;
begin
	-- Read/Write values
	read_write_values: process (clk, reset)
	begin
		if reset = '1' then
			address <= 0;
		elsif rising_edge(clk) then
			if read_enable = '0' and write_enable = '1' and is_full = '0' then
				data(address) <= din;
				address <= (address + 1) mod DEPTH;
			elsif read_enable = '1' and write_enable = '0' and is_empty = '0' then
				temp_dout <= data((address + DEPTH - 1) mod DEPTH);
				address <= (address + DEPTH - 1) mod DEPTH;
			end if;
		end if;
	end process read_write_values;
	
	dout <= temp_dout when read_enable = '1' else (dout'range => 'Z');
	
	-- Update state
	update_state: process (clk, reset)
	begin
		if reset = '1' then
			is_full <= '0';
			is_empty <= '1';
		elsif rising_edge(clk) then
			if read_enable = '0' and write_enable = '1' then
				is_empty <= '0';
				if address = DEPTH - 1 and is_empty = '0' then
					is_full <= '1';
				end if;
			elsif read_enable = '1' and write_enable = '0' then
				is_full <= '0';
				if address = 1 and is_full = '0' then
					is_empty <= '1';
				end if;
			end if;
		end if;
	end process update_state;
	
	full <= is_full;
	empty <= is_empty;
end architecture behavioral;
