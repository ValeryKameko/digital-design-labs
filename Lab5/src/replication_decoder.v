`timescale 1ns / 1ps


module replication_decoder_(
		input [N * BITS - 1:0] encoded_data,
		output [BITS - 1:0] decoded_data,
		output reg [BITS - 1:0] has_errors,
		output has_error
    );

parameter N = 4;
parameter BITS = 3;

function calc_majority_bit(input [N - 1:0] bits);
	integer i;
	integer count [1:0];
begin
	count[0] = 0;
	count[1] = 0;
	for (i = 0; i < N; i = i + 1)
		count[bits[i]] = count[bits[i]] + 1;
	calc_majority_bit = (count[1] > count[0]);
end
endfunction

genvar i;
for (i = 0; i < BITS; i = i + 1)
begin
	wire [N - 1:0] bits_group = encoded_data[i * N +: N];
	assign decoded_data[i] = calc_majority_bit(bits_group[N - 1:0]);
	
	always @*
	begin
		if (bits_group == {N{1'b0}} || bits_group == {N{1'b1}})
			has_errors[i] <= 0;
		else
			has_errors[i] <= 1;
	end;
end

assign has_error = |has_errors;

endmodule

