library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


entity parity_decoder is
	generic(
		BITS: natural := 3
	);
	port(
		encoded_data: in std_logic_vector(integer(ceil((sqrt(real(8 * BITS + 1)) - 1.0) / 2.0)) + BITS downto 0);
		decoded_data: out std_logic_vector(BITS - 1 downto 0);
		has_errors: out std_logic_vector(BITS - 1 downto 0);
		has_error: out std_logic
	);
end entity parity_decoder;

architecture behavioral of parity_decoder is
	constant PARITY_BITS: natural := integer(ceil((sqrt(real(8 * BITS + 1)) - 1.0) / 2.0)) + 1;
	constant ENCODED_BITS: natural := BITS + PARITY_BITS;
	
	type t_std_logic_2d is array (0 to PARITY_BITS - 1) of std_logic_vector(PARITY_BITS - 1 downto 0);
	signal aligned_bits: t_std_logic_2d;
	signal error_bits: std_logic_vector(PARITY_BITS - 1 downto 0);
	signal has_error_bits: std_logic_vector(BITS - 1 downto 0);
begin
	align_bits: process (encoded_data)
		variable i, j: integer;
	begin
		i := 0;
		j := 0;
		
		for k in 0 to ENCODED_BITS - 1 loop
			aligned_bits(i)(j) <= encoded_data(k);
			j := j + 1;
			if j >= PARITY_BITS - i then
				i := i + 1;
				j := 0;				
			end if;
		end loop;
	end process align_bits;

	calc_errors_bits: process (aligned_bits)
		variable i, j: integer;
		variable xor_value: std_logic;
	begin		
		for k in 0 to PARITY_BITS - 1 loop
			i := k;
			j := PARITY_BITS - k - 1;
			
			xor_value := '0';
			for h in 0 to i - 1 loop
				xor_value := xor_value xor aligned_bits(h)(j);
			end loop;
			
			for h in 0 to j - 1 loop
				xor_value := xor_value xor aligned_bits(i)(h);
			end loop;
			error_bits(k) <= aligned_bits(i)(j) xor xor_value;
		end loop;
	end process calc_errors_bits;
	
	decode_data: process (aligned_bits, error_bits)
		variable i, j: integer;
		variable xor_value: std_logic;
		variable temp_error: std_logic;
	begin
		i := 0;
		j := 0;
		
		for k in 0 to BITS - 1 loop
			temp_error := error_bits(i) and error_bits(PARITY_BITS - j - 1);
			has_error_bits(k) <= temp_error;
			decoded_data(k) <= aligned_bits(i)(j) xor temp_error;
			
			j := j + 1;	
			if j >= PARITY_BITS - i - 1 then
				i := i + 1;
				j := 0;
			end if;
		end loop;
	end process decode_data;
	
	has_errors <= has_error_bits;
	has_error <= '0' when has_error_bits = (has_error_bits'range => '0') else '1';
end architecture behavioral;
