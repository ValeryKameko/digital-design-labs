`timescale 1ns / 1ps


module register_file_(
		input set,
		input reset,
		input clk,
		
		input write_enable,
		input [ADDRESS_BITS - 1:0] write_address,
		input [DATA_BITS - 1:0] write_port,
		
		input [READ_PORTS_COUNT * ADDRESS_BITS - 1:0] read_addresses,
		output reg [READ_PORTS_COUNT * DATA_BITS - 1:0] read_ports
    );
`include "../libraries/math/math.v"

function calc_read_enable(integer index);
	integer i;
	reg result;
begin
	result = 0;	
	for (i = 0; i < READ_PORTS_COUNT; i = i + 1)
		result = result | read_enables[i][index];
	calc_read_enable = result;
end
endfunction

parameter SIZE = 1 << 4;
parameter DATA_BITS = 8;
parameter READ_PORTS_COUNT = 2;

localparam ADDRESS_BITS = ceil_log2_(SIZE);
localparam FULL_SIZE = 1 << ADDRESS_BITS;

wire [FULL_SIZE - 1:0] read_enables [READ_PORTS_COUNT - 1:0];
wire [FULL_SIZE - 1:0] write_enables;

wire [DATA_BITS - 1:0] read_values [FULL_SIZE - 1:0][READ_PORTS_COUNT - 1:0];

// Generate registers
genvar i, j;
for (i = 0; i < SIZE; i = i + 1)
begin
	wire [DATA_BITS - 1:0] read_value;
	
	reg read_enable;

	always @(read_enables)
		read_enable <= calc_read_enable(i);
	
	register_ #(.BITS(DATA_BITS)) store_register_value(
		.set(set),
		.reset(reset),
		.clk(clk),
		
		.write_enable(write_enables[i] & write_enable),
		.din(write_port),
		
		.read_enable(read_enable),
		.dout(read_value)
	);
	
	for (j = 0; j < READ_PORTS_COUNT; j = j + 1)
		assign read_values[i][j] = read_value;
end;

// Generate read ports
for (i = 0; i < READ_PORTS_COUNT; i = i + 1)
begin
	integer index;
	always @(read_values or read_addresses)
	begin
		index = read_addresses[i * ADDRESS_BITS +: ADDRESS_BITS];
		read_ports[i * DATA_BITS +: DATA_BITS] = read_values[index][i];
	end;

	decoder_ #(.N(ADDRESS_BITS)) decode_read_address(
		.inputs(read_addresses[i * ADDRESS_BITS +: ADDRESS_BITS]),
		.outputs(read_enables[i])
	);
end;

// Generate write port
decoder_ #(.N(ADDRESS_BITS)) decode_write_address(
	.inputs(write_address),
	.outputs(write_enables)
);

endmodule
