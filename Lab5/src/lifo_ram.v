`timescale 1ns / 1ps


module lifo_ram_(
		input reset,
		input clk,
		
		input write_enable,
		input [DATA_BITS - 1:0] din,
		
		input read_enable,
		output [DATA_BITS - 1:0] dout,
		
		output full,
		output empty
    );
`include "../libraries/math/math.v"

parameter DEPTH = 1024;
parameter DATA_BITS = 8;
parameter RAM_TYPE = "auto";
localparam ADDRESS_BITS = ceil_log2_(DEPTH);

reg is_empty;
reg is_full;
integer address;
reg [DATA_BITS - 1:0] temp_dout;
(* ram_style = RAM_TYPE *) reg [DATA_BITS - 1:0] data [2 ** ADDRESS_BITS - 1:0];

// Read/Write values
always @(posedge clk or posedge reset)
begin
	if (reset == 1)
		address <= 0;
	else if (read_enable == 0 && write_enable == 1 && is_full == 0)
	begin
		data[address] <= din;
		address <= (address + 1) % DEPTH;
	end
	else if (read_enable == 1 && write_enable == 0 && is_empty == 0)
	begin
		temp_dout <= data[(address + DEPTH - 1) % DEPTH];
		address <= (address + DEPTH - 1) % DEPTH;
	end;
end;

assign dout = (read_enable == 1) ? temp_dout : {DATA_BITS{1'bZ}};

// Update state
always @(posedge clk or posedge reset)
begin
	if (reset == 1)
		{is_full, is_empty} <= 'b01;
	else if (read_enable == 0 && write_enable == 1)
	begin
		is_empty <= 0;
		if (address == DEPTH - 1 && is_empty == 0)
			is_full <= 1;
	end
	else if (read_enable == 1 && write_enable == 0)
	begin
		is_full <= 0;
		if (address == 1 && is_full == 0)
			is_empty <= 1;
	end;
end;

assign full = is_full;
assign empty = is_empty;

endmodule
