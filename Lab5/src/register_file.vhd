library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity register_file is
	generic(
		DATA_BITS: natural := 8;
		SIZE: natural := 2 ** 4;
		READ_PORTS_COUNT: natural := 2
	);
	port(
		set, reset, clk: in std_logic;
			
		write_enable: in std_logic;
		write_address: in std_logic_vector(integer(ceil(log2(real(SIZE)))) - 1 downto 0);
		write_port: in std_logic_vector(DATA_BITS - 1 downto 0);
		
		read_addresses: in std_logic_vector(READ_PORTS_COUNT * integer(ceil(log2(real(SIZE)))) - 1 downto 0);
		read_ports: out std_logic_vector(READ_PORTS_COUNT * DATA_BITS - 1 downto 0)
	);
end entity register_file;

architecture behavioral of register_file is
	component decoder is
		generic(
			N: natural
		);
		port(
			inputs: in std_logic_vector(N - 1 downto 0);
			outputs: out std_logic_vector(2 ** N - 1 downto 0)
		);
	end component decoder;
	
	component registerN is
		generic(
			BITS: natural
		);
		port(
			set, reset, clk: in std_logic;
			write_enable: in std_logic;
			din: in std_logic_vector(BITS - 1 downto 0);
			read_enable: in std_logic;
			dout: out std_logic_vector(BITS - 1 downto 0)
		);
	end component registerN;
		
	constant ADDRESS_BITS: integer := integer(ceil(log2(real(SIZE))));
	constant FULL_SIZE: integer := 2 ** ADDRESS_BITS;
	
	type read_values_row_t is array(0 to FULL_SIZE - 1) of std_logic_vector(DATA_BITS - 1 downto 0);
	type read_values_array_t is array(0 to READ_PORTS_COUNT - 1) of read_values_row_t;
	signal read_values: read_values_array_t;
	
	type read_enables_array_t is array(0 to READ_PORTS_COUNT - 1) of std_logic_vector(FULL_SIZE - 1 downto 0);
	signal read_enables: read_enables_array_t;
	
	signal write_enables: std_logic_vector(FULL_SIZE - 1 downto 0);
begin
	-- Generate registers
	generate_registers: for i in 0 to SIZE - 1 generate
		signal read_enable: std_logic;
		signal read_value: std_logic_vector(DATA_BITS - 1 downto 0); 
	begin
		calc_read_enable: process (read_enables)
			variable result: std_logic;
		begin
			result := '0';
			for j in 0 to READ_PORTS_COUNT - 1 loop
				result := result or read_enables(j)(i);
			end loop;
			read_enable <= result;
		end process calc_read_enable;
	
		store_values: registerN
			generic map (BITS => DATA_BITS)
			port map (
				set => set, 
				reset => reset, 
				clk => clk,
				write_enable => write_enables(i) and write_enable,
				din => write_port, 
				read_enable => read_enable,
				dout => read_value);

		assign_read_values: for j in 0 to READ_PORTS_COUNT - 1 generate
			read_values(j)(i) <= read_value;
		end generate assign_read_values;
	end generate generate_registers;
	
	-- Generate write selector
	select_write_address: decoder
		generic map (N => ADDRESS_BITS)
		port map (inputs => write_address, outputs => write_enables);
		
	-- Read values
	generate_read_values: for i in 0 to READ_PORTS_COUNT - 1 generate
		signal read_address: std_logic_vector(ADDRESS_BITS - 1 downto 0);
	begin
		read_address <= read_addresses((i + 1) * ADDRESS_BITS - 1 downto i * ADDRESS_BITS);
		
		select_read_addresses: decoder
			generic map (N => ADDRESS_BITS)
			port map (inputs => read_address, outputs => read_enables(i));
		
		assign_read_ports: process (read_values, read_address)
			variable index: integer;
			variable read_value: std_logic_vector(DATA_BITS - 1 downto 0);
		begin
			index := to_integer(unsigned(read_address));
			read_value := read_values(i)(index);
			read_ports((i + 1) * DATA_BITS - 1 downto i * DATA_BITS) <= read_value;
		end process assign_read_ports;
	end generate generate_read_values;
end architecture behavioral;

