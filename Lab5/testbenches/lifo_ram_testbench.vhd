library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.random_pkg.all;

entity lifo_ram_testbench is
end entity lifo_ram_testbench;

architecture testbench of lifo_ram_testbench is
	component lifo_ram is
		generic(
			RAM_TYPE: string := "auto";
			DEPTH: natural := 1024;
			DATA_BITS: natural := 8
		);
		port(
			reset, clk: in std_logic;
			write_enable: in std_logic;
			din: in std_logic_vector(DATA_BITS - 1 downto 0);
			read_enable: in std_logic;
			dout: out std_logic_vector(DATA_BITS - 1 downto 0);
			full, empty: out std_logic
		);
	end component lifo_ram;
	
	constant RAM_TYPE: string := "auto";
	constant DEPTH: natural := 4;
	constant DATA_BITS: natural := 8;
	constant DELAY: time := 10 ns;
	
	signal reset, clock: std_logic := '0';
	signal clk: std_logic;
	signal write_enable: std_logic := '0';
	signal din: std_logic_vector(DATA_BITS - 1 downto 0) := (others => '0');
	signal read_enable: std_logic := '0';
	signal dout: std_logic_vector(DATA_BITS - 1 downto 0);
	signal full, empty: std_logic;
begin
	uut: lifo_ram
		generic map (RAM_TYPE => RAM_TYPE, DEPTH => DEPTH, DATA_BITS => DATA_BITS)
		port map (
			reset => reset,
			clk => clk,
			write_enable => write_enable,
			din => din,
			read_enable => read_enable,
			dout => dout,
			full => full,
			empty => empty);

	clk <= clock;
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		-- Reset
		reset <= '1';
		wait until falling_edge(clock);
		reset <= '0';


		--	Check push
		write_enable <= '1';
		for i in 0 to DEPTH - 1 loop
			din <= std_logic_vector(to_unsigned(i, din'length));
			wait until falling_edge(clock);
		end loop;
		write_enable <= '0';
			assert full = '1'
				report "Test failed: push"
				severity FAILURE;

		--	Check pop
		read_enable <= '1';
		for i in DEPTH - 1 downto 0 loop
			wait until falling_edge(clock);
				assert dout = std_logic_vector(to_unsigned(i, dout'length))
					report "Test failed: pop"
					severity FAILURE;
		end loop;
		read_enable <= '0';
			assert empty = '1'
				report "Test failed: pop"
				severity FAILURE;


		assert false
			report "End simulation"
			severity FAILURE;
	end process;
end architecture testbench;
