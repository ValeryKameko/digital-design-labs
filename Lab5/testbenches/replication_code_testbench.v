`timescale 1ns / 1ps
`include "assert.v"


module replication_code__testbench;

reg clock = 0;
reg [BITS - 1:0] data;
reg [BITS * N - 1:0] decoder_encoded_data;
wire [N * BITS - 1:0] encoded_data;
wire [BITS - 1:0] decoded_data;
wire [BITS - 1:0] has_errors;
wire has_error;

localparam BITS = 10;
localparam N = 3;
localparam DELAY = 10;
integer seed = 123;

replication_encoder_ #(
	.N(N),
	.BITS(BITS)
) uut_encoder(
	.data(data),
	.encoded_data(encoded_data)
);

replication_decoder_ #(
	.N(N),
	.BITS(BITS)
) uut_decoder(
	.encoded_data(decoder_encoded_data),
	.decoded_data(decoded_data),
	.has_errors(has_errors),
	.has_error(has_error)
);

task randomize_data;
	integer i;
begin
	for (i = 0; i < BITS; i = i + 1)
		data[i] = $random(seed) % 2;
end
endtask

task randomize_encoded_data_errors(integer group_max_errors);
	integer i, j;
	integer choosed_index, index;
	integer group_errors_count;
	reg [N - 1:0] choosed;
	reg is_choosed;
begin
	for (i = 0; i < BITS; i = i + 1)
	begin
		choosed = 0;
		group_errors_count = $random(seed) % (group_max_errors + 1);
		
		for (j = 0; j < group_errors_count; j = j + 1)
		begin
			is_choosed = 0;
			while (!is_choosed)
			begin
				choosed_index = $random(seed) % N;
				if (!choosed[choosed_index])
					is_choosed = 1;
				choosed[choosed_index] = 1;
			end;
		end;
		
		decoder_encoded_data[i * N +: N] = decoder_encoded_data[i * N +: N] ^ choosed;
	end;
end
endtask

always
	clock = #(DELAY / 2) ~clock;

integer i;

initial
begin
	// Reset
	data <= 'b0;
	@(negedge clock);
	
	// Check encode/decode without error
	for (i = 0; i < 3; i = i + 1)
	begin
		randomize_data; #0;
		decoder_encoded_data = encoded_data; #0;
		@(negedge clock);
			`assert_(data == decoded_data,
				"Test fail: encode/decode without error");
	end;
	
	// Check encode/decode with error
	for (i = 0; i < 3; i = i + 1)
	begin
		randomize_data; #0;
		decoder_encoded_data = encoded_data; #0;
		randomize_encoded_data_errors(N / 2); #0;
		@(negedge clock);
			`assert_(data == decoded_data,
				"Test fail: encode/decode without error");
	end;
	
	$stop;
end;

endmodule
