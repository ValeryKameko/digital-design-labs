`timescale 1ns / 1ps
`include "assert.v"


module register_file__testbench;
`include "../libraries/math/math.v"

reg clock = 0;
reg set = 0, reset = 0;
wire clk = clock;
reg write_enable = 0;
reg [ADDRESS_BITS - 1:0] write_address = 0;
reg [DATA_BITS - 1:0] write_port = 0;
reg [READ_PORTS_COUNT * ADDRESS_BITS - 1:0] read_addresses = 0;
wire [READ_PORTS_COUNT * DATA_BITS - 1:0] read_ports;

localparam SIZE = 1 << 2;
localparam DATA_BITS = 2;
localparam READ_PORTS_COUNT = 2;
localparam ADDRESS_BITS = ceil_log2_(SIZE);
localparam FULL_SIZE = 1 << ADDRESS_BITS;

localparam DELAY = 10;
integer seed = 123;


reg [ADDRESS_BITS - 1:0] address;
reg [DATA_BITS - 1:0] data;
integer port;

register_file_ #(
	.SIZE(SIZE),
	.DATA_BITS(DATA_BITS),
	.READ_PORTS_COUNT(READ_PORTS_COUNT)
) uut(
	.set(set),
	.reset(reset),
	.clk(clk),
	.write_enable(write_enable),
	.write_address(write_address),
	.write_port(write_port),
	.read_addresses(read_addresses),
	.read_ports(read_ports)
);

task randomize_address;
	integer i;
begin
	for (i = 0; i < ADDRESS_BITS; i = i + 1)
		address[i] = $random(seed) % 2;
end
endtask

task randomize_data;
	integer i;
begin
	for (i = 0; i < DATA_BITS; i = i + 1)
		data[i] = $random(seed) % 2;
end
endtask

task randomize_port;
begin
	port = ($random(seed) % READ_PORTS_COUNT + READ_PORTS_COUNT) % READ_PORTS_COUNT;
end
endtask


always
	clock = #(DELAY / 2) ~clock;

integer i, j;

initial
begin
	// Reset
	reset <= 1;
	@(negedge clock);
	reset <= 0;

	
	// Check registers write value
	for (i = 0; i < 5; i = i + 1)
	begin
		randomize_address; #0;
		randomize_data; #0;
		randomize_port; #0;
		
		write_enable <= 1;
		write_address <= address;
		write_port <= data;
		@(negedge clock);
		write_enable <= 0;
		
		read_addresses[port * ADDRESS_BITS +: ADDRESS_BITS] <= address;
		@(negedge clock);
			`assert_(data == read_ports[port * DATA_BITS +: DATA_BITS],
				"Test fail: registers write value");
	end;
	
	// Check reset
	set <= 1;
	@(negedge clock);
	set <= 0;
	
	reset <= 1;
	@(negedge clock);
	reset <= 0;
	
	for (i = 0; i < READ_PORTS_COUNT; i = i + 1)
	begin
		for (j = 0; j < SIZE; j = j + 1)
		begin
			read_addresses[i * ADDRESS_BITS +: ADDRESS_BITS] <= j;
			@(negedge clock);
				`assert_(read_ports[port * DATA_BITS +: DATA_BITS] == {DATA_BITS{1'b0}},
					"Test fail: reset");
		end;
	end;
	
	
	// Check set
	reset <= 1;
	@(negedge clock);
	reset <= 0;
	
	set <= 1;
	@(negedge clock);
	set <= 0;
	
	for (i = 0; i < READ_PORTS_COUNT; i = i + 1)
	begin
		for (j = 0; j < SIZE; j = j + 1)
		begin
			read_addresses[i * ADDRESS_BITS +: ADDRESS_BITS] <= j;
			@(negedge clock);
				`assert_(read_ports[port * DATA_BITS +: DATA_BITS] == {DATA_BITS{1'b1}},
					"Test fail: set");
		end;
	end;


	$stop;
end;

endmodule
