library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rom_testbench is
end entity rom_testbench;

architecture testbench of rom_testbench is
	component rom is
		generic(
			ROM_DATA_VALUES: std_logic_vector;
			ADDRESS_BITS: natural;
			DATA_BITS: natural
		);
		port(
			enable: in std_logic;
			address: in std_logic_vector(ADDRESS_BITS - 1 downto 0);
			dout: out std_logic_vector(DATA_BITS - 1 downto 0)
		);
	end component rom;
	
	constant ROM_DATA_VALUES: std_logic_vector := (
			X"E002" &
			X"10A0" &
			X"0506" &
			X"0010"
		);
	constant ADDRESS_BITS: natural := 2;
	constant DATA_BITS: natural := 16;
	constant DELAY: time := 10 ns;

	signal clock: std_logic := '0';
	signal enable: std_logic := '0';
	signal address: std_logic_vector(ADDRESS_BITS - 1 downto 0) := (others => '0');
	signal dout: std_logic_vector(DATA_BITS - 1 downto 0);
begin
	uut: rom
		generic map (ROM_DATA_VALUES => ROM_DATA_VALUES, ADDRESS_BITS => ADDRESS_BITS, DATA_BITS => DATA_BITS)
		port map (enable => enable, address => address, dout => dout);

	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		--	Check store
		enable <= '1';
		for i in 0 to 2 ** ADDRESS_BITS - 1 loop
			address <= std_logic_vector(to_unsigned(i, address'length));
			wait until falling_edge(clock);
		end loop;

		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
