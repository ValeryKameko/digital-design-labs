`timescale 1ns / 1ps
`include "assert.v"


module ram__testbench;

reg clock = 0;
wire clk = clock;
reg write_enable = 0;
reg [ADDRESS_BITS - 1:0] address = 0;
reg [DATA_BITS - 1:0] din = 0;
wire [DATA_BITS - 1:0] dout;


localparam RAM_TYPE = "block";
localparam ADDRESS_BITS = 10;
localparam DATA_BITS = 8;

localparam DELAY = 10;
integer seed = 123;

ram_ #(
	.RAM_TYPE(RAM_TYPE),
	.ADDRESS_BITS(ADDRESS_BITS),
	.DATA_BITS(DATA_BITS)
) uut(
	.clk(clk),
	.write_enable(write_enable),
	.address(address),
	.din(din),
	.dout(dout)
);

task randomize_address;
	integer i;
begin
	for (i = 0; i < ADDRESS_BITS; i = i + 1)
		address[i] = $random(seed) % 2;
end
endtask

task randomize_din;
	integer i;
begin
	for (i = 0; i < DATA_BITS; i = i + 1)
		din[i] = $random(seed) % 2;
end
endtask


always
	clock = #(DELAY / 2) ~clock;

integer i;

initial
begin	
	// Check store
	for (i = 0; i < 5; i = i + 1)
	begin
		randomize_address;
		randomize_din;
		
		write_enable <= 1;
		@(negedge clock);
		write_enable <= 0;
		
		@(negedge clock);
			`assert_(din == dout,
				"Test fail: store");
	end;

	$stop;
end;

endmodule
