library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.random_pkg.all;

entity ram_testbench is
end entity ram_testbench;

architecture testbench of ram_testbench is
	component ram is
		generic(
			RAM_TYPE: string;
			ADDRESS_BITS: natural;
			DATA_BITS: natural
		);
		port(
			clk, write_enable: in std_logic;
			address: in std_logic_vector(ADDRESS_BITS - 1 downto 0);
			din: in std_logic_vector(DATA_BITS - 1 downto 0);
			dout: out std_logic_vector(DATA_BITS - 1 downto 0)
		);
	end component ram;
	
	for all: ram use entity work.hamming_ecc_ram(behavioral);
	
	constant RAM_TYPE: string := "distributed";
	constant ADDRESS_BITS: natural := 10;
	constant DATA_BITS: natural := 8;
	constant DELAY: time := 10 ns;

	signal clock: std_logic := '0';
	signal clk: std_logic;
	signal write_enable: std_logic := '0';
	signal address: std_logic_vector(ADDRESS_BITS - 1 downto 0) := (others => '0');
	signal din: std_logic_vector(DATA_BITS - 1 downto 0) := (others => '0');
	signal dout: std_logic_vector(DATA_BITS - 1 downto 0);
begin
	uut: ram
		generic map (RAM_TYPE => RAM_TYPE, ADDRESS_BITS => ADDRESS_BITS, DATA_BITS => DATA_BITS)
		port map (clk => clk, write_enable => write_enable, address => address, din => din, dout => dout);

	clk <= clock;

	clock <= not clock after (DELAY / 2);
	
	test: process
		variable seed1, seed2: integer := 228;
	begin
		--	Check store
		for i in 0 to 4 loop
			random_std_logic_vector(seed1, seed2, address);
			random_std_logic_vector(seed1, seed2, din);
			
			write_enable <= '1';
			wait until falling_edge(clock);
			write_enable <= '0';
			
			wait until falling_edge(clock);
				assert din = dout
					report "Test fail: store"
					severity FAILURE;
		end loop;

		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
