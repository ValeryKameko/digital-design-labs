`timescale 1ns / 1ps
`include "assert.v"


module lifo_ram__testbench;

reg clock = 0;

reg reset = 0;
wire clk = clock;
reg write_enable = 0;
reg [DATA_BITS - 1:0] din = 0;
reg read_enable = 0;
wire [DATA_BITS - 1:0] dout;
wire full, empty;

localparam DEPTH = 4;
localparam DATA_BITS = 8;
localparam RAM_TYPE = "auto";
localparam DELAY = 10;

lifo_ram_ #(
	.DEPTH(DEPTH),
	.DATA_BITS(DATA_BITS),
	.RAM_TYPE(RAM_TYPE)
) uut(
	.reset(reset),
	.clk(clk),
	.write_enable(write_enable),
	.din(din),
	.read_enable(read_enable),
	.dout(dout),
	.full(full),
	.empty(empty)
);

always
	clock = #(DELAY / 2) ~clock;

integer i;

initial
begin
	// Reset
	reset <= 1;
	@(negedge clock);
	reset <= 0;

	// Check push
	write_enable <= 1;
	for (i = 0; i < DEPTH; i = i + 1)
	begin
		din <= i;
		@(negedge clock);
	end;
	write_enable <= 0;
		`assert_(full == 1,
			"Test fail: push");

	// Check pop
	read_enable <= 1;
	for (i = DEPTH - 1; i >= 0; i = i - 1)
	begin
		@(negedge clock);
			`assert_(dout == i,
				"Test fail: pop");
	end;
	read_enable <= 0;
		`assert_(empty == 1,
			"Test fail: pop");

	$stop;
end;

endmodule
