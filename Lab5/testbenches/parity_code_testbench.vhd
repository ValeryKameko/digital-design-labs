library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use work.random_pkg.all;

entity parity_code_testbench is
end entity parity_code_testbench;

architecture testbench of parity_code_testbench is
	component parity_encoder is
		generic(
			BITS: natural
		);
		port(
			data: in std_logic_vector(BITS - 1 downto 0);
			encoded_data: out std_logic_vector(integer(ceil((sqrt(real(8 * BITS + 1)) - 1.0) / 2.0)) + BITS downto 0)
		);
	end component parity_encoder;

	component parity_decoder is
		generic(
			BITS: natural
		);
		port(
			encoded_data: in std_logic_vector(integer(ceil((sqrt(real(8 * BITS + 1)) - 1.0) / 2.0)) + BITS downto 0);
			decoded_data: out std_logic_vector(BITS - 1 downto 0);
			has_errors: out std_logic_vector(BITS - 1 downto 0);
			has_error: out std_logic
		);
	end component parity_decoder;
	
	constant BITS: natural := 6;
	constant PARITY_BITS: natural := integer(ceil((sqrt(real(8 * BITS + 1)) - 1.0) / 2.0)) + 1;
	constant ENCODED_BITS: natural := BITS + PARITY_BITS;
	constant DELAY: time := 10 ns;
	
	signal clock: std_logic := '0';
	signal data: std_logic_vector(BITS - 1 downto 0);
	signal encoded_data, decoder_encoded_data: std_logic_vector(ENCODED_BITS - 1 downto 0);
	signal decoded_data, has_errors: std_logic_vector(BITS - 1 downto 0);
	signal has_error: std_logic;

	procedure randomize_encoded_data_errors(variable seed1, seed2: inout positive; errors_count: integer; signal decoder_encoded_data: inout std_logic_vector) is
		variable i, j: integer;
		variable choosed_index, index: integer;
		variable group_errors_count: integer;
		variable choosed: bit_vector(ENCODED_BITS - 1 downto 0);
		variable is_choosed: boolean;
	begin
		choosed := (others => '0');
	
		for j in 0 to errors_count - 1 loop
			is_choosed := false;
			while not is_choosed loop
				random_integer(seed1, seed2, 0, ENCODED_BITS, choosed_index);
				is_choosed := choosed(choosed_index) = '1';
				choosed(choosed_index) := '1';
			end loop;
			decoder_encoded_data(choosed_index) <= not decoder_encoded_data(choosed_index);
		end loop;
		wait for 0 ns;
	end procedure randomize_encoded_data_errors; 
begin
	uut_encoder: parity_encoder
		generic map (BITS => BITS)
		port map (data => data, encoded_data => encoded_data);

	uut_decoder: parity_decoder 
		generic map (BITS => BITS)
		port map (encoded_data => decoder_encoded_data, decoded_data => decoded_data, has_errors => has_errors, has_error => has_error);
	
	clock <= not clock after (DELAY / 2);
	
	test: process
		variable seed1, seed2: integer := 228;
	begin
		-- Reset
		data <= (others => '0');
		wait until falling_edge(clock);
		
		
		-- Check encode/decode without error
		for i in 0 to 3 loop
			random_std_logic_vector(seed1, seed2, data); wait for 1 ps;
			decoder_encoded_data <= encoded_data; wait for 1 ps;
			wait until falling_edge(clock);
				assert data = decoded_data report "Test fail: encode/decode without error" severity FAILURE;
		end loop;
		
		-- Check encode/decode with error
		for k in 0 to 3 loop
			random_std_logic_vector(seed1, seed2, data); wait for 1 ps;
			decoder_encoded_data <= encoded_data; wait for 1 ps;
			randomize_encoded_data_errors(seed1, seed2, 1, decoder_encoded_data);
			wait until falling_edge(clock);
				assert data = decoded_data report "Test fail: encode/decode with error" severity FAILURE;
		end loop;


		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
