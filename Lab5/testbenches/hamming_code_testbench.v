`timescale 1ns / 1ps
`include "assert.v"


module hamming_code__testbench;
`include "libraries/math/math.v"

reg clock = 0;
reg [BITS - 1:0] data;
reg [ENCODED_BITS - 1:0] decoder_encoded_data;
wire [ENCODED_BITS - 1:0] encoded_data;
wire [BITS - 1:0] decoded_data;
wire [BITS - 1:0] has_errors;
wire has_error;

localparam BITS = 6;
localparam integer PARITY_BITS = inverse_hamming_value_(BITS);
localparam integer ENCODED_BITS = BITS + PARITY_BITS;
localparam DELAY = 10;
integer seed = 123;

reg [ENCODED_BITS:1] encoded_bit_values;

hamming_encoder_ #(
	.BITS(BITS)
) uut_encoder(
	.data(data),
	.encoded_data(encoded_data)
);

hamming_decoder_ #(
	.BITS(BITS)
) uut_decoder(
	.encoded_data(decoder_encoded_data),
	.decoded_data(decoded_data),
	.has_errors(has_errors),
	.has_error(has_error)
);

task randomize_data;
	integer i;
begin
	for (i = 0; i < BITS; i = i + 1)
		data[i] = $random(seed) % 2;
end
endtask

task randomize_encoded_data_errors(integer errors_count);
	integer i;
	integer choosed_index, index;
	reg [ENCODED_BITS - 1:0] choosed;
	reg is_choosed;
begin
	choosed = 0;
	
	for (i = 0; i < errors_count; i = i + 1)
	begin
		is_choosed = 0;
		while (!is_choosed)
		begin
			choosed_index = $random(seed) % ENCODED_BITS;
			if (!choosed[choosed_index])
				is_choosed = 1;
			choosed[choosed_index] = 1;
		end;
	end;
		
	decoder_encoded_data = decoder_encoded_data ^ choosed;
end
endtask

always
	clock = #(DELAY / 2) ~clock;

integer i;

initial
begin
	// Reset
	data <= 'b0;
	@(negedge clock);
	
	// Check encode/decode without error
	for (i = 0; i < 3; i = i + 1)
	begin
		randomize_data; #0;
		decoder_encoded_data = encoded_data; #0;
		@(negedge clock);
			`assert_(data == decoded_data,
				"Test fail: encode/decode without error");
	end;
	
	// Check encode/decode with error
	for (i = 0; i < 3; i = i + 1)
	begin
		randomize_data; #0;
		decoder_encoded_data = encoded_data; #0;
		randomize_encoded_data_errors(1); #0;
		@(negedge clock);
			`assert_(data == decoded_data,
				"Test fail: encode/decode with error");
	end;
	
	$stop;
end;

endmodule
