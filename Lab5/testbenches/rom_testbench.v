`timescale 1ns / 1ps
`include "assert.v"


module rom__testbench;

reg clock = 0;

reg enable = 0;
reg [ADDRESS_BITS - 1:0] address = 0;
wire [DATA_BITS - 1:0] dout;

localparam [DATA_BITS * 2 ** ADDRESS_BITS - 1:0] ROM_DATA_VALUES = {
	16'hE002,
	16'h10A0,
	16'h0506,
	16'h0010
};
localparam ADDRESS_BITS = 2;
localparam DATA_BITS = 16;

localparam DELAY = 10;
integer seed = 123;

rom_ #(
	.ADDRESS_BITS(ADDRESS_BITS),
	.DATA_BITS(DATA_BITS),
	.ROM_DATA_VALUES(ROM_DATA_VALUES)
) uut(
	.enable(enable),
	.address(address),
	.dout(dout)
);

always
	clock = #(DELAY / 2) ~clock;

integer i;
	
initial
begin	
	// Check read
	enable <= 1;
	for (i = 0; i < 2 ** ADDRESS_BITS; i = i + 1)
	begin
		address <= i;
		@(negedge clock);
	end;


	$stop;
end;

endmodule
