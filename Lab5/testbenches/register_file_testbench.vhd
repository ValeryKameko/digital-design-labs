library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.random_pkg.all;

entity register_file_testbench is
end entity register_file_testbench;

architecture testbench of register_file_testbench is
	component register_file is
		generic(
			DATA_BITS: natural;
			SIZE: natural;
			READ_PORTS_COUNT: natural
		);
		port(
			set, reset, clk: in std_logic;	
			write_enable: in std_logic;
			write_address: in std_logic_vector(integer(ceil(log2(real(SIZE)))) - 1 downto 0);
			write_port: in std_logic_vector(DATA_BITS - 1 downto 0);
			read_addresses: in std_logic_vector(READ_PORTS_COUNT * integer(ceil(log2(real(SIZE)))) - 1 downto 0);
			read_ports: out std_logic_vector(READ_PORTS_COUNT * DATA_BITS - 1 downto 0)
		);
	end component register_file;

	constant DATA_BITS: natural := 8;
	constant SIZE: natural := 2 ** 2;
	constant READ_PORTS_COUNT: natural := 2;
	constant ADDRESS_BITS: natural := integer(ceil(log2(real(SIZE))));
	constant DELAY: time := 10 ns;
	
	signal clock: std_logic := '0';
	signal set, reset, clk: std_logic;
	signal write_enable: std_logic;
	signal write_address: std_logic_vector(ADDRESS_BITS - 1 downto 0);
	signal write_port: std_logic_vector(DATA_BITS - 1 downto 0);
	signal read_addresses: std_logic_vector(READ_PORTS_COUNT * ADDRESS_BITS - 1 downto 0);
	signal read_ports: std_logic_vector(READ_PORTS_COUNT * DATA_BITS - 1 downto 0);
	
	signal address: std_logic_vector(ADDRESS_BITS - 1 downto 0);
	signal data: std_logic_vector(DATA_BITS - 1 downto 0);
begin
	uut: register_file
		generic map (DATA_BITS => DATA_BITS, SIZE => SIZE, READ_PORTS_COUNT => READ_PORTS_COUNT)
		port map (
			set => set, 
			reset => reset, 
			clk => clk,
			write_enable => write_enable,
			write_address => write_address,
			write_port => write_port,
			read_addresses => read_addresses,
			read_ports => read_ports);

	clk <= clock;

	clock <= not clock after (DELAY / 2);
	
	test: process
		variable seed1, seed2: integer := 228;
		variable port_index: integer;
	begin
		-- Reset
		reset <= '1';
		wait until falling_edge(clock);
		reset <= '0';
		
		
		--	Check registers write value
		for i in 0 to 4 loop
			random_std_logic_vector(seed1, seed2, address);		
			random_std_logic_vector(seed1, seed2, data);
			random_integer(seed1, seed2, 0, READ_PORTS_COUNT, port_index);
			
			write_enable <= '1';
			write_address <= address;
			write_port <= data;
			wait until falling_edge(clock);
			write_enable <= '0';
			
			read_addresses((port_index + 1) * ADDRESS_BITS - 1 downto port_index * ADDRESS_BITS) <= address;
			wait until falling_edge(clock);
				assert data = read_ports((port_index + 1) * DATA_BITS - 1 downto port_index * DATA_BITS)
					report "Test fail: registers write value"
					severity FAILURE;
		end loop;
	
		-- Check reset
		set <= '1';
		wait until falling_edge(clock);
		set <= '0';
		
		reset <= '1';
		wait until falling_edge(clock);
		reset <= '0';
		
		for i in 0 to READ_PORTS_COUNT - 1 loop
			for j in 0 to SIZE - 1 loop
				address <= std_logic_vector(to_unsigned(j, address'length));
				read_addresses((i + 1) * ADDRESS_BITS - 1 downto i * ADDRESS_BITS) <= address;
				wait until falling_edge(clock);
					assert read_ports((i + 1) * DATA_BITS - 1 downto i * DATA_BITS) = (others => '0')
						report "Test fail: reset"
						severity FAILURE;
			end loop;
		end loop;
	
		-- Check set
		reset <= '1';
		wait until falling_edge(clock);
		reset <= '0';

		set <= '1';
		wait until falling_edge(clock);
		set <= '0';
		
		for i in 0 to READ_PORTS_COUNT - 1 loop
			for j in 0 to SIZE - 1 loop
				address <= std_logic_vector(to_unsigned(j, address'length));
				read_addresses((i + 1) * ADDRESS_BITS - 1 downto i * ADDRESS_BITS) <= address;
				wait until falling_edge(clock);
					assert read_ports((i + 1) * DATA_BITS - 1 downto i * DATA_BITS) = (others => '1')
						report "Test fail: reset"
						severity FAILURE;
			end loop;
		end loop;
	

		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
