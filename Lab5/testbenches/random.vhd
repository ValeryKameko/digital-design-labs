library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;

package random_pkg is
	procedure random_std_logic_vector(variable seed1, seed2: inout positive; signal vector: out std_logic_vector);
	procedure random_std_logic(variable seed1, seed2: inout positive; signal value: out std_logic);
	procedure random_integer(variable seed1, seed2: inout positive; min, max: integer; variable value: out integer);
end random_pkg;

package body random_pkg is
	procedure random_integer(variable seed1, seed2: inout positive; min, max: integer; variable value: out integer) is
		variable r: real;
	begin
		uniform(seed1, seed2, r);
		value := integer(trunc(real(max - min) * r + real(min)));
	end procedure random_integer;

	procedure random_std_logic(variable seed1, seed2: inout positive; signal value: out std_logic) is
		variable r: real;
	begin
		uniform(seed1, seed2, r);
		if r > 0.5 then
			value <= '1';
		else 
			value <= '0';
		end if;
		wait for 0 ns;
	end procedure random_std_logic; 

	procedure random_std_logic_vector(variable seed1, seed2: inout positive; signal vector: out std_logic_vector) is
		variable r: real;
	begin
		for i in vector'range loop
			uniform(seed1, seed2, r);
			if r > 0.5 then
				vector(i) <= '1';
			else 
				vector(i) <= '0';
			end if;
		end loop;
		wait for 0 ns;
	end procedure random_std_logic_vector; 
end random_pkg;
