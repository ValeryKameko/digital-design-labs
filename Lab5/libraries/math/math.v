function integer ceil_sqrt_(input integer value);
	localparam integer ITERATIONS = 20;
	integer x;
	integer i;
begin
	x = value;
	
	for (i = 0; i < ITERATIONS; i = i + 1)
		x = (x + value / x) / 2;
	
	while (x * x < value)
		x = x + 1;

	ceil_sqrt_ = x;
end
endfunction

function integer ceil_log2_(input integer value);
	integer x;
	integer i;
begin
	x = 0;
	while (2 ** x < value)
		x = x + 1;

	ceil_log2_ = x;
end
endfunction

function integer inverse_hamming_value_(input integer value);
	integer x;
begin
	x = 0;
	
	while ((1 << x) - x - 1 < value)
		x = x + 1;
	
	inverse_hamming_value_ = x;
end
endfunction
