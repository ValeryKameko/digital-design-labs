library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;

package math_pkg is
	function inverse_hamming_value(value: in integer) return integer;
end math_pkg;

package body math_pkg is
	function inverse_hamming_value(value: in integer) return integer is
		variable x: integer;
	begin
		x := 0;
		
		while (2 ** x) - x - 1 < value loop
			x := x + 1;
		end loop;
		
		return x;
	end function inverse_hamming_value;
end math_pkg;