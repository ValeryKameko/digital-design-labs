library ieee;
use ieee.std_logic_1164.all;

entity or2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end or2;

architecture behavioral of or2 is
begin
	output <= inputs(0) or inputs(1);
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity or3 is
	port(
		inputs: in std_logic_vector(2 downto 0);
		output: out std_logic
	);
end or3;

architecture behavioral of or3 is
begin
	output <= inputs(0) or inputs(1) or inputs(2);
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity orN is
	generic(
		N: natural := 2
	);
	port(
		inputs: in std_logic_vector(N - 1 downto 0);
		output: out std_logic
	);
end orN;

architecture behavioral of orN is
	signal zeroes: std_logic_vector(N - 1 downto 0) := (others => '0');
begin
	output <= '0' when inputs = zeroes else '1';
end behavioral;