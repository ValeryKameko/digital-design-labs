library ieee;
use ieee.std_logic_1164.all;

entity nor2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end nor2;

architecture behavioral of nor2 is
begin
	output <= inputs(0) nor inputs(1);
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity nor3 is
	port(
		inputs: in std_logic_vector(2 downto 0);
		output: out std_logic
	);
end nor3;

architecture behavioral of nor3 is
begin
	output <= not (inputs(0) or inputs(1) or inputs(2));
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity norN is
	generic(
		N: natural := 2
	);
	port(
		inputs: in std_logic_vector(N - 1 downto 0);
		output: out std_logic
	);
end norN;

architecture behavioral of norN is
	signal zeroes: std_logic_vector(N - 1 downto 0) := (others => '0');
begin
	output <= '1' when inputs = zeroes else '0';
end behavioral;