library ieee;
use ieee.std_logic_1164.all;

entity xor2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end xor2;

architecture behavioral of xor2 is
begin
	output <= inputs(0) xor inputs(1);
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity xor3 is
	port(
		inputs: in std_logic_vector(2 downto 0);
		output: out std_logic
	);
end xor3;

architecture behavioral of xor3 is
begin
	output <= inputs(0) xor inputs(1) xor inputs(2);
end behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity xorN is
	generic(
		N: natural := 2
	);
	port(
		inputs: in std_logic_vector(N - 1 downto 0);
		output: out std_logic
	);
end xorN;

architecture behavioral of xorN is
begin
	calc_xor: process
		variable xor_value: std_logic;
	begin
		xor_value := inputs(0);
		for i in (inputs'low + 1) to inputs'high loop
			xor_value := xor_value xor inputs(i);
		end loop;

		output <= xor_value;
	end process calc_xor;
end behavioral;