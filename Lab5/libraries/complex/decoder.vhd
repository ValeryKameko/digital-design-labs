library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity decoder is
	generic(
		N: natural := 4
	);
	port(
		inputs: in std_logic_vector(N - 1 downto 0);
		outputs: out std_logic_vector(2 ** N - 1 downto 0)
	);
end entity decoder;

architecture behavioral of decoder is
begin
	calc_outputs: process (inputs)
		variable index: integer;
	begin
		index := to_integer(unsigned(inputs));
		outputs <= (others => '0');
		outputs(index) <= '1';
	end process calc_outputs;
end architecture behavioral;