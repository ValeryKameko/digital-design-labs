`timescale 1ns / 1ps


module decoder_(
		input [N - 1:0] inputs,
		output [2 ** N - 1:0] outputs
    );
parameter N = 4;

assign outputs = 1 << inputs;

endmodule
