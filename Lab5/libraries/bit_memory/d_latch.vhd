library ieee;
use ieee.std_logic_1164.all;

entity d_latch is
	port(
		d, en: in std_logic;
		set, reset: in std_logic;
		q, nq: out std_logic
	);
end entity d_latch;

architecture behavioral of d_latch is
	signal storing_value: std_logic;
begin
	store_value: process (d, en, set, reset)
	begin
		if reset = '1' then
			storing_value <= '0';
		elsif set = '1' then
			storing_value <= '1';
		elsif en = '1' then
			storing_value <= d;
		else
			storing_value <= storing_value;
		end if;
	end process store_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;