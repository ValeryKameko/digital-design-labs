library ieee;
use ieee.std_logic_1164.all;

entity rs_latch is
	port(
		set, reset: in std_logic;
		en: in std_logic;
		q, nq: out std_logic
	);
end entity rs_latch;

architecture behavioral of rs_latch is
	signal storing_value: std_logic;
begin
	store_value: process (set, reset, en)
	begin
		if en = '1' then
			if reset = '1' then
				storing_value <= '0';
			elsif set = '1' then
				storing_value <= '1';
			else
				storing_value <= storing_value;
			end if;
		end if;
	end process store_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;