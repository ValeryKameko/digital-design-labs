`timescale 1ns / 1ps


module rs_flip_flop_(
		input set,
		input reset,

		input clk,
		input en,

		output q,
		output nq

    );

reg storing_value;

always @(posedge clk or posedge en)
begin : store_value
	if (en == 1)
		if (reset == 1)
			storing_value <= 0;
		else if (set == 1) 
			storing_value <= 1;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule
