`timescale 1ns / 1ps


module d_latch_(
		input d,
		input en,
		
		input reset,
		input set,
		
		output q,
		output nq
    );

reg storing_value;

always @(d or en or set or reset)
begin : store_value
	if (reset == 1)
		storing_value <= 0;
	else if (set == 1)
		storing_value <= 1;
	else if (en == 1)
		storing_value <= d;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule
