`timescale 1ns / 1ps


module jk_latch_(
		input j,
		input k,
		
		input en,
		
		input set,
		input reset,
		
		output q,
		output nq
    );

reg storing_value;

always @(set or reset or en or j or k)
begin : store_value
	if (reset == 1)
		storing_value <= 0;
	else if (set == 1)
		storing_value <= 1;
	else if (en == 1)
		case ({j, k})
			2'b11: storing_value <= ~storing_value;
			2'b10: storing_value <= 1;
			2'b01: storing_value <= 0;
			2'b00: storing_value <= storing_value;
		endcase
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule
