`timescale 1ns / 1ps


module t_flip_flop_(
		input t,
		input clk,
		
		input set,
		input reset,

		output q,
		output nq

    );

reg storing_value;

always @(posedge clk or posedge set or posedge reset)
begin : store_value
	if (reset == 1)
		storing_value <= 0;
	else if (set == 1)
		storing_value <= 1;
	else if (t == 1)
		storing_value <= ~storing_value;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule
