`timescale 1ns / 1ps

module universal2(
		input [1:0] ins,
		output out
    );

parameter [3:0] VALUES = 4'b0000;

assign out = VALUES[ins];

endmodule


module mux2(
		input [1:0] ins,
		input sel,

		output out
    );

assign out = ins[sel];

endmodule
