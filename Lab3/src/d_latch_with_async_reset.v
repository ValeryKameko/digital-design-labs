`timescale 1ns / 1ps


module d_latch_with_async_reset__structural(
		input d,
		input en,
		input reset,

		output q,
		output nq
    );

wire enable_saving, saving_value;
	
universal2 #(.VALUES(4'b0010)) enable_saving_value(
	.ins({reset, d}),
	.out(saving_value)
);

or calc_enable_saving(enable_saving, en, reset);

d_latch_with_enable__structural store(
	.d(saving_value),
	.en(enable_saving),
	.q(q),
	.nq(nq)
);

endmodule


module d_latch_with_async_reset__behavioral(
		input d,
		input en,
		input reset,
		
		output q,
		output nq
    );
reg storing_value;

always @(d or en or reset)
begin : store_value
	if (reset == 1) 
		storing_value <= 0;
	else if (en == 1)
		storing_value <= d;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule
