`timescale 1ns / 1ps


module d_latch__structural(
		input d,

		output q,
		output nq
    );

wire reset, set;
assign set = d;

not calc_reset(reset, set);

rs_latch__structural rs_latch(
	.set(set),
	.reset(reset),
	.q(q),
	.nq(nq)
);

endmodule


module d_latch__behavioral(
		input d,

		output reg q,
		output reg nq
    );

parameter time INERTIAL_DELAY = 0;
parameter time TRANSPORT_DELAY = 0;

wire set = d;
wire #INERTIAL_DELAY reset = ~set;

wire temp_q, temp_nq;

assign #INERTIAL_DELAY temp_nq = ~|{temp_q, set};
assign #INERTIAL_DELAY temp_q = ~|{temp_nq, reset};

always @(temp_q, temp_nq)
	{q, nq} <= #TRANSPORT_DELAY {temp_q, temp_nq};

endmodule
