`timescale 1ns / 1ps


module rs_flip_flop__structural(
		input set,
		input reset,
		input clk,

		output q,
		output nq
    );

wire not_clk;
wire primary_setting, primary_resetting;
wire primary_q, primary_nq;
wire secondary_setting, secondary_resetting;

not invert_clock(not_clk, clk);

// Primary
and enable_primary_setting(primary_setting, not_clk, set);

and enable_primary_resetting(primary_resetting, not_clk, reset);

rs_latch_structural store_primary(
	.set(primary_setting),
	.reset(primary_resetting),
	.q(primary_q),
	.nq(primary_nq)
);

// Secondary
and enable_secondary_setting(secondary_setting, clk, primary_q);

and enable_secondary_resetting(secondary_resetting, clk, primary_nq);

rs_latch_structural store_secondary(
	.set(secondary_setting),
	.reset(secondary_resetting),
	.q(q),
	.nq(nq)
);

endmodule


module rs_flip_flop__behavioral(
		input set,
		input reset,
		input clk,
		
		output q,
		output nq
    );

reg storing_value;

always @(posedge clk)
begin
	if (reset == 1)
		storing_value <= 0;
	else if (set == 1)
		storing_value <= 1;
end;

assign q = storing_value;
assign nq = ~storing_value;

endmodule
