`timescale 1ns / 1ps


module d_latch_with_enable__structural(
		input d,
		input en,

		output q,
		output nq
    );

wire need_reset, need_set;
wire reset, set;

assign need_set = d;

not calc_need_reset(need_reset, need_set);

and enable_set(set, en, need_set);
and enable_reset(reset, en, need_reset);

rs_latch__structural rs_latch(
	.set(set),
	.reset(reset),
	.q(q),
	.nq(nq)
);

endmodule


module d_latch_with_enable__behavioral(
		input d,
		input en,
		
		output reg q,
		output reg nq
    );

parameter time INERTIAL_DELAY = 0;
parameter time TRANSPORT_DELAY = 0;

reg temp_q;

always @(d or en)
begin : store_value
	if (en == 1)
		temp_q = d;
end : store_value;

wire #INERTIAL_DELAY temp_nq = ~temp_q;

always @*
	{q, nq} <= #TRANSPORT_DELAY {temp_q, temp_nq};

endmodule
