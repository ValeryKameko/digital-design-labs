library ieee;
use ieee.std_logic_1164.all;

-- Not synthesizable???
entity bistable_behavioral is
	generic(
		initial: std_logic := '0'
	);
	port(
		q, nq: out std_logic
	);
end entity bistable_behavioral;

architecture behavioral of bistable_behavioral is
	signal temp_q: std_logic := initial;
	signal temp_nq: std_logic := not initial;
begin
	process (temp_q, temp_nq)
		variable prev_temp_q, prev_temp_nq: std_logic;
	begin
		prev_temp_q := temp_q;
		prev_temp_nq := temp_nq;
		
		temp_q <= not prev_temp_nq;
		temp_nq <= not prev_temp_q;
	end process;
	
	q <= temp_q;
	nq <= temp_nq;
end;


library ieee;
use ieee.std_logic_1164.all;

entity bistable_structural is
	generic(
		initial: std_logic := '0'
	);
	port(
		q, nq: out std_logic
	);
end entity bistable_structural;

architecture structural of bistable_structural is
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	
	signal temp_q: std_logic := initial;
	signal temp_nq: std_logic := not initial;
begin
	invert_1: not1
		port map(input => temp_q, output => temp_nq);
	
	invert_2: not1
		port map(input => temp_nq, output => temp_q);
	
	q <= temp_q;
	nq <= temp_nq;
end architecture structural;