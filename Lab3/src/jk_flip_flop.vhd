library ieee;
use ieee.std_logic_1164.all;

entity jk_flip_flop_structural is
	port(
		j, k, clk: in std_logic;
		reset: in std_logic;
		q, nq: out std_logic
	);
end entity jk_flip_flop_structural;

architecture structural of jk_flip_flop_structural is
	component rs_flip_flop is
		port(
			set, reset: in std_logic;
			clk: in std_logic;
			q, nq: out std_logic
		);
	end component rs_flip_flop;
	
	component and2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component and2;
		
	component or2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component or2;
	
	component nor2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component nor2;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	
	for all: rs_flip_flop use entity work.rs_flip_flop_structural(structural);
	
	signal setting_value, resetting_value: std_logic;
	signal need_resetting_value: std_logic;
	signal storing_value, not_storing_value: std_logic;
begin
	calc_setting_value: and2
		port map (inputs => j & not_storing_value, output => setting_value);
	
	calc_need_resetting_value: and2
		port map (inputs => k & storing_value, output => need_resetting_value);
	
	calc_resetting_value: or2
		port map (inputs => need_resetting_value & reset, output => resetting_value);

	store_value: rs_flip_flop
		port map (set => setting_value, reset => resetting_value, clk => clk, q => storing_value, nq => not_storing_value);

	q <= storing_value;
	nq <= not_storing_value;
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity jk_flip_flop_behavioral is
	port(
		j, k, clk: in std_logic;
		reset: in std_logic;
		q, nq: out std_logic
	);
end entity jk_flip_flop_behavioral;

architecture behavioral of jk_flip_flop_behavioral is
	signal storing_value: std_logic;
begin
	save_value: process (clk, j, k)
	begin
		if reset = '1' then
			storing_value <= '0';
		elsif rising_edge(clk) then
			case to_stdlogicvector(j & k) is
				when b"11" => storing_value <= not storing_value;
				when b"10" => storing_value <= '1';
				when b"01" => storing_value <= '0';
				when others => storing_value <= storing_value;
			end case;
		end if;
	end process save_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;