library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop_with_enable_structural is
	port(
		d, clk, en: in std_logic;
		q, nq: out std_logic
	);
end entity d_flip_flop_with_enable_structural;

architecture structural of d_flip_flop_with_enable_structural is
	component d_flip_flop is
		port(
			d, clk: in std_logic;
			q, nq: out std_logic
		);
	end component d_flip_flop;

	component mux2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			sel: in std_logic;
			output: out std_logic
		);
	end component mux2;
	
	for all: d_flip_flop use entity work.d_flip_flop_structural(structural);

	signal saving_value, storing_value: std_logic;
begin
	calc_saving_value: mux2
		port map (inputs => d & storing_value, sel => en, output => saving_value);
	
	store_value: d_flip_flop
		port map (d => saving_value, clk => clk, q => storing_value, nq => nq);
	
	q <= storing_value;
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop_with_enable_behavioral is
	port(
		d, clk, en: in std_logic;
		q, nq: out std_logic
	);
end entity d_flip_flop_with_enable_behavioral;

architecture behavioral of d_flip_flop_with_enable_behavioral is
	signal storing_value: std_logic;
begin
	save_value: process (clk, d, en)
	begin
		if rising_edge(clk) and en = '1' then
			storing_value <= d;
		end if;
	end process save_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;
