library ieee;
use ieee.std_logic_1164.all;

entity t_flip_flop_structural is
	port(
		t, clk: in std_logic;
		reset: in std_logic := '0';
		q, nq: out std_logic
	);
end entity t_flip_flop_structural;

architecture structural of t_flip_flop_structural is
	component d_flip_flop_with_async_set_async_reset_enable is
		port(
			d, clk: in std_logic;
			en: in std_logic := '1';
			set: in std_logic := '0';
			reset: in std_logic := '0';
			q, nq: out std_logic
		);
	end component d_flip_flop_with_async_set_async_reset_enable;
	
	component xor2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component xor2;
	
	for all: d_flip_flop_with_async_set_async_reset_enable
		use entity work.d_flip_flop_with_async_set_async_reset_enable_structural(structural);

	signal storing_value, saving_value: std_logic;
begin
	calc_saving_value: xor2
		port map (inputs => storing_value & t, output => saving_value);
	
	store_value: d_flip_flop_with_async_set_async_reset_enable
		port map (d => saving_value, reset => reset, clk => clk, q => storing_value, nq => nq);

	q <= storing_value;
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity t_flip_flop_behavioral is
	port(
		t, clk: in std_logic;
		reset: in std_logic := '0';
		q, nq: out std_logic
	);
end entity t_flip_flop_behavioral;

architecture behavioral of t_flip_flop_behavioral is
	signal storing_value: std_logic;
begin
	save_value: process (clk, t, reset)
	begin
		if reset = '1' then
			storing_value <= '0';
		elsif rising_edge(clk) then
			storing_value <= storing_value xor t;
		end if;
	end process save_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;