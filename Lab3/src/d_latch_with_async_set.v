`timescale 1ns / 1ps


module d_latch_with_async_set__structural(
		input d,
		input en,
		input set,

		output q,
		output nq
    );

wire enable_saving, saving_value;

or calc_saving_value(saving_value, set, d);

or calc_enable_saving(enable_saving, en, set);

d_latch_with_enable__structural store_value(
	.d(saving_value),
	.en(enable_saving),
	.q(q),
	.nq(nq)
);

endmodule


module d_latch_with_async_set__behavioral(
		input d,
		input en,
		input set,
		
		output q,
		output nq
    );
reg storing_value;

always @(d or en or set)
begin : store_value
	if (set == 1)
		storing_value <= 1;
	else if (en == 1)
		storing_value <= d;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule
