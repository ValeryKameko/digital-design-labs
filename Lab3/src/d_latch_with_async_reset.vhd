library ieee;
use ieee.std_logic_1164.all;

entity d_latch_with_async_reset_structural is
	port(
		d, en, reset: in std_logic;
		q, nq: out std_logic
	);
end entity d_latch_with_async_reset_structural;

architecture structural of d_latch_with_async_reset_structural is
	component d_latch_with_enable is
		port(
			d, en: in std_logic;
			q, nq: out std_logic
		);
	end component d_latch_with_enable;
	
	component or2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component or2;
	
	component universal2 is
		generic(
			values: std_logic_vector(3 downto 0) := b"0000"
		);
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component universal2;
	
	for all: d_latch_with_enable use entity work.d_latch_with_enable_structural(structural);

	signal saving_value: std_logic;
	signal enable_saving: std_logic;
	
begin
	enable_saving_value: universal2
		generic map (values => b"0010")
		port map (inputs => reset & d, output => saving_value);

	calc_enable_saving: or2
		port map (inputs => en & reset, output => enable_saving);

	store: d_latch_with_enable
		port map (d => saving_value, en => enable_saving, q => q, nq => nq);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity d_latch_with_async_reset_behavioral is
	port(
		d, en, reset: in std_logic;
		q, nq: out std_logic
	);
end entity d_latch_with_async_reset_behavioral;

architecture behavioral of d_latch_with_async_reset_behavioral is
	signal storing_value: std_logic;
begin
	save_value: process (reset, en, d)
	begin
		if reset = '1' then
			storing_value <= '0';
		elsif en = '1' then
			storing_value <= d;
		end if;
	end process save_value;
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;