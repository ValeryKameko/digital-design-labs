`timescale 1ns / 1ps


module d_flip_flop_with_async_set_async_reset_enable__structural(
		input d,
		input clk,
		input en,
		
		input set,
		input reset,

		output q,
		output nq
    );

wire not_clk, enable;
wire set_resetting_value, reset_resetting_value;
wire resetting_value, not_resetting_value;
wire set_setting_value, reset_setting_value;
wire setting_value, not_setting_value;
wire preset_primary_value, prereset_primary_value;
wire set_primary_value, reset_primary_value;

not invert_clk(not_clk, clk);

and calc_enable(enable, not_clk, en);

// Resetting RS-latch
or calc_set_resetting_value(set_resetting_value, reset, not_setting_value);

or calc_reset_resetting_value(reset_resetting_value, set, enable);

rs_latch__structural store_resetting_value(
	.set(set_resetting_value),
	.reset(reset_resetting_value),
	.q(resetting_value),
	.nq(not_resetting_value)
);

// Setting RS-latch
or calc_set_setting_value(set_setting_value, d, set);

or calc_reset_setting_value(reset_setting_value, resetting_value, enable);

rs_latch__structural store_setting_value(
	.set(set_setting_value),
	.reset(reset_setting_value),
	.q(setting_value),
	.nq(not_setting_value)
);

// Primary RS-latch
and enable_preset_primary_value(preset_primary_value, en, setting_value);
or calc_set_primary_value(set_primary_value, set, preset_primary_value);

and enable_prereset_primary_value(prereset_primary_value, en, resetting_value);
or calc_reset_primary_value(reset_primary_value, reset, prereset_primary_value);

rs_latch__structural store_primary_value(
	.set(set_primary_value),
	.reset(reset_primary_value),
	.q(q),
	.nq(nq)
);

endmodule


module d_flip_flop_with_async_set_async_reset_enable__behavioral(
		input d,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output q,
		output nq
    );

reg storing_value;

always @(posedge clk or posedge set or negedge set or posedge reset or negedge reset)
begin : store_value
	if (reset == 1) 
		storing_value <= 0;
	else if (set == 1) 
		storing_value <= 1;
	else if (en == 1)
		storing_value <= d;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule

