`timescale 1ns / 1ps


// Not synthesizable by Xilinx ISE???
module bistable__structural(
		output q,
		output nq
    );

wire temp_q;
wire temp_nq;

not invert_1(temp_q, temp_nq);
not invert_2(temp_nq, temp_q);

assign q = temp_q;
assign nq = temp_nq;

endmodule


// Not synthesizable by Xilinx ISE???
module bistable__behavioral(
		output q,
		output nq
    );
parameter INITIAL = 0;

reg temp_q = INITIAL;
reg temp_nq = ~INITIAL;

always @(temp_q or temp_nq) 
	{temp_q, temp_nq} = {~temp_nq, ~temp_q};

assign q = temp_q;
assign nq = temp_nq;

endmodule
