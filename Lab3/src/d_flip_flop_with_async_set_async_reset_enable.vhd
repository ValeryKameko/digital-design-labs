library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop_with_async_set_async_reset_enable_structural is
	port(
		d, clk, en: in std_logic;
		set: in std_logic := '0';
		reset: in std_logic := '0';
		q, nq: out std_logic
	);
end entity d_flip_flop_with_async_set_async_reset_enable_structural;

architecture structural of d_flip_flop_with_async_set_async_reset_enable_structural is
	component rs_latch is
		port(
			set, reset: in std_logic;
			q, nq: out std_logic
		);
	end component rs_latch;
	
	component and2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component and2;
	
	component or2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component or2;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	
	for all: rs_latch use entity work.rs_latch_structural(structural);

	signal not_clk, enable: std_logic;
	signal set_resetting_value, reset_resetting_value: std_logic;
	signal resetting_value, not_resetting_value: std_logic;
	signal set_setting_value, reset_setting_value: std_logic;
	signal setting_value, not_setting_value: std_logic;
	signal preset_primary_value, prereset_primary_value: std_logic;
	signal set_primary_value, reset_primary_value: std_logic;
begin
	invert_clk: not1
		port map (input => clk, output => not_clk);
	
	calc_enable: and2
		port map (inputs => not_clk & en, output => enable);
	
	-- Resetting RS-latch
	calc_set_resetting_value: or2
		port map (inputs => reset & not_setting_value, output => set_resetting_value);

	calc_reset_resetting_value: or2
		port map (inputs => set & enable, output => reset_resetting_value);
	
	store_resetting_value: rs_latch
		port map (set => set_resetting_value, reset => reset_resetting_value, q => resetting_value, nq => not_resetting_value);

	-- Setting RS-latch
	calc_set_setting_value: or2
		port map (inputs => d & set, output => set_setting_value);

	calc_reset_setting_value: or2
		port map (inputs => resetting_value & enable, output => reset_setting_value);
	
	store_setting_value: rs_latch
		port map (set => set_setting_value, reset => reset_setting_value, q => setting_value, nq => not_setting_value);

	-- Primary RS-latch
	enable_preset_primary_value: and2
		port map (inputs => en & setting_value, output => preset_primary_value);
	calc_set_primary_value: or2
		port map (inputs => set & preset_primary_value, output => set_primary_value);
		
	enable_prereset_primary_value: and2
		port map (inputs => en & resetting_value, output => prereset_primary_value);
	calc_reset_primary_value: or2
		port map (inputs => reset & prereset_primary_value, output => reset_primary_value);

	store_primary_value: rs_latch
		port map (set => set_primary_value, reset => reset_primary_value, q => q, nq => nq);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop_with_async_set_async_reset_enable_behavioral is
	port(
		d, clk, en: in std_logic;
		set: in std_logic := '0';
		reset: in std_logic := '0';
		q, nq: out std_logic
	);
end entity d_flip_flop_with_async_set_async_reset_enable_behavioral;

architecture behavioral of d_flip_flop_with_async_set_async_reset_enable_behavioral is
	signal storing_value: std_logic;
begin
	save_value: process (clk, d, set, reset, en)
	begin
		if reset = '1' then
			storing_value <= '0';
		elsif set = '1' then
			storing_value <= '1';
		elsif rising_edge(clk) and en = '1' then
			storing_value <= d;
		end if;
	end process save_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;
