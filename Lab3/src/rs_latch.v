`timescale 1ns / 1ps


module rs_latch__structural(
		input set,
		input reset,
		
		output q,
		output nq
    );

wire temp_q, temp_nq;

nor perform_set(temp_nq, temp_q, set);
nor perform_reset(temp_q, temp_nq, reset);

assign q = temp_q;
assign nq = temp_nq;

endmodule


module rs_latch__behavioral(
		input set,
		input reset,
		
		output reg q,
		output reg nq
    );

parameter time INERTIAL_DELAY = 0;
parameter time TRANSPORT_DELAY = 0;

wire temp_q, temp_nq;

assign #INERTIAL_DELAY temp_nq = ~|{temp_q, set};
assign #INERTIAL_DELAY temp_q = ~|{temp_nq, reset};

always @(temp_q or temp_nq)
	{q, nq} <= #TRANSPORT_DELAY {temp_q, temp_nq};

endmodule
