`timescale 1ns / 1ps


module d_flip_flop_with_sync_set_async_reset_enable__behavioral(
		input d,
		input clk,
		input en,
		
		input set,
		input reset,
		
		output q,
		output nq
    );

reg storing_value;

always @(posedge clk or posedge reset)
begin : store_value
	if (reset == 1) 
		storing_value <= 0;
	else if (set == 1) 
		storing_value <= 1;
	else if (en == 1)
		storing_value <= d;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule

