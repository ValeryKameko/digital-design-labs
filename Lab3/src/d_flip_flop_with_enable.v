`timescale 1ns / 1ps


module d_flip_flop_with_enable__structural(
		input d,
		input clk,
		input en,

		output q,
		output nq
    );

wire storing_value, saving_value;

mux2 calc_saving_value(
	.ins({d, storing_value}),
	.sel(en),
	.out(saving_value)
);

d_flip_flop__structural store_value(
	.d(saving_value),
	.clk(clk),
	.q(storing_value),
	.nq(nq)
);

assign q = storing_value;

endmodule


module d_flip_flop_with_enable__behavioral(
		input d,
		input clk,
		input en,
		
		output q,
		output nq
    );

reg storing_value;

always @(posedge clk)
begin : store_value
	if (en == 1)
		storing_value <= d;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule
