library ieee;
use ieee.std_logic_1164.all;

entity not1 is
	port(
		input: in std_logic;
		output: out std_logic
	);
end entity not1;

architecture behavioral of not1 is
begin
	output <= not input;
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity or2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end entity or2;

architecture behavioral of or2 is
begin
	output <= inputs(0) or inputs(1);
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity xor2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end entity xor2;

architecture behavioral of xor2 is
begin
	output <= inputs(0) xor inputs(1);
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity nor2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end entity nor2;

architecture behavioral of nor2 is
begin
	output <= inputs(0) nor inputs(1);
end architecture behavioral;

library ieee;
use ieee.std_logic_1164.all;

entity and2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end entity and2;

architecture behavioral of and2 is
begin
	output <= inputs(0) and inputs(1);
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity nand2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end entity nand2;

architecture behavioral of nand2 is
begin
	output <= inputs(0) nand inputs(1);
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux2 is
	port(
		inputs: in std_logic_vector(1 downto 0);
		sel: in std_logic;
		output: out std_logic
	);
end entity mux2;

architecture behavioral of mux2 is
	signal index: integer := 0;
begin
	index <= 1 when (sel = '1') else 0;
	output <= inputs(index);
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity universal2 is
	generic(
		values: std_logic_vector(3 downto 0) := b"0000"
	);
	port(
		inputs: in std_logic_vector(1 downto 0);
		output: out std_logic
	);
end entity universal2;

architecture behavioral of universal2 is
begin
	output <= values(to_integer(unsigned(inputs)));
end architecture behavioral;


library ieee;
use ieee.std_logic_1164.all;

entity and3 is
	port(
		inputs: in std_logic_vector(2 downto 0);
		output: out std_logic
	);
end entity and3;

architecture behavioral of and3 is
begin
	output <= inputs(0) and inputs(1) and inputs(2);
end architecture behavioral;