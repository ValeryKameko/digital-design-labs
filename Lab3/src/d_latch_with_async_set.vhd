library ieee;
use ieee.std_logic_1164.all;

entity d_latch_with_async_set_structural is
	port(
		d, en, set: in std_logic;
		q, nq: out std_logic
	);
end entity d_latch_with_async_set_structural;

architecture structural of d_latch_with_async_set_structural is
	component d_latch_with_enable is
		port(
			d, en: in std_logic;
			q, nq: out std_logic
		);
	end component d_latch_with_enable;
	
	component or2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component or2;
	
	for all: d_latch_with_enable use entity work.d_latch_with_enable_structural(structural);

	signal saving_value: std_logic;
	signal enable_saving: std_logic;

begin
	calc_saving_value: or2
		port map (inputs => set & d, output => saving_value);

	calc_enable_saving: or2
		port map (inputs => en & set, output => enable_saving);

	store_value: d_latch_with_enable
		port map (d => saving_value, en => enable_saving, q => q, nq => nq);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity d_latch_with_async_set_behavioral is
	port(
		d, en, set: in std_logic;
		q, nq: out std_logic
	);
end entity d_latch_with_async_set_behavioral;

architecture behavioral of d_latch_with_async_set_behavioral is
	signal storing_value: std_logic;
begin
	store_value: process (set, en, d)
	begin
		if set = '1' then
			storing_value <= '1';
		elsif en = '1' then
			storing_value <= d;
		end if;
	end process store_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;