library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop_with_sync_set_async_reset_enable_behavioral is
	port(
		d, clk, en: in std_logic;
		set: in std_logic := '0';
		reset: in std_logic := '0';
		q, nq: out std_logic
	);
end entity d_flip_flop_with_sync_set_async_reset_enable_behavioral;

architecture behavioral of d_flip_flop_with_sync_set_async_reset_enable_behavioral is
	signal storing_value: std_logic;
begin
	save_value: process (clk, d, set, reset, en)
	begin
		if reset = '1' then
			storing_value <= '0';
		elsif rising_edge(clk) then
			if set = '1' then
				storing_value <= '1';
			elsif en = '1' then
				storing_value <= d;
			end if;
		end if;
	end process save_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;
