library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop_structural is
	port(
		d, clk: in std_logic;
		q, nq: out std_logic
	);
end entity d_flip_flop_structural;

architecture structural of d_flip_flop_structural is
	component d_latch_with_enable is
		port(
			d, en: in std_logic;
			q, nq: out std_logic
		);
	end component d_latch_with_enable;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	
	for all: d_latch_with_enable use entity work.d_latch_with_enable_structural(structural);

	signal primary_value: std_logic;
	signal primary_saving_value, secondary_saving_value: std_logic;
	signal enable_primary_saving, enable_secondary_saving: std_logic;
	
begin
	calc_enable_primary_saving: not1
		port map (input => clk, output => enable_primary_saving);
		
	primary_saving_value <= d;
	
	store_primary: d_latch_with_enable
		port map (d => primary_saving_value, en => enable_primary_saving, q => primary_value);
	
	enable_secondary_saving <= clk;
	
	secondary_saving_value <= primary_value;
	
	store_secondary: d_latch_with_enable
		port map (d => secondary_saving_value, en => enable_secondary_saving, q => q, nq => nq);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop_behavioral is
	port(
		d, clk: in std_logic;
		q, nq: out std_logic
	);
end entity d_flip_flop_behavioral;

architecture behavioral of d_flip_flop_behavioral is
	signal storing_value: std_logic;
begin
	save_value: process (clk, d)
	begin
		if rising_edge(clk) then
			storing_value <= d;
		end if;
	end process save_value;
	
	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;