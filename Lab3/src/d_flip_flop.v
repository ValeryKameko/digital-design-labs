`timescale 1ns / 1ps


module d_flip_flop__structural(
		input d,
		input clk,

		output q,
		output nq
    );

wire enable_primary_saving, enable_secondary_saving;
wire primary_saving_value, secondary_saving_value;

not calc_enable_primary_saving(enable_primary_saving, clk);
		
assign primary_saving_value = d;
	
d_latch_with_enable__structural store_primary(
	.d(primary_saving_value), 
	.en(enable_primary_saving),
	.q(primary_value)
);

assign enable_secondary_saving = clk;

assign secondary_saving_value = primary_value;

d_latch_with_enable__structural store_secondary(
	.d(secondary_saving_value), 
	.en(enable_secondary_saving),
	.q(q),
	.nq(nq)
);

endmodule


module d_flip_flop__behavioral(
		input d,
		input clk,
		
		output q,
		output nq
    );

reg storing_value;

always @(posedge clk)
begin : store_value
	storing_value <= d;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule
