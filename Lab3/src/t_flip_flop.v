`timescale 1ns / 1ps


module t_flip_flop__structural(
		input t,
		input clk,
		
		input reset,

		output q,
		output nq
    );

wire saving_value, storing_value;

xor calc_saving_value(saving_value, t, storing_value);

d_flip_flop_with_async_set_async_reset_enable__structural store_value(
	.d(saving_value),
	.en(1),
	.set(0),
	.reset(reset),
	.clk(clk),
	.q(storing_value),
	.nq(nq)
);

assign q = storing_value;

endmodule


module t_flip_flop__behavioral(
		input t,
		input clk,
		
		input reset,
		
		output q,
		output nq
    );

reg storing_value;

always @(posedge clk or posedge reset)
begin : store_value
	if (reset == 1)
		storing_value <= 0;
	else
		storing_value <= storing_value ^ t;
end : store_value

assign q = storing_value;
assign nq = ~storing_value;

endmodule
