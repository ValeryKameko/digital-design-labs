library ieee;
use ieee.std_logic_1164.all;

entity d_latch_with_enable_structural is
	port(
		d, en: in std_logic;
		q, nq: out std_logic
	);
end entity d_latch_with_enable_structural;

architecture structural of d_latch_with_enable_structural is
	component rs_latch is
		port(
			set, reset: in std_logic;
			q, nq: out std_logic
		);
	end component rs_latch;
	
	component and2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component and2;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	
	for store: rs_latch use entity work.rs_latch_structural(structural);

	signal need_set, need_reset: std_logic;
	signal set, reset: std_logic;
begin
	need_set <= d;
	
	calc_need_reset: not1
		port map (input => need_set, output => need_reset);
	
	enable_set: and2
		port map (inputs => en & need_set, output => set);
	
	enable_reset: and2
		port map (inputs => en & need_reset, output => reset);
	
	store: rs_latch
		port map (set => set, reset => reset, q => q, nq => nq);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

-- Weird synthesis???
entity d_latch_with_enable_behavioral is
	generic(
		inertial_delay: time := 0 ns;
		transport_delay: time := 0 ns
	);
	port(
		d, en: in std_logic;
		q, nq: out std_logic
	);
end entity d_latch_with_enable_behavioral;

architecture behavioral of d_latch_with_enable_behavioral is
	signal temp_q, temp_nq: std_logic;
begin
	store_bit: process (en, d)
	begin
		if en = '1' then
			temp_q <= transport d after transport_delay;
		end if;
	end process store_bit;

	temp_nq <= not temp_q after inertial_delay;
	
	q <= transport temp_q after transport_delay;
	nq <= transport temp_nq after transport_delay;
end architecture behavioral;