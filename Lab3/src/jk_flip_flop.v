`timescale 1ns / 1ps


module jk_flip_flop__structural(
		input j,
		input k,
		
		input reset,
		input clk,
		
		output q,
		output nq
    );

wire need_resetting_value;
wire setting_value, resetting_value;
wire storing_value, not_storing_value;

and calc_setting_value(setting_value, j, not_storing_value);
and calc_need_resetting_value(need_resetting_value, k, storing_value);

or calc_resetting_value(resetting_value, need_resetting_value, reset);

rs_flip_flop__structural store_value(
	.set(setting_value),
	.reset(resetting_value),
	.clk(clk),
	.q(storing_value),
	.nq(not_storing_value)
);

assign q = storing_value;
assign nq = not_storing_value;

endmodule


module jk_flip_flop__behavioral(
		input j,
		input k,
		
		input reset,
		input clk,
		
		output q,
		output nq
    );

reg storing_value;

always @(posedge clk or posedge reset)
begin
	if (reset == 1)
		storing_value <= 0;
	else
	begin
		case ({j, k})
			2'b11: storing_value <= ~storing_value;
			2'b10: storing_value <= 1;
			2'b01: storing_value <= 0;
			2'b00: storing_value <= storing_value;
		endcase
	end;
end;

assign q = storing_value;
assign nq = ~storing_value;

endmodule
