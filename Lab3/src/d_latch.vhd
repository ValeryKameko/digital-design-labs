library ieee;
use ieee.std_logic_1164.all;

entity d_latch_structural is
	port(
		d: in std_logic;
		q, nq: out std_logic
	);
end entity d_latch_structural;

architecture structural of d_latch_structural is
	component rs_latch is
		port(
			set, reset: in std_logic;
			q, nq: out std_logic
		);
	end component rs_latch;
	
	for store: rs_latch use entity work.rs_latch_structural(structural);
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;

	signal set, reset: std_logic;
begin
	set <= d;
	
	calc_reset: not1
		port map (input => set, output => reset);

	store: rs_latch
		port map (set => set, reset => reset, q => q, nq => nq);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity d_latch_behavioral is
	generic(
		inertial_delay: time := 0 ns;
		transport_delay: time := 0 ns
	);
	port(
		d: in std_logic;
		q, nq: out std_logic
	);
end entity d_latch_behavioral;

architecture behavioral of d_latch_behavioral is
	signal temp_q, temp_nq, set, reset: std_logic;
begin
	process (d)
	begin
		if d = '0' then
			temp_q <= transport '0' after transport_delay;
		elsif d = '1' then
			temp_q <= transport '1' after transport_delay;
		end if;
	end process;

	temp_nq <= not temp_q after inertial_delay;
	
	q <= transport temp_q after transport_delay;
	nq <= transport temp_nq after transport_delay;
end architecture behavioral;