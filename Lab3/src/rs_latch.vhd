library ieee;
use ieee.std_logic_1164.all;

entity rs_latch_structural is
	port(
		set, reset: in std_logic;
		q, nq: out std_logic
	);
end entity rs_latch_structural;

architecture structural of rs_latch_structural is
	component nor2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component nor2;

	signal temp_q, temp_nq: std_logic;
begin
	perform_set: nor2
		port map(inputs => temp_q & set, output => temp_nq);

	perform_reset: nor2
		port map(inputs => temp_nq & reset, output => temp_q);
	
	q <= temp_q;
	nq <= temp_nq;
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity rs_latch_behavioral is
	generic(
		inertial_delay: time := 0 ns;
		transport_delay: time := 0 ns
	);
	port(
		set, reset: in std_logic;
		q, nq: out std_logic
	);
end entity rs_latch_behavioral;

architecture behavioral of rs_latch_behavioral is
	signal temp_q, temp_nq: std_logic;
begin
	temp_nq <= temp_q nor set after inertial_delay;
	temp_q <= temp_nq nor reset after inertial_delay;
	
	q <= transport temp_q after transport_delay;
	nq <= transport temp_nq after transport_delay;
end architecture behavioral;