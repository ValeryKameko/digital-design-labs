library ieee;
use ieee.std_logic_1164.all;

entity rs_flip_flop_structural is
	port(
		set, reset: in std_logic;
		clk: in std_logic;
		q, nq: out std_logic
	);
end entity rs_flip_flop_structural;

architecture structural of rs_flip_flop_structural is
	component rs_latch is
		port(
			set, reset: in std_logic;
			q, nq: out std_logic
		);
	end component rs_latch;
	
	component and2 is
		port(
			inputs: in std_logic_vector(1 downto 0);
			output: out std_logic
		);
	end component and2;
	
	component not1 is
		port(
			input: in std_logic;
			output: out std_logic
		);
	end component not1;
	
	for all: rs_latch use entity work.rs_latch_structural(structural);
	
	signal primary_q, primary_nq: std_logic;
	signal primary_setting, primary_resetting: std_logic;
	signal secondary_setting, secondary_resetting: std_logic;
	signal not_clk: std_logic;
begin
	invert_clock: not1
		port map (input => clk, output => not_clk);
	
	-- Primary
	enable_primary_setting: and2
		port map (inputs => not_clk & set, output => primary_setting);
	
	enable_primary_resetting: and2
		port map (inputs => not_clk & reset, output => primary_resetting);
		
	store_primary: rs_latch
		port map (set => primary_setting, reset => primary_resetting, q => primary_q, nq => primary_nq);
	
	-- Secondary
	enable_secondary_setting: and2
		port map (inputs => clk & primary_q, output => secondary_setting);
	
	enable_secondary_resetting: and2
		port map (inputs => clk & primary_nq, output => secondary_resetting);
	
	store_secondary: rs_latch
		port map (set => secondary_setting, reset => secondary_resetting, q => q, nq => nq);
end architecture structural;


library ieee;
use ieee.std_logic_1164.all;

entity rs_flip_flop_behavioral is
	port(
		set, reset: in std_logic;
		clk: in std_logic;
		q, nq: out std_logic
	);
end entity rs_flip_flop_behavioral;

architecture behavioral of rs_flip_flop_behavioral is
	signal storing_value: std_logic;
begin
	store_value: process (clk, set, reset)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				storing_value <= '0';
			elsif set = '1' then
				storing_value <= '1';
			end if;
		end if;
	end process;

	q <= storing_value;
	nq <= not storing_value;
end architecture behavioral;
