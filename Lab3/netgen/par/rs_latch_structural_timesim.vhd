--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: rs_latch_structural_timesim.vhd
-- /___/   /\     Timestamp: Thu Oct 15 03:03:22 2020
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -s 3 -pcf rs_latch_structural.pcf -rpw 100 -tpw 0 -ar Structure -tm rs_latch_structural -insert_pp_buffers true -w -dir netgen/par -ofmt vhdl -sim rs_latch_structural.ncd rs_latch_structural_timesim.vhd 
-- Device	: 7a100tcsg324-3 (PRODUCTION 1.10 2013-10-13)
-- Input file	: rs_latch_structural.ncd
-- Output file	: D:\projects\hardware\digital-design-labs\Lab3\netgen\par\rs_latch_structural_timesim.vhd
-- # of Entities	: 1
-- Design Name	: rs_latch_structural
-- Xilinx	: D:\programs\XilinxISE\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity rs_latch_structural is
  port (
    set : in STD_LOGIC := 'X'; 
    reset : in STD_LOGIC := 'X'; 
    q : out STD_LOGIC; 
    nq : out STD_LOGIC 
  );
end rs_latch_structural;

architecture Structure of rs_latch_structural is
  signal q_OBUF_20 : STD_LOGIC; 
  signal set_IBUF_21 : STD_LOGIC; 
  signal reset_IBUF_22 : STD_LOGIC; 
  signal nq_OBUF_23 : STD_LOGIC; 
  signal ProtoComp2_INTERMDISABLE_GND_0 : STD_LOGIC; 
  signal reset_ProtoComp2_INTERMDISABLE_GND_0 : STD_LOGIC; 
  signal nq_OBUF_pack_1 : STD_LOGIC; 
  signal NlwBufferSignal_q_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_nq_OBUF_I : STD_LOGIC; 
begin
  ProtoComp2_INTERMDISABLE_GND : X_ZERO
    generic map(
      LOC => "IOB_X0Y147"
    )
    port map (
      O => ProtoComp2_INTERMDISABLE_GND_0
    );
  set_IBUF : X_IBUF_INTERMDISABLE_TPWRGT
    generic map(
      LOC => "IOB_X0Y147"
    )
    port map (
      IBUFDISABLE => '0',
      INTERMDISABLE => ProtoComp2_INTERMDISABLE_GND_0,
      O => set_IBUF_21,
      I => set,
      TPWRGT => '1'
    );
  ProtoComp2_INTERMDISABLE_GND_1 : X_ZERO
    generic map(
      LOC => "IOB_X0Y146"
    )
    port map (
      O => reset_ProtoComp2_INTERMDISABLE_GND_0
    );
  reset_IBUF : X_IBUF_INTERMDISABLE_TPWRGT
    generic map(
      LOC => "IOB_X0Y146"
    )
    port map (
      IBUFDISABLE => '0',
      INTERMDISABLE => reset_ProtoComp2_INTERMDISABLE_GND_0,
      O => reset_IBUF_22,
      I => reset,
      TPWRGT => '1'
    );
  q_OBUF : X_OBUF
    generic map(
      LOC => "IOB_X0Y149"
    )
    port map (
      I => NlwBufferSignal_q_OBUF_I,
      O => q
    );
  nq_OBUF : X_OBUF
    generic map(
      LOC => "IOB_X0Y148"
    )
    port map (
      I => NlwBufferSignal_nq_OBUF_I,
      O => nq
    );
  q_OBUF_q_OBUF_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 50 ps
    )
    port map (
      I => nq_OBUF_pack_1,
      O => nq_OBUF_23
    );
  perform_reset_output1 : X_LUT6
    generic map(
      LOC => "SLICE_X0Y147",
      INIT => X"0000333300003333"
    )
    port map (
      ADR0 => '1',
      ADR3 => '1',
      ADR2 => '1',
      ADR1 => reset_IBUF_22,
      ADR4 => nq_OBUF_23,
      ADR5 => '1',
      O => q_OBUF_20
    );
  perform_set_n00001 : X_LUT5
    generic map(
      LOC => "SLICE_X0Y147",
      INIT => X"0F0F0C0C"
    )
    port map (
      ADR0 => '1',
      ADR3 => '1',
      ADR2 => set_IBUF_21,
      ADR1 => reset_IBUF_22,
      ADR4 => nq_OBUF_23,
      O => nq_OBUF_pack_1
    );
  NlwBufferBlock_q_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 50 ps
    )
    port map (
      I => q_OBUF_20,
      O => NlwBufferSignal_q_OBUF_I
    );
  NlwBufferBlock_nq_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 50 ps
    )
    port map (
      I => nq_OBUF_23,
      O => NlwBufferSignal_nq_OBUF_I
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;

