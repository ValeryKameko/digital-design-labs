--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: jk_flip_flop_structural_timesim.vhd
-- /___/   /\     Timestamp: Thu Oct 15 02:59:11 2020
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -s 3 -pcf jk_flip_flop_structural.pcf -rpw 100 -tpw 0 -ar Structure -tm jk_flip_flop_structural -insert_pp_buffers true -w -dir netgen/par -ofmt vhdl -sim jk_flip_flop_structural.ncd jk_flip_flop_structural_timesim.vhd 
-- Device	: 7a100tcsg324-3 (PRODUCTION 1.10 2013-10-13)
-- Input file	: jk_flip_flop_structural.ncd
-- Output file	: D:\projects\hardware\digital-design-labs\Lab3\netgen\par\jk_flip_flop_structural_timesim.vhd
-- # of Entities	: 1
-- Design Name	: jk_flip_flop_structural
-- Xilinx	: D:\programs\XilinxISE\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity jk_flip_flop_structural is
  port (
    j : in STD_LOGIC := 'X'; 
    k : in STD_LOGIC := 'X'; 
    clk : in STD_LOGIC := 'X'; 
    reset : in STD_LOGIC := 'X'; 
    q : out STD_LOGIC; 
    nq : out STD_LOGIC 
  );
end jk_flip_flop_structural;

architecture Structure of jk_flip_flop_structural is
  signal nq_OBUF_35 : STD_LOGIC; 
  signal q_OBUF_36 : STD_LOGIC; 
  signal clk_IBUF_37 : STD_LOGIC; 
  signal store_value_store_primary_perform_reset_n0000 : STD_LOGIC; 
  signal j_IBUF_39 : STD_LOGIC; 
  signal k_IBUF_40 : STD_LOGIC; 
  signal reset_IBUF_41 : STD_LOGIC; 
  signal ProtoComp1_INTERMDISABLE_GND_0 : STD_LOGIC; 
  signal j_ProtoComp1_INTERMDISABLE_GND_0 : STD_LOGIC; 
  signal q_OBUF_pack_1 : STD_LOGIC; 
  signal clk_ProtoComp1_INTERMDISABLE_GND_0 : STD_LOGIC; 
  signal k_ProtoComp1_INTERMDISABLE_GND_0 : STD_LOGIC; 
  signal NlwBufferSignal_nq_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_q_OBUF_I : STD_LOGIC; 
begin
  ProtoComp1_INTERMDISABLE_GND_3 : X_ZERO
    generic map(
      LOC => "IOB_X1Y148"
    )
    port map (
      O => ProtoComp1_INTERMDISABLE_GND_0
    );
  reset_IBUF : X_IBUF_INTERMDISABLE_TPWRGT
    generic map(
      LOC => "IOB_X1Y148"
    )
    port map (
      IBUFDISABLE => '0',
      INTERMDISABLE => ProtoComp1_INTERMDISABLE_GND_0,
      O => reset_IBUF_41,
      I => reset,
      TPWRGT => '1'
    );
  ProtoComp1_INTERMDISABLE_GND : X_ZERO
    generic map(
      LOC => "IOB_X1Y147"
    )
    port map (
      O => j_ProtoComp1_INTERMDISABLE_GND_0
    );
  j_IBUF : X_IBUF_INTERMDISABLE_TPWRGT
    generic map(
      LOC => "IOB_X1Y147"
    )
    port map (
      IBUFDISABLE => '0',
      INTERMDISABLE => j_ProtoComp1_INTERMDISABLE_GND_0,
      O => j_IBUF_39,
      I => j,
      TPWRGT => '1'
    );
  nq_OBUF : X_OBUF
    generic map(
      LOC => "IOB_X1Y144"
    )
    port map (
      I => NlwBufferSignal_nq_OBUF_I,
      O => nq
    );
  store_value_store_primary_perform_reset_n00001 : X_LUT6
    generic map(
      LOC => "SLICE_X89Y146",
      INIT => X"FFFF5550FBFB5050"
    )
    port map (
      ADR4 => store_value_store_primary_perform_reset_n0000,
      ADR1 => j_IBUF_39,
      ADR5 => q_OBUF_36,
      ADR0 => clk_IBUF_37,
      ADR2 => reset_IBUF_41,
      ADR3 => k_IBUF_40,
      O => store_value_store_primary_perform_reset_n0000
    );
  nq_OBUF_nq_OBUF_DMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 50 ps
    )
    port map (
      I => q_OBUF_pack_1,
      O => q_OBUF_36
    );
  store_value_store_secondary_perform_set_output1 : X_LUT6
    generic map(
      LOC => "SLICE_X89Y145",
      INIT => X"0F0F05050F0F0505"
    )
    port map (
      ADR3 => '1',
      ADR1 => '1',
      ADR2 => q_OBUF_36,
      ADR0 => clk_IBUF_37,
      ADR4 => store_value_store_primary_perform_reset_n0000,
      ADR5 => '1',
      O => nq_OBUF_35
    );
  store_value_store_secondary_perform_reset_n00001 : X_LUT5
    generic map(
      LOC => "SLICE_X89Y145",
      INIT => X"5050FAFA"
    )
    port map (
      ADR3 => '1',
      ADR1 => '1',
      ADR2 => q_OBUF_36,
      ADR0 => clk_IBUF_37,
      ADR4 => store_value_store_primary_perform_reset_n0000,
      O => q_OBUF_pack_1
    );
  ProtoComp1_INTERMDISABLE_GND_2 : X_ZERO
    generic map(
      LOC => "IOB_X1Y145"
    )
    port map (
      O => clk_ProtoComp1_INTERMDISABLE_GND_0
    );
  clk_IBUF : X_IBUF_INTERMDISABLE_TPWRGT
    generic map(
      LOC => "IOB_X1Y145"
    )
    port map (
      IBUFDISABLE => '0',
      INTERMDISABLE => clk_ProtoComp1_INTERMDISABLE_GND_0,
      O => clk_IBUF_37,
      I => clk,
      TPWRGT => '1'
    );
  q_OBUF : X_OBUF
    generic map(
      LOC => "IOB_X1Y146"
    )
    port map (
      I => NlwBufferSignal_q_OBUF_I,
      O => q
    );
  ProtoComp1_INTERMDISABLE_GND_1 : X_ZERO
    generic map(
      LOC => "IOB_X1Y149"
    )
    port map (
      O => k_ProtoComp1_INTERMDISABLE_GND_0
    );
  k_IBUF : X_IBUF_INTERMDISABLE_TPWRGT
    generic map(
      LOC => "IOB_X1Y149"
    )
    port map (
      IBUFDISABLE => '0',
      INTERMDISABLE => k_ProtoComp1_INTERMDISABLE_GND_0,
      O => k_IBUF_40,
      I => k,
      TPWRGT => '1'
    );
  NlwBufferBlock_nq_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 50 ps
    )
    port map (
      I => nq_OBUF_35,
      O => NlwBufferSignal_nq_OBUF_I
    );
  NlwBufferBlock_q_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 50 ps
    )
    port map (
      I => q_OBUF_36,
      O => NlwBufferSignal_q_OBUF_I
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;

