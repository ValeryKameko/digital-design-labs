`timescale 1ns / 1ps

`define assert_(expression, message) \
	if ((expression) !== 1) \
	begin \
		$display((message)); $stop; \
	end;


module jk_flip_flop__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;

localparam time DELAY = 10;
reg clock = 0;
reg j, k, reset;
wire clk = clock;

jk_flip_flop__behavioral uut_behavioral(
	.j(j),
	.k(k),
	.reset(reset),
	.clk(clk),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

jk_flip_flop__structural uut_structural(
	.j(j),
	.k(k),
	.reset(reset),
	.clk(clk),
	.q(q_structural),
	.nq(nq_structural)
);

always
	clock = #(DELAY / 2) ~clock;

always
begin
	// Reset
	{j, k, reset} <= 3'b001;
	@(negedge clock);
	reset <= 0;
	

	// Check reset
	{j, k} <= 2'b10;
	@(negedge clock);
	{j, k} <= 2'b01;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check reset");
	
	// Check set
	{j, k} <= 2'b01;
	@(negedge clock);
	{j, k} <= 2'b10;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check set");
			
	// Check flip
	{j, k} <= 2'b01;
	@(negedge clock);
	{j, k} <= 2'b11;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check flip to 1");
	{j, k} <= 2'b11;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check flip to 0");
	

	$stop;
end;

endmodule
