library ieee;
use ieee.std_logic_1164.all;

entity rs_latch_x_state_testbench is
end entity rs_latch_x_state_testbench;

architecture post_route_testbench of rs_latch_x_state_testbench is
	component rs_latch is
		port(
			set, reset: in std_logic;
			q, nq: out std_logic
		);
	end component rs_latch;
	
	for uut_behavioral: rs_latch use entity work.rs_latch_structural(Structure);

	signal q_behavioral, nq_behavioral: std_logic;
	
	constant DELAY: time := 100 ns;
	signal clock: std_logic := '0';

	signal set, reset: std_logic;
begin
	
	uut_behavioral: rs_latch
		port map (set => set, reset => reset, q => q_behavioral, nq => nq_behavioral);
			
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		for i in 0 to 9 loop
			-- To X state
			set <= '1'; reset <= '1';
			wait until falling_edge(clock);
			
			-- To store state
			set <= '0'; reset <= '0';
			wait until falling_edge(clock);
		end loop;
		
		assert false 
			report "End post place & route simulation" 
			severity FAILURE;
	end process;
end architecture post_route_testbench;