library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop_testbench is
end entity d_flip_flop_testbench;

architecture testbench of d_flip_flop_testbench is
	component d_flip_flop is
		port(
			d, clk: in std_logic;
			q, nq: out std_logic
		);
	end component d_flip_flop;
	
	for uut_structural: d_flip_flop use entity work.d_flip_flop_structural(structural);
	for uut_behavioral: d_flip_flop use entity work.d_flip_flop_behavioral(behavioral);
	
	signal q_structural, nq_structural: std_logic;
	signal q_behavioral, nq_behavioral: std_logic;
	
	signal clock: std_logic := '0';
	signal d, clk: std_logic;
	
	constant DELAY: time := 10 ns;
begin
	uut_structural: d_flip_flop
		port map (d => d, clk => clk, q => q_structural, nq => nq_structural);
	
	uut_behavioral: d_flip_flop
		port map (d => d, clk => clk, q => q_behavioral, nq => nq_behavioral);
	
	clk <= clock;
	
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		-- Reset
		d <= '0';
		wait until falling_edge(clock);
	

		-- Check store 0
		d <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and
					 nq_structural = '1' and nq_behavioral = '1' report "Test fail: Check store 0" severity FAILURE;
		
		-- Check store 1
		d <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and
					 nq_structural = '0' and nq_behavioral = '0' report "Test fail: Check store 0" severity FAILURE;
		
		
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;