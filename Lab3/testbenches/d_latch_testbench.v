`timescale 1ns / 1ps
`include "assert.v"


module d_latch__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;
wire q_behavioral_parametric, nq_behavioral_parametric;

localparam time DELAY = 10;
reg clock = 0;
reg d;

d_latch__behavioral uut_behavioral(
	.d(d),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

d_latch__structural uut_structural(
	.d(d),
	.q(q_structural),
	.nq(nq_structural)
);

d_latch__behavioral #(.INERTIAL_DELAY(1), .TRANSPORT_DELAY(2)) uut_behavioral_parametric(
	.d(d),
	.q(q_behavioral_parametric),
	.nq(nq_behavioral_parametric)
);

always 
	clock = #(DELAY / 2) ~clock;

always
begin
	// Reset
	d <= 0;
	@(negedge clock);

	// Check store 0
	d <= 1;
	@(negedge clock);
	d <= 0;
	@(negedge clock);
		`assert_({q_structural, q_behavioral, q_behavioral_parametric} == 3'b000 && 
					{nq_structural, nq_behavioral, nq_behavioral_parametric} == 3'b111,
			"Test fail: Check store 0");
	
	// Check store 1
	d <= 0;
	@(negedge clock);
	d <= 1;
	@(negedge clock);
		`assert_({q_structural, q_behavioral, q_behavioral_parametric} == 3'b111 && 
					{nq_structural, nq_behavioral, nq_behavioral_parametric} == 3'b000,
			"Test fail: Check store 1");
	
	
	$stop;
end;

endmodule
