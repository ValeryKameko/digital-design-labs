`timescale 1ns / 1ps
`include "assert.v"


module d_latch_with_async_reset__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;

localparam time DELAY = 10;
reg clock = 0;
reg d, en, reset;

d_latch_with_async_reset__behavioral uut_behavioral(
	.d(d),
	.en(en),
	.reset(reset),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

d_latch_with_async_reset__structural uut_structural(
	.d(d),
	.en(en),
	.reset(reset),
	.q(q_structural),
	.nq(nq_structural)
);

always 
	clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	{d, en, reset} <= 3'b000;
	@(negedge clock);
	

	// Check store 0
	{d, en, reset} <= 3'b010;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check store 0");
	{d, en, reset} <= 3'b000;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check store 0");
	{d, en, reset} <= 3'b100;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check store 0");
	
	// Check store 1
	{d, en, reset} <= 3'b110;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check store 1");
	{d, en, reset} <= 3'b000;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check store 1");
	{d, en, reset} <= 3'b100;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check store 1");
	
	// Check reset for {d, en} = 2'b00
	{d, en, reset} <= 3'b110;
	@(negedge clock);
	{d, en, reset} <= 3'b001;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00,
			"Test fail: Check reset for {d, en} = 2'b00");
			
	// Check reset for {d, en} = 2'b01
	{d, en, reset} <= 3'b110;
	@(negedge clock);
	{d, en, reset} <= 3'b011;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00,
			"Test fail: Check reset for {d, en} = 2'b01");

	// Check reset for {d, en} = 2'b10
	{d, en, reset} <= 3'b110;
	@(negedge clock);
	{d, en, reset} <= 3'b101;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00,
			"Test fail: Check reset for {d, en} = 2'b10");
			
	// Check reset for {d, en} = 2'b11
	{d, en, reset} <= 3'b110;
	@(negedge clock);
	{d, en, reset} <= 3'b111;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00,
			"Test fail: Check reset for {d, en} = 2'b11");
	
	$stop;
end;

endmodule
