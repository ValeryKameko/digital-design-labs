library ieee;
use ieee.std_logic_1164.all;

entity rs_latch_testbench is
end entity rs_latch_testbench;

architecture testbench of rs_latch_testbench is
	component rs_latch is
		port(
			set, reset: in std_logic;
			q, nq: out std_logic
		);
	end component rs_latch;
	
	component rs_latch_parametric is
		generic(
			inertial_delay: time := 0 ns;
			transport_delay: time := 0 ns
		);
		port(
			set, reset: in std_logic;
			q, nq: out std_logic
		);
	end component rs_latch_parametric;
	
	for uut_structural: rs_latch use entity work.rs_latch_structural(structural);
	for uut_behavioral: rs_latch use entity work.rs_latch_behavioral(behavioral);
	for uut_behavioral_parametric: rs_latch_parametric use entity work.rs_latch_behavioral(behavioral);

	signal q_structural, nq_structural: std_logic;
	signal q_behavioral, nq_behavioral: std_logic;
	signal q_behavioral_parametric, nq_behavioral_parametric: std_logic;
	
	constant DELAY: time := 10 ns;	
	signal clock: std_logic := '0';

	signal set, reset: std_logic;
begin
	uut_structural: rs_latch
		port map (set => set, reset => reset, q => q_structural, nq => nq_structural);
	
	uut_behavioral: rs_latch
		port map (set => set, reset => reset, q => q_behavioral, nq => nq_behavioral);
		
	uut_behavioral_parametric: rs_latch_parametric
		generic map (inertial_delay => 1 ns, transport_delay => 2 ns)
		port map (set => set, reset => reset, q => q_behavioral_parametric, nq => nq_behavioral_parametric);
		
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		-- Reset
		set <= '0'; reset <= '1';
		wait until falling_edge(clock);
		

		-- Check reset
		set <= '1'; reset <= '0';
		wait until falling_edge(clock);
		set <= '0'; reset <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and  q_behavioral_parametric = '0' and
					 nq_structural = '1' and nq_behavioral = '1' and nq_behavioral_parametric = '1' 
					 report "Test fail: Check reset" severity FAILURE;
		
		-- Check set
		set <= '0'; reset <= '1';
		wait until falling_edge(clock);
		set <= '1'; reset <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and  q_behavioral_parametric = '1' and
					 nq_structural = '0' and nq_behavioral = '0' and nq_behavioral_parametric = '0' 
					 report "Test fail: Check set" severity FAILURE;

		
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;