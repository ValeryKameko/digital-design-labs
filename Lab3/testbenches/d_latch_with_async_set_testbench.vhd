library ieee;
use ieee.std_logic_1164.all;

entity d_latch_with_async_set_testbench is
end entity d_latch_with_async_set_testbench;

architecture testbench of d_latch_with_async_set_testbench is
	component d_latch_with_async_set is
		port(
			d, en, set: in std_logic;
			q, nq: out std_logic
		);
	end component d_latch_with_async_set;
	
	for uut_structural: d_latch_with_async_set use entity work.d_latch_with_async_set_structural(structural);
	for uut_behavioral: d_latch_with_async_set use entity work.d_latch_with_async_set_behavioral(behavioral);
	
	signal q_structural, nq_structural: std_logic;
	signal q_behavioral, nq_behavioral: std_logic;
	
	signal d, en, set: std_logic;
	
	constant DELAY: time := 10 ns;
	signal clock: std_logic := '0';
begin
	uut_structural: d_latch_with_async_set
		port map (d => d, en => en, set => set, q => q_structural, nq => nq_structural);
	
	uut_behavioral: d_latch_with_async_set
		port map (d => d, en => en, set => set, q => q_behavioral, nq => nq_behavioral);
	
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		-- Reset
		d <= '0'; en <= '1'; set <= '1';
		wait until falling_edge(clock);
		
		
		-- Check store 0
		d <= '0'; en <= '1'; set <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and
					 nq_structural = '1' and nq_behavioral = '1' report "Test fail: Check store 0" severity FAILURE;
		d <= '0'; en <= '0'; set <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and
					 nq_structural = '1' and nq_behavioral = '1' report "Test fail: Check store 0" severity FAILURE;
		d <= '1'; en <= '0'; set <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and
					 nq_structural = '1' and nq_behavioral = '1' report "Test fail: Check store 0" severity FAILURE;
	
		-- Check store 1
		d <= '1'; en <= '1'; set <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and
					 nq_structural = '0' and nq_behavioral = '0' report "Test fail: Check store 0" severity FAILURE;
		d <= '0'; en <= '0'; set <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and
					 nq_structural = '0' and nq_behavioral = '0' report "Test fail: Check store 0" severity FAILURE;
		d <= '1'; en <= '0'; set <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and
					 nq_structural = '0' and nq_behavioral = '0' report "Test fail: Check store 0" severity FAILURE;
		
		-- Check set for d&en = '00'
		d <= '1'; en <= '1'; set <= '0';
		wait until falling_edge(clock);
		d <= '0'; en <= '0'; set <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' report "Test fail: Check set for d&en = '00'" severity FAILURE;
		
		-- Check set for d&en = '01'
		d <= '1'; en <= '1'; set <= '0';
		wait until falling_edge(clock);
		d <= '0'; en <= '1'; set <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' report "Test fail: Check set for d&en = '01'" severity FAILURE;
		
		-- Check set for d&en = '10'
		d <= '1'; en <= '1'; set <= '0';
		wait until falling_edge(clock);
		d <= '1'; en <= '0'; set <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' report "Test fail: Check set for d&en = '10'" severity FAILURE;
		
		-- Check set for d&en = '11'
		d <= '1'; en <= '1'; set <= '0';
		wait until falling_edge(clock);
		d <= '1'; en <= '1'; set <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' report "Test fail: Check set for d&en = '11'" severity FAILURE;
		
		
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;