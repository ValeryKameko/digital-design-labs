`timescale 1ns / 1ps

`define assert_(expression, message) \
	if ((expression) !== 1) \
	begin \
		$display((message)); $stop; \
	end;


module d_flip_flop_with_async_set_async_reset_enable__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;

localparam time DELAY = 10;
reg clock = 0;
reg d, en;
reg set, reset;

wire clk = clock;

d_flip_flop_with_async_set_async_reset_enable__behavioral uut_behavioral(
	.d(d),
	.clk(clk),
	.en(en),
	.set(set),
	.reset(reset),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

d_flip_flop_with_async_set_async_reset_enable__structural uut_structural(
	.d(d),
	.clk(clk),
	.en(en),
	.set(set),
	.reset(reset),	
	.q(q_structural),
	.nq(nq_structural)
);

always
	 clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	{en, d, set, reset} <= 'b1000;
	@(negedge clock);
	

	// Check store 0
	{en, d, set, reset} <= 'b1000;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check store 0");
	
	// Check store 1
	{en, d, set, reset} <= 'b1100;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check store 1");
	
	// Check not store 0
	{en, d, set, reset} <= 'b1100;
	@(negedge clock);
	{en, d, set, reset} <= 'b0000;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check not store 0");
	
	// Check not store 1
	{en, d, set, reset} <= 'b1000;
	@(negedge clock);
	{en, d, set, reset} <= 'b0100;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check not store 1");
	
	// Check async set
	{en, d, set, reset} <= 'b1000;
	@(negedge clock);
	{en, d, set, reset} <= 'b0010;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check async set");
			
	// Check async reset
	{en, d, set, reset} <= 'b1100;
	@(negedge clock);
	{en, d, set, reset} <= 'b0001;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check async reset");		

	$stop;
end;

endmodule
