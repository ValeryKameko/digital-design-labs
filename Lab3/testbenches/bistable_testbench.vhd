library ieee;
use ieee.std_logic_1164.all;

entity bistable_testbench is
end entity bistable_testbench;

architecture testbench of bistable_testbench is
	component bistable is
		generic(
			initial: std_logic
		);
		port(
			q, nq: out std_logic
		);
	end component bistable;
	
	for uut_structural: bistable use entity work.bistable_structural(structural);
	for uut_behavioral: bistable use entity work.bistable_behavioral(behavioral);

	signal q_structural, nq_structural: std_logic;
	signal q_behavioral, nq_behavioral: std_logic;

	constant DELAY: time := 10 ns;
	signal clock: std_logic := '0';
	
	constant initial: std_logic := '0';
begin
	uut_structural: bistable
		generic map (initial => initial)
		port map (q => q_structural, nq => nq_structural);
	
	uut_behavioral: bistable
		generic map (initial => initial)
		port map (q => q_behavioral, nq => nq_behavioral);
	
	clock_process: process
	begin
		wait for (DELAY / 2);
		clock <= not clock;
	end process clock_process;
	
	test: process
	begin
		for i in 0 to 9 loop
			wait until falling_edge(clock);
		end loop;
		
		assert false
			report "End simulation"
			severity FAILURE;
	end process test;
end architecture testbench;