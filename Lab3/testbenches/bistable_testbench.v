`timescale 1ns / 1ps


module bistable__testbench;

wire nq_structural, q_structural;
wire nq_behavioral, q_behavioral;

localparam time DELAY = 10;

reg clock = 0;

localparam INITIAL = 0;

bistable__behavioral uut_behavioral(.q(q_behavioral), .nq(nq_behavioral));

bistable__structural #(.INITIAL(INITIAL)) uut_structural(.q(q_structural), .nq(nq_structural));

always
	 clock = #(DELAY / 2) ~clock;

integer i;

initial
begin
	for (i = 0; i < 10; i = i + 1)
		@(negedge clock);

	$stop;
end;

endmodule
