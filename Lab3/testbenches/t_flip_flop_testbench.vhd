library ieee;
use ieee.std_logic_1164.all;

entity t_flip_flop_testbench is
end entity t_flip_flop_testbench;

architecture testbench of t_flip_flop_testbench is
	component t_flip_flop is
		port(
			t, clk: in std_logic;
			reset: in std_logic;
			q, nq: out std_logic
		);
	end component t_flip_flop;
	
	for uut_structural: t_flip_flop use entity work.t_flip_flop_structural(structural);
	for uut_behavioral: t_flip_flop use entity work.t_flip_flop_behavioral(behavioral);
	
	signal q_structural, nq_structural: std_logic;
	signal q_behavioral, nq_behavioral: std_logic;
	
	signal clock: std_logic := '0';
	signal t, clk, reset: std_logic;
	
	constant DELAY: time := 10 ns;
begin
	uut_structural: t_flip_flop
		port map (t => t, clk => clk, reset => reset, q => q_structural, nq => nq_structural);
	
	uut_behavioral: t_flip_flop
		port map (t => t, clk => clk, reset => reset, q => q_behavioral, nq => nq_behavioral);
	
	clk <= clock;
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		-- Reset
		t <= '0'; reset <= '1';
		wait until falling_edge(clock);
		
		-- Check flip t = 1
		t <= '0'; reset <= '1';
		wait until falling_edge(clock);
		t <= '1'; reset <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and
					 nq_structural = '0' and nq_behavioral = '0' report "Test fail: Check flip t = 1" severity FAILURE;
		
		-- Check flip t = 0
		t <= '0'; reset <= '1';
		wait until falling_edge(clock);
		t <= '0'; reset <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and
					 nq_structural = '1' and nq_behavioral = '1' report "Test fail: Check flip t = 0" severity FAILURE;
		
		-- Check reset
		t <= '0'; reset <= '1';
		wait until falling_edge(clock);
		
		t <= '1'; reset <= '0';
		wait until falling_edge(clock);
		
		t <= '0'; reset <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and
					 nq_structural = '1' and nq_behavioral = '1' report "Test fail: Check reset" severity FAILURE;
		
		
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
