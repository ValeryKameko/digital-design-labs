`timescale 1ns / 1ps
`include "assert.v"


module rs_latch__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;
wire q_behavioral_parametric, nq_behavioral_parametric;

localparam time DELAY = 10;
reg clock = 0;
reg set, reset;

rs_latch__behavioral uut_behavioral(
	.set(set),
	.reset(reset),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

rs_latch__structural uut_structural(
	.set(set),
	.reset(reset),
	.q(q_structural),
	.nq(nq_structural)
);

rs_latch__behavioral #(.INERTIAL_DELAY(1), .TRANSPORT_DELAY(2)) uut_behavioral_parametric(
	.set(set),
	.reset(reset),
	.q(q_behavioral_parametric),
	.nq(nq_behavioral_parametric)
);

always
	clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	{set, reset} <= 2'b01;
	@(negedge clock);
	
	
	// Check reset
	{set, reset} <= 2'b10;
	@(negedge clock);
	{set, reset} <= 2'b01;
	@(negedge clock);
		`assert_({q_structural, q_behavioral, q_behavioral_parametric} == 3'b000 && 
					{nq_structural, nq_behavioral, nq_behavioral_parametric} == 3'b111,
			"Test fail: Check reset");
	
	// Check set
	{set, reset} <= 2'b01;
	@(negedge clock);
	{set, reset} <= 2'b10;
	@(negedge clock);
		`assert_({q_structural, q_behavioral, q_behavioral_parametric} == 3'b111 && 
					{nq_structural, nq_behavioral, nq_behavioral_parametric} == 3'b000,
			"Test fail: Check set");
	

	$stop;
end

endmodule
