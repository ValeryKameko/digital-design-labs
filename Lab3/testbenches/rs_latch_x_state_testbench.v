`timescale 1ns / 1ps


module rs_latch_x_state__testbench;

wire q_behavioral, nq_behavioral;

localparam time DELAY = 100;
reg clock = 0;
reg set, reset;

rs_latch__behavioral uut_behavioral(
	.set(set),
	.reset(reset),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

always 
	clock = #(DELAY / 2) ~clock;

integer i;

initial
begin
	for (i = 0; i < 10; i = i + 1)
	begin
		// To X state
		{set, reset} <= 2'b11; 
		@(negedge clock);
		
		// To store state
		{set, reset} <= 2'b00;
		@(negedge clock);
	end;
	
	$stop;
end;

endmodule
