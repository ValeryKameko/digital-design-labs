library ieee;
use ieee.std_logic_1164.all;

entity d_flip_flop_with_sync_set_async_reset_enable_testbench is
end entity d_flip_flop_with_sync_set_async_reset_enable_testbench ;

architecture testbench of d_flip_flop_with_sync_set_async_reset_enable_testbench  is
	component d_flip_flop_with_sync_set_async_reset_enable is
		port(
			d, clk, en: in std_logic;
			set, reset: in std_logic;
			q, nq: out std_logic
		);
	end component d_flip_flop_with_sync_set_async_reset_enable;
	
	for uut_behavioral: d_flip_flop_with_sync_set_async_reset_enable
		use entity work.d_flip_flop_with_sync_set_async_reset_enable_behavioral(behavioral);
	
	signal q_behavioral, nq_behavioral: std_logic;
	
	signal clock: std_logic := '0';
	signal d, clk, en: std_logic;
	signal set, reset: std_logic;
	
	constant DELAY: time := 10 ns;
begin
	uut_behavioral: d_flip_flop_with_sync_set_async_reset_enable 
		port map (d => d, clk => clk, en => en, set => set, reset => reset, q => q_behavioral, nq => nq_behavioral);
	
	clk <= clock;
	
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin	
		-- Reset
		en <= '1'; d <= '0'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
		

		-- Check store 0
		en <= '1'; d <= '0'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
			assert  q_behavioral = '0' and
					 nq_behavioral = '1' report "Test fail: Check store 0" severity FAILURE;
		
		-- Check store 1
		en <= '1'; d <= '1'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
			assert  q_behavioral = '1' and
					 nq_behavioral = '0' report "Test fail: Check store 1" severity FAILURE;
		
		-- Check not store 0
		en <= '1'; d <= '1'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
		en <= '0'; d <= '0'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
			assert  q_behavioral = '1' and
					 nq_behavioral = '0' report "Test fail: Check not store 0" severity FAILURE;
		
		-- Check not store 1
		en <= '1'; d <= '0'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
		en <= '0'; d <= '1'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
			assert  q_behavioral = '0' and
					 nq_behavioral = '1' report "Test fail: Check not store 1" severity FAILURE;
		
		-- Check sync set
		en <= '1'; d <= '0'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
		en <= '0'; d <= '0'; set <= '1'; reset <= '0';
		wait until rising_edge(clock);
			assert  q_behavioral = '0' and
					 nq_behavioral = '1' report "Test fail: Check sync set" severity FAILURE;
		wait until falling_edge(clock);
			assert  q_behavioral = '1' and
					 nq_behavioral = '0' report "Test fail: Check sync set" severity FAILURE;

		-- Check async reset
		en <= '1'; d <= '1'; set <= '0'; reset <= '0';
		wait until falling_edge(clock);
		en <= '0'; d <= '0'; set <= '0'; reset <= '1';
		wait until rising_edge(clock);
			assert  q_behavioral = '0' and
					 nq_behavioral = '1' report "Test fail: Check async reset" severity FAILURE;
		wait until falling_edge(clock);
			assert  q_behavioral = '0' and
					 nq_behavioral = '1' report "Test fail: Check async reset" severity FAILURE;
		
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
