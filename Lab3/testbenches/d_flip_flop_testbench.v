`timescale 1ns / 1ps
`include "assert.v"


module d_flip_flop__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;

localparam time DELAY = 10;
reg clock = 0;
reg d;

wire clk = clock;

d_flip_flop__behavioral uut_behavioral(
	.d(d),
	.clk(clk),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

d_flip_flop__structural uut_structural(
	.d(d),
	.clk(clk),
	.q(q_structural),
	.nq(nq_structural)
);

always
	 clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	d <= 0;
	@(negedge clock);
	

	// Check store 0
	d <= 0;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check store 0");
	
	// Check store 1
	d <= 1;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check store 1");
	
	$stop;
end;

endmodule

