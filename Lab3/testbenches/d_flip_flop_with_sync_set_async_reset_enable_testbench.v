`timescale 1ns / 1ps
`include "assert.v"


module d_flip_flop_with_sync_set_async_reset_enable__testbench;

wire q_behavioral, nq_behavioral;

localparam time DELAY = 10;
reg clock = 0;
reg d, en;
reg set, reset;
wire clk = clock;

d_flip_flop_with_sync_set_async_reset_enable__behavioral uut_behavioral(
	.d(d),
	.clk(clk),
	.en(en),
	.set(set),
	.reset(reset),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

always
	clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	{en, d, set, reset} <= 4'b1000; 
	@(negedge clock);
	

	// Check store 0
	{en, d, set, reset} <= 4'b1000;
	@(negedge clock);
		`assert_({q_behavioral, nq_behavioral} == 2'b01,
			"Test fail: Check store 0");
	
	// Check store 1
	{en, d, set, reset} <= 4'b1100;
	@(negedge clock);
		`assert_({q_behavioral, nq_behavioral} == 2'b10,
			"Test fail: Check store 1");
	
	// Check not store 0
	{en, d, set, reset} <= 4'b1100;
	@(negedge clock);
	{en, d, set, reset} <= 4'b0000;
	@(negedge clock);
		`assert_({q_behavioral, nq_behavioral} == 2'b10,
			"Test fail: Check not store 0");
	
	// Check not store 1
	{en, d, set, reset} <= 4'b1000;
	@(negedge clock);
	{en, d, set, reset} <= 4'b0100;
	@(negedge clock);
		`assert_({q_behavioral, nq_behavioral} == 2'b01,
			"Test fail: Check not store 1");
	
	// Check sync set
	{en, d, set, reset} <= 4'b1000;
	@(negedge clock);
	{en, d, set, reset} <= 4'b0010;
	@(posedge clock);
		`assert_({q_behavioral, nq_behavioral} == 2'b01,
			"Test fail: Check sync set");
	{en, d, set, reset} <= 4'b0010;
	@(negedge clock);
		`assert_({q_behavioral, nq_behavioral} == 2'b10,
			"Test fail: Check sync set");
			
	// Check async reset
	{en, d, set, reset} <= 4'b1100; #DELAY;
	@(negedge clock);
	{en, d, set, reset} <= 4'b0001; #DELAY;
	@(posedge clock);
		`assert_({q_behavioral, nq_behavioral} == 2'b01,
			"Test fail: Check async reset");
	{en, d, set, reset} <= 4'b0001; #DELAY;
	@(negedge clock);
		`assert_({q_behavioral, nq_behavioral} == 2'b01,
			"Test fail: Check async reset");

	$stop;
end;

endmodule
