`timescale 1ns / 1ps

`define assert_(expression, message) \
	if ((expression) !== 1) \
	begin \
		$display((message)); $stop; \
	end;


module t_flip_flop__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;

localparam time DELAY = 10;
reg clock = 0;
reg t;
reg reset;
wire clk = clock;

t_flip_flop__behavioral uut_behavioral(
	.t(t),
	.reset(reset),
	.clk(clk),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

t_flip_flop__structural uut_structural(
	.t(t),
	.reset(reset),
	.clk(clk),
	.q(q_structural),
	.nq(nq_structural)
);

always
	clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	{t, reset} <= 2'b01;
	@(negedge clock);


	// Check flip t = 1
	{t, reset} <= 2'b01;
	@(negedge clock);
	{t, reset} <= 2'b10;
	@(negedge clock);
	`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check flip t = 1");
	
	// Check not flip t = 0
	{t, reset} <= 2'b01;
	@(negedge clock);
	{t, reset} <= 2'b00;
	@(negedge clock);
	`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check not flip t = 0");
	
	// Check reset
	{t, reset} <= 2'b01;
	@(negedge clock);
	
	{t, reset} <= 2'b10;
	@(negedge clock);
	
	{t, reset} <= 2'b01;
	@(negedge clock);
	`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check reset");
	
	$stop;
end;

endmodule
