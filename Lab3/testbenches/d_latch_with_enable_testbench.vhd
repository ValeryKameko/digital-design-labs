library ieee;
use ieee.std_logic_1164.all;

entity d_latch_with_enable_testbench is
end entity d_latch_with_enable_testbench;

architecture testbench of d_latch_with_enable_testbench is
	component d_latch_with_enable is
		port(
			d, en: in std_logic;
			q, nq: out std_logic
		);
	end component d_latch_with_enable;
	
	component d_latch_with_enable_parametric is
		generic(
			inertial_delay: time := 0 ns;
			transport_delay: time := 0 ns
		);
		port(
			d, en: in std_logic;
			q, nq: out std_logic
		);
	end component d_latch_with_enable_parametric;
	
	for uut_structural: d_latch_with_enable use entity work.d_latch_with_enable_structural(structural);
	for uut_behavioral: d_latch_with_enable use entity work.d_latch_with_enable_behavioral(behavioral);
	for uut_behavioral_parametric: d_latch_with_enable_parametric use entity work.d_latch_with_enable_behavioral(behavioral);
	
	
	signal q_structural, nq_structural: std_logic;
	signal q_behavioral, nq_behavioral: std_logic;
	signal q_behavioral_parametric, nq_behavioral_parametric: std_logic;
	
	signal d, en: std_logic;
	
	constant DELAY: time := 10 ns;
	signal clock: std_logic := '0';
begin
	uut_structural: d_latch_with_enable
		port map (d => d, en => en, q => q_structural, nq => nq_structural);
	
	uut_behavioral: d_latch_with_enable
		port map (d => d, en => en, q => q_behavioral, nq => nq_behavioral);
		
	uut_behavioral_parametric: d_latch_with_enable_parametric
		generic map (inertial_delay => 1 ns, transport_delay => 2 ns)
		port map (d => d, en => en, q => q_behavioral_parametric, nq => nq_behavioral_parametric);
	
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		-- Reset
		en <= '1'; d <= '0';
		wait until falling_edge(clock);
		

		-- Check store 0
		en <= '1'; d <= '1';
		wait until falling_edge(clock);
		en <= '1'; d <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and  q_behavioral_parametric = '0' and
					 nq_structural = '1' and nq_behavioral = '1' and nq_behavioral_parametric = '1' 
					 report "Test fail: Check store 0" severity FAILURE;
		
		-- Check store 1
		en <= '1'; d <= '0';
		wait until falling_edge(clock);
		en <= '1'; d <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and  q_behavioral_parametric = '1' and
					 nq_structural = '0' and nq_behavioral = '0' and nq_behavioral_parametric = '0'
					 report "Test fail: Check store 1" severity FAILURE;
		
		-- Check not store 0
		en <= '1'; d <= '1';
		wait until falling_edge(clock);
		en <= '0'; d <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and  q_behavioral_parametric = '1' and
					 nq_structural = '0' and nq_behavioral = '0' and nq_behavioral_parametric = '0'
					 report "Test fail: Check not store 0" severity FAILURE;
		
		-- Check not store 1
		en <= '1'; d <= '0';
		wait until falling_edge(clock);
		en <= '0'; d <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and  q_behavioral_parametric = '0' and
					 nq_structural = '1' and nq_behavioral = '1' and nq_behavioral_parametric = '1'
					 report "Test fail: Check not store 1" severity FAILURE;		
		
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;
