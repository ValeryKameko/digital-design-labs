library ieee;
use ieee.std_logic_1164.all;

entity jk_flip_flop_testbench is
end entity jk_flip_flop_testbench;

architecture testbench of jk_flip_flop_testbench is
	component jk_flip_flop is
		port(
			j, k, clk: in std_logic;
			reset: in std_logic;
			q, nq: out std_logic
		);
	end component jk_flip_flop;
	
	for uut_structural: jk_flip_flop use entity work.jk_flip_flop_structural(structural);
	for uut_behavioral: jk_flip_flop use entity work.jk_flip_flop_behavioral(behavioral);
	
	signal q_structural, nq_structural: std_logic;
	signal q_behavioral, nq_behavioral: std_logic;
	
	signal clock: std_logic := '0';
	signal j, k, clk, reset: std_logic;
	
	constant DELAY: time := 10 ns;
begin
	uut_structural: jk_flip_flop
		port map (j => j, k => k, clk => clk, reset => reset, q => q_structural, nq => nq_structural);
	
	uut_behavioral: jk_flip_flop
		port map (j => j, k => k, clk => clk, reset => reset, q => q_behavioral, nq => nq_behavioral);
	
	clk <= clock;
	
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		-- Reset
		j <= '0'; k <= '0'; reset <= '1';
		wait until falling_edge(clock);
		reset <= '0';
		

		-- Check reset
		j <= '1'; k <= '0';
		wait until falling_edge(clock);
		j <= '0'; k <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and
					 nq_structural = '1' and nq_behavioral = '1' report "Test fail: Check reset" severity FAILURE;
		
		-- Check set
		j <= '0'; k <= '1';
		wait until falling_edge(clock);
		j <= '1'; k <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and
					 nq_structural = '0' and nq_behavioral = '0' report "Test fail: Check set" severity FAILURE;
		
		-- Check flip
		j <= '0'; k <= '1';
		wait until falling_edge(clock);
		j <= '1'; k <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and
					 nq_structural = '0' and nq_behavioral = '0' report "Test fail: Check flip to 1" severity FAILURE;		
		j <= '1'; k <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and
					 nq_structural = '1' and nq_behavioral = '1' report "Test fail: Check flip to 0" severity FAILURE;		
		
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;