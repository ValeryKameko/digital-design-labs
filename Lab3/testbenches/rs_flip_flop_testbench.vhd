library ieee;
use ieee.std_logic_1164.all;

entity rs_flip_flop_testbench is
end entity rs_flip_flop_testbench;

architecture testbench of rs_flip_flop_testbench is
	component rs_flip_flop is
		port(
			set, reset: in std_logic;
			clk: in std_logic;
			q, nq: out std_logic
		);
	end component rs_flip_flop;
	
	for uut_structural: rs_flip_flop use entity work.rs_flip_flop_structural(structural);
	for uut_behavioral: rs_flip_flop use entity work.rs_flip_flop_behavioral(behavioral);
	
	signal q_structural, nq_structural: std_logic;
	signal q_behavioral, nq_behavioral: std_logic;
	
	signal clock: std_logic := '0';
	signal set, reset, clk: std_logic;
	
	constant DELAY: time := 10 ns;
begin
	uut_structural: rs_flip_flop
		port map (set => set, reset => reset, clk => clk, q => q_structural, nq => nq_structural);
	
	uut_behavioral: rs_flip_flop
		port map (set => set, reset => reset, clk => clk, q => q_behavioral, nq => nq_behavioral);
	
	clk <= clock;
	
	clock <= not clock after (DELAY / 2);
	
	test: process
	begin
		-- Reset
		set <= '0'; reset <= '1';
		wait until falling_edge(clock);
		

		-- Check reset
		set <= '1'; reset <= '0';
		wait until falling_edge(clock);
		set <= '0'; reset <= '1';
		wait until falling_edge(clock);
			assert  q_structural = '0' and  q_behavioral = '0' and
					 nq_structural = '1' and nq_behavioral = '1' report "Test fail: Check reset" severity FAILURE;
		
		-- Check set
		set <= '0'; reset <= '1';
		wait until falling_edge(clock);
		set <= '1'; reset <= '0';
		wait until falling_edge(clock);
			assert  q_structural = '1' and  q_behavioral = '1' and
					 nq_structural = '0' and nq_behavioral = '0' report "Test fail: Check set" severity FAILURE;
				
		
		assert false 
			report "End simulation" 
			severity FAILURE;
	end process;
end architecture testbench;