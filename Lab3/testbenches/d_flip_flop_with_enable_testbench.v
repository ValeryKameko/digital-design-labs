`timescale 1ns / 1ps
`include "assert.v"


module d_flip_flop_with_enable__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;

localparam time DELAY = 10;
reg clock = 0;
reg d, en;
wire clk = clock;

d_flip_flop_with_enable__behavioral uut_behavioral(
	.d(d),
	.clk(clk),
	.en(en),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

d_flip_flop_with_enable__structural uut_structural(
	.d(d),
	.clk(clk),
	.en(en),
	.q(q_structural),
	.nq(nq_structural)
);

always
	clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	{d, en} <= 2'b01;
	@(negedge clock);
	

	// Check store 0
	{d, en} <= 2'b01;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check store 0");
	
	// Check store 1
	{d, en} <= 2'b11;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check store 1");
	
	// Check not store 0
	{d, en} <= 2'b11;
	@(negedge clock);
	{d, en} <= 2'b00;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check not store 0");
	
	// Check not store 1
	{d, en} <= 2'b01;
	@(negedge clock);
	{d, en} <= 2'b10;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check not store 1");
		

	$stop;
end;

endmodule
