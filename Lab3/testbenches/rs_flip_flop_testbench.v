`timescale 1ns / 1ps
`include "assert.v"


module rs_flip_flop__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;

localparam time DELAY = 10;
reg clock = 0;
reg set, reset;
wire clk = clock;

rs_flip_flop__behavioral uut_behavioral(
	.set(set),
	.reset(reset),
	.clk(clk),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

rs_flip_flop__structural uut_structural(
	.set(set),
	.reset(reset),
	.clk(clk),
	.q(q_structural),
	.nq(nq_structural)
);

always
	clock = #(DELAY / 2) ~clock;

initial
begin
	// Reset
	{set, reset} <= 2'b01;
	@(negedge clock);

	// Check reset
	{set, reset} <= 2'b10;
	@(negedge clock);
	{set, reset} <= 2'b01;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b00 && {nq_structural, nq_behavioral} == 2'b11,
			"Test fail: Check reset");
	
	// Check set
	{set, reset} <= 2'b01;
	@(negedge clock);
	{set, reset} <= 2'b10;
	@(negedge clock);
		`assert_({q_structural, q_behavioral} == 2'b11 && {nq_structural, nq_behavioral} == 2'b00,
			"Test fail: Check set");
		
	$stop;
end;

endmodule
