`timescale 1ns / 1ps
`include "assert.v"


module d_latch_with_enable__testbench;

wire q_structural, nq_structural;
wire q_behavioral, nq_behavioral;
wire q_behavioral_parametric, nq_behavioral_parametric;

localparam time DELAY = 10;
reg clock = 0;
reg d, en;

d_latch_with_enable__behavioral uut_behavioral(
	.d(d),
	.en(en),
	.q(q_behavioral),
	.nq(nq_behavioral)
);

d_latch_with_enable__structural uut_structural(
	.d(d),
	.en(en),
	.q(q_structural),
	.nq(nq_structural)
);

d_latch_with_enable__behavioral #(.INERTIAL_DELAY(1), .TRANSPORT_DELAY(2)) uut_behavioral_parametric(
	.d(d),
	.en(en),
	.q(q_behavioral_parametric),
	.nq(nq_behavioral_parametric)
);

always 
	clock = #(DELAY / 2) ~clock;

always
begin
	// Reser
	{en, d} <= 2'b10;
	@(negedge clock);
	
	// Check store 0
	{en, d} <= 2'b11;
	@(negedge clock);
	{en, d} <= 2'b10;
	@(negedge clock);
		`assert_({q_structural, q_behavioral, q_behavioral_parametric} == 3'b000 && 
					{nq_structural, nq_behavioral, nq_behavioral_parametric} == 3'b111,
			"Test fail: Check store 0");
			
	// Check store 1
	{en, d} <= 2'b10;
	@(negedge clock);
	{en, d} <= 2'b11;
	@(negedge clock);
		`assert_({q_structural, q_behavioral, q_behavioral_parametric} == 3'b111 && 
					{nq_structural, nq_behavioral, nq_behavioral_parametric} == 3'b000,
			"Test fail: Check store 1");
	
	// Check not store 0
	{en, d} <= 2'b11;
	@(negedge clock);
	{en, d} <= 2'b00;
	@(negedge clock);
		`assert_({q_structural, q_behavioral, q_behavioral_parametric} == 3'b111 && 
					{nq_structural, nq_behavioral, nq_behavioral_parametric} == 3'b000,
			"Test fail: Check not store 0");
			
	// Check not store 1
	{en, d} <= 2'b10;
	@(negedge clock);
	{en, d} <= 2'b01;
	@(negedge clock);
		`assert_({q_structural, q_behavioral, q_behavioral_parametric} == 3'b000 && 
					{nq_structural, nq_behavioral, nq_behavioral_parametric} == 3'b111,
			"Test fail: Check not store 1");

	$stop;
end;

endmodule
